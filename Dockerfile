FROM node:10-alpine

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install -g @angular/cli@7.3.9

RUN npm i
#RUN ng update
#RUN npm update
COPY . .
EXPOSE 4202
CMD [ "npm", "run", "start"]
