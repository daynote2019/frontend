// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  supportedLanguages: ['ru', 'en'],
  defaultLanguage: 'ru'
};
// export const logo = 'LiDocs';
// export const logo = 'FlexNote';
// export const logo = 'FlexDocs';
// export const logo = 'CatNote';
// export const logo = 'FrienDocs';
// export const logo = 'WhoseDocs';
// export const logo = 'Skryf';
export const logo = 'ScreenDocs';
export const hostServer = 'http://127.0.0.1:8001/';
export const hostClient = 'http://localhost:4202/';
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
