export const environment = {
  supportedLanguages: ['ru', 'en'],
  defaultLanguage: 'ru',
  production: true
};
export const logo = 'ScreenDocs';
export const hostServer = 'http://127.0.0.1:8001/';
export const hostClient = 'http://localhost:4202/';
