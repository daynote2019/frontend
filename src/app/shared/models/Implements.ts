export interface Deserializable {
  deserialize(): any;
}
