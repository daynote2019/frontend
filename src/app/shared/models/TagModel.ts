import {ITag} from '@shared/interfaces/work';
import {ETagType} from '@shared/enums/work';
import {GlobalsService} from '@shared/services/globals.service';
import {Deserializable} from '@shared/models/Implements';

export class TagModel implements Deserializable {
  public id: string;
  public name: string;
  public type: ETagType;
  public isSelected: boolean;
  public readonly: boolean;

  constructor(tag: ITag) {
    this.id = tag.id;
    this.name = tag.name;
    this.type = tag.type;
    this.isSelected =
      GlobalsService.isNotNullAndUndefined(tag.isSelected) ? tag.isSelected : false;
    this.readonly = true;
  }

  deserialize(): ITag {
    return {
      id: this.id,
      name: this.name,
      type: this.type,
      isSelected: this.isSelected,
      readonly: this.readonly
    }
  }
}
