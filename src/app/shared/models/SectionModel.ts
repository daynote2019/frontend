import {InternalUsers, ISectionInfo, ITag} from '@shared/interfaces/work';
import {ETagType} from '@shared/enums/work';
import {GlobalsService} from '@shared/services/globals.service';
import {Deserializable} from '@shared/models/Implements';

export class SectionModel implements Deserializable {
  id: string;
  name: string;
  internalUserId: string;
  active: boolean;
  isRemoved: boolean;

  constructor(section: ISectionInfo) {
    this.id = section.id;
    this.name = section.name;
    this.internalUserId = section.internalUserId;
    this.active = section.active;
    this.isRemoved = section.isRemoved;
  }

  deserialize(): ISectionInfo {
    return {
      id: this.id,
      name: this.name,
      internalUserId: this.internalUserId,
      active: this.active,
      isRemoved: this.isRemoved,
    }
  }
}
