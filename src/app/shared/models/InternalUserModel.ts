import {InternalUsers, ITag} from '@shared/interfaces/work';
import {Deserializable} from '@shared/models/Implements';

export class InternalUserModel implements Deserializable {
  id: string;
  name: string;
  icon: string;
  active: boolean;
  isRemoved: boolean;

  constructor(iUser: InternalUsers) {
    this.id = iUser.id;
    this.name = iUser.name;
    this.icon = iUser.icon;
    this.active = iUser.active;
    this.isRemoved = iUser.isRemoved;
  }

  deserialize(): InternalUsers {
    return {
      id: this.id,
      name: this.name,
      icon: this.icon,
      active: this.active,
      isRemoved: this.isRemoved,
    }
  }
}
