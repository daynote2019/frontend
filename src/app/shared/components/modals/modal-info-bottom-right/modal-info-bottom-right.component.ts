import { Component, OnInit } from '@angular/core';
import {IModalBasicCenter} from '@shared/interfaces/common';
import {select, Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';
import {GlobalsService} from '@shared/services/globals.service';
import {IModalBasicCenterType} from '@shared/enums/common';

@Component({
  selector: 'app-modal-info-bottom-right',
  templateUrl: './modal-info-bottom-right.component.html',
  styleUrls: ['./modal-info-bottom-right.component.scss']
})
export class ModalInfoBottomRightComponent implements OnInit {

  public alert: IModalBasicCenter;
  public timeout = null;

  public Type = IModalBasicCenterType;

  constructor(
    private store$: Store<mainRoot.FeatureState>,
    private globals: GlobalsService,
  ) { }

  ngOnInit() {
    this.store$.pipe(select(mainRoot.selectModalInfoBottomRight))
      .subscribe(alert => {
        if (this.globals.isNotEmptyObject(alert)) {
          this.alert = alert;
          if (alert.state) {
            this.alert.state = true;
            if (this.timeout) { // Обнулить таймаут, чтобы модалка не закрывалась раньше времени
              clearTimeout(this.timeout);
            }
            this.timeout = setTimeout(() => {
              this.closeModal();
            }, 7000);
          }
        }
      });
  }

  closeModal() {
    this.alert.state = false;
  }

  private returnStructBack() {
    if (this.alert.action) {
      this.alert.action();
    }
    this.closeModal();
  }
}
