import {Component, ElementRef, HostListener, OnInit} from '@angular/core';
import {IModalRightClick} from '@shared/interfaces/common';
import {select, Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';
import {GlobalsService} from '@shared/services/globals.service';
import {EStructureInfoField, StructureInfoType} from '@shared/enums/work';
import * as workRoot from '@app/root-store/work/reduser';
import {ChangeStructureInfoAnyValue, SelectActiveStructAction} from '@app/root-store/work/actions';
import {WorkService} from '@shared/services/work.service';

@Component({
  selector: 'app-modal-right-click',
  templateUrl: './modal-right-click.component.html',
  styleUrls: ['./modal-right-click.component.scss']
})
export class ModalRightClickComponent implements OnInit {

  public modal: IModalRightClick;
  public StructType = StructureInfoType;

  constructor(
    private eRef: ElementRef,
    private storeWork$: Store<workRoot.WorkState>,
    private store$: Store<mainRoot.FeatureState>,
    private globals: GlobalsService,
    private workService: WorkService,
  ) { }

  ngOnInit() {
    this.store$.pipe(select(mainRoot.selectModalRightClick))
      .subscribe(modal => {
        if (this.globals.isNotEmptyObject(modal)) {
          this.modal = modal;
        }
      });
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if(this.eRef.nativeElement.contains(event.target)) {} else {
      this.closeModal()
    }
  }

  create(type: StructureInfoType) {
    this.workService.addNewStructElement(type);
    this.closeModal()
  }

  /** Метод вызывается, когда в модалке кликаем на Переименовать.
   * Инпут становится редактируемым, в него устанавливается фокус и выделяется текст
   * Далее в structure-info при событиях focusout или keyup.enter вызывается метод saveTitle() */
  rename() {
    const struct = this.modal.struct; /* Сюда передали объект, поэтому им и манипулируем */
    struct.readonly = false;
    struct.isFocus = true;
    if (!struct.readonly && struct.isFocus) {
      setTimeout(() => {
        const structElementRef = this.globals.getStructElementRefFromRef(struct, this.modal.structRefElement);
        if (structElementRef) {
          structElementRef.nativeElement.focus();
          structElementRef.nativeElement.select();
        }
      }, 50);
    }
    this.closeModal();
  }

  removeElement() {
    this.workService.removeStructElement(this.modal.struct);
    this.closeModal()
  }

  closeModal() {
    this.modal.state = false;
  }
}
