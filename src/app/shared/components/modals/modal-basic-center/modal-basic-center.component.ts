import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import { IModalBasicCenter} from '@shared/interfaces/common';
import {GlobalsService} from '@shared/services/globals.service';
import {select, Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';
import {IModalBasicCenterType} from '@shared/enums/common';
import {AbstractControl, FormControl, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {AuthService} from "@shared/services/auth.service";

@Component({
  selector: 'app-modal-basic-center',
  templateUrl: './modal-basic-center.component.html',
  styleUrls: ['./modal-basic-center.component.scss']
})
export class ModalBasicCenterComponent implements OnInit {

  public basicCenterForm: FormGroup;
  public alert: IModalBasicCenter;
  public scrollTop = `${GlobalsService.desktopNav}px`;

  public Type = IModalBasicCenterType;
  public GlobalsService = GlobalsService;
  @ViewChild('confirmInput') confirmInput: ElementRef;

  public selectValue: string;
  public onceFormInvalidCheck: boolean = true; // Можно здесь поставить FALSE и тогда проигнорирует ошибку в форме

  constructor(
    private store$: Store<mainRoot.FeatureState>,
    private globals: GlobalsService,
    private renderer: Renderer2,
  ) {
    this.basicCenterForm = new FormGroup({
      confirmInput: new FormControl('', [
        this.confirmInputCheckValidator()
      ]),
    });
  }

  ngOnInit() {
    this.store$.pipe(select(mainRoot.selectModalBasicCenter))
      .subscribe(alert => {
        if (this.globals.isNotEmptyObject(alert)) {
          this.alert = alert;
          if (alert.state) {
            this.globals.fixBody(this.renderer, true);
            setTimeout(() => {
              if (this.confirmInput && this.confirmInput.nativeElement) {
                this.confirmInput.nativeElement.focus();
              }
              if (alert.inputValue) {
                this.basicCenterForm.controls['confirmInput'].setValue(alert.inputValue);
              }
              this.selectValue = alert.selectValue ? alert.selectValue : undefined;
            }, 200)
          }
        }
    });
    this.globals.documentInfo$.subscribe(doc => {
      this.scrollTop = `${this.globals.getScrollTop(doc)}px`;
    });
  }

  ngOnDestroy() {
    this.closeModal();
  }

  public isConfirmInputSubmit() {
    let isConfirm = false;
    const confirmInputText = this.basicCenterForm.get('confirmInput').value;
    if (this.alert && this.alert.notConfirmedInputSubmit) {
      isConfirm = (GlobalsService.isNotNullAndUndefined(this.selectValue) && this.selectValue !== '') ||
        (GlobalsService.isNotNullAndUndefined(confirmInputText) && confirmInputText !== '');
    } else {
      isConfirm = (GlobalsService.isNotNullAndUndefined(confirmInputText) && confirmInputText !== '');
    }

    return isConfirm;
  }

  public chooseSelect(selectValue) {
    this.selectValue = selectValue;
  }

  private action() {
    if (this.onceFormInvalidCheck ? this.basicCenterForm.invalid : false) {
      this.onceFormInvalidCheck = false; // Первый раз показываем ошибку, а второй уже пропускаем дальше
      return;
    }
    if (this.alert.action) {
      const confirmInputText = this.basicCenterForm.get('confirmInput').value;
      if (this.alert.type === IModalBasicCenterType.confirmInput) {
        this.alert.action(confirmInputText);
      } else if (this.alert.type === IModalBasicCenterType.confirmInputWithSelect) {
        this.alert.action(confirmInputText, this.selectValue);
      } else {
        this.alert.action();
      }
    }
    this.closeModal();
  }

  public closeModal() {
    this.alert.state = false;
    // Снимаем фиксирование контента
    this.globals.fixBody(this.renderer, false);
    this.basicCenterForm.controls['confirmInput'].setValue('');
    // this.confirmInputText = '';
  }

  private confirmInputCheckValidator(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: boolean} | null => {
      if (this.alert && this.alert.confirmInputCheck &&
        this.alert.confirmInputCheck(control.value)) {
        return { 'confirmInputCheck': true };
      }
      return null;
    };
  }
}
