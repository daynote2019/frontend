import {Component, ElementRef, OnInit, QueryList, ViewChildren} from '@angular/core';
import {GlobalsService} from '@shared/services/globals.service';
import {TagModalInfo} from '@shared/interfaces/common';
import {ITag} from '@shared/interfaces/work';
import {select, Store} from '@ngrx/store';
import * as workRoot from '@app/root-store/work/reduser';
import {HelpersService} from '@shared/services/helpers.service';
import {ChangeTagNameAction} from '@app/root-store/work/actions';
import * as editingAreaRoot from '@app/root-store/editing-area/reduser';
import {RemoveTagFromAllOnServerAction} from '@app/root-store/editing-area/actions';

@Component({
  selector: 'app-modal-management',
  templateUrl: './modal-management.component.html',
  styleUrls: ['./modal-management.component.scss',
    '../modal-basic-center/modal-basic-center.component.scss']
})
export class ModalManagementComponent implements OnInit {
  private scrollTopSubscription;
  private modalsInfoSubscription;
  private tagsSubscription;

  @ViewChildren('tag') tagElements: QueryList<ElementRef>;

  public scrollTop = `${GlobalsService.desktopNav}px`;
  public tagsManagementModal: TagModalInfo;
  public tags: Array<ITag> = [];
  private filteredTags: Array<ITag> = [];
  public filterTagName: string;
  public isShowRemoveTagModal = false;
  public currentTag: ITag|null = null;

  public GlobalsService = GlobalsService;

  constructor(
    private globals: GlobalsService,
    private storeWork$: Store<workRoot.WorkState>,
    private storeEditingArea$: Store<editingAreaRoot.EditingAreaState>,
  ) { }

  ngOnInit() {
    this.globals.documentInfo$.subscribe(doc => {
      this.scrollTop = `${this.globals.getScrollTop(doc)}px`;
    });
    this.globals.modalsInfo$.subscribe(modal => {
      this.tagsManagementModal = modal.tagsManagementModal;
      this.resetFilterTagName();
    });
    this.tagsSubscription = this.storeWork$
      .pipe(select(workRoot.selectTags))
      .subscribe(tags => {
        this.tags = tags;
        this.filteredTags = tags.sort(HelpersService.compareTags);
        this.changeFilterTagName();
      });
  }

  ngOnDestroy() {
    this.scrollTopSubscription.unsubscribe();
    this.modalsInfoSubscription.unsubscribe();
    this.tagsSubscription.unsubscribe();
  }

  /* TAGS */
  modalTagsManagementOpenClose(state: boolean) {
    this.globals.modalTagsManagementOpenClose({
      tagsManagementModal: {
        isOpenState: state
      }
    });
  }

  setTagEditable(tag: ITag) {
    this.filteredTags.forEach(_tag => _tag.readonly = !(_tag.id === tag.id));
    setTimeout(() => {
      const tagElementRef = this.getOneTagElementRef(tag);
      if (tagElementRef) {
        tagElementRef.nativeElement.focus();
        tagElementRef.nativeElement.select();
      }
    }, 30);
  }

  saveTagName(tag: ITag) {
    this.filteredTags.forEach(_tag => _tag.readonly = true);
    this.storeWork$.dispatch(new ChangeTagNameAction({
      tagId: tag.id,
      name: tag.name
    }))
  }

  changeFilterTagName() {
    this.filteredTags = this.tags.filter(_tag => {
      if (_tag.name.toLowerCase().indexOf(this.filterTagName.toLowerCase()) !== -1) {
        return _tag;
      }
    });
  }

  showHideRemoveTagModal(state: boolean, tag?: ITag) {
    this.isShowRemoveTagModal = state;
    if (tag) { this.currentTag = tag }
  }

  removeTag() {
    if (this.currentTag) {
      this.storeEditingArea$.dispatch(new RemoveTagFromAllOnServerAction({
        tagId: this.currentTag.id
      }));
      this.showHideRemoveTagModal(false);
    }
  }

  resetFilterTagName() {
    this.filterTagName = '';
    this.changeFilterTagName();
  }

  private getOneTagElementRef(tag: ITag): ElementRef {
    return this.tagElements.find(el => {
      if (el.nativeElement && el.nativeElement.dataset.id) {
        return el.nativeElement.dataset.id === tag.id;
      }
    });
  }
}
