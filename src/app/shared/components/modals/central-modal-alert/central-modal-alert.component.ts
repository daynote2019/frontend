import {Component, OnInit, Renderer2} from '@angular/core';
import {select, Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';
import {NavService} from '@shared/services/nav.service';
import {GlobalsService} from '@shared/services/globals.service';
import {CentralModalAlert} from '@shared/interfaces/common';
import {ResponseErrorValidateAction} from '@app/root-store/errors/actions';
import * as errorRoot from '@app/root-store/errors/reduser';

@Component({
  selector: 'app-central-modal-alert',
  templateUrl: './central-modal-alert.component.html',
  styleUrls: ['./central-modal-alert.component.scss']
})
export class CentralModalAlertComponent implements OnInit {

  public alert: CentralModalAlert;
  public scrollTop = `${GlobalsService.desktopNav}px`;

  constructor(
    private store$: Store<mainRoot.FeatureState>,
    private navService: NavService,
    private globals: GlobalsService,
    private renderer: Renderer2,
    private storeError$: Store<errorRoot.ErrorsState>,
  ) {}

  ngOnInit() {
    this.store$.pipe(select(mainRoot.selectCentralModalAlert))
      .subscribe(alert => {
        if (this.globals.isNotEmptyObject(alert)) {
          this.alert = alert;
          if (alert.state) {
            this.globals.fixBody(this.renderer, true);
            this.alert.state = true;
          }
        }
    });
    this.globals.documentInfo$.subscribe(doc => {
      this.scrollTop = `${this.globals.getScrollTop(doc)}px`;
    });
  }

  public click(isRoute: boolean) {
    this.closeModal();
    if (isRoute || this.alert.isRedirectIfClickBg) {
      this.onPageRoute();
    }
    /* Очистить ошибки TODO здесь только очищаем responseValidate, как быть с остальными? */
    this.storeError$.dispatch(new ResponseErrorValidateAction(
      errorRoot.initialState.responseValidate
      )
    );
  }

  private closeModal() {
    this.alert.state = false;
    // Снимаем фиксирование контента
    this.globals.fixBody(this.renderer, false);
  }

  private onPageRoute() {
    if (this.alert.url) {
      this.navService.onPageRoute(this.alert.url);
    }
  }
}
