import {Component, OnInit, Renderer2} from '@angular/core';
import {GlobalsService} from '@shared/services/globals.service';
import {select, Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';
import {SpinnerCenter} from '@shared/interfaces/common';

@Component({
  selector: 'app-spinner-center',
  templateUrl: './spinner-center.component.html',
  styleUrls: ['./spinner-center.component.scss']
})
export class SpinnerCenterComponent implements OnInit {

  private requestTime = 0;
  public spinner = false;

  constructor(
    private store$: Store<mainRoot.FeatureState>,
    private globals: GlobalsService,
    private renderer: Renderer2,
  ) { }

  ngOnInit() {
    this.store$.pipe(select(mainRoot.selectSpinnerCenter)).subscribe((spinner: SpinnerCenter) => {
      if (this.globals.isNotEmptyObject(spinner)) {
        if (!spinner.state && this.spinner) { // Скрыть лоадер
          // Вычислить время после ответа сервер -> фронт
          let timeout = new Date().getTime() - this.requestTime;
          // Ставим задержку, если запрос отработал меньше, чем за delayTimeout миллисенунд
          timeout = (timeout > GlobalsService.delayTimeout) ? 0 : GlobalsService.delayTimeout;
          setTimeout(() => {
            this.globals.fixBody(this.renderer, false);
            this.spinner =  false;
            if (spinner.action) {
              spinner.action();
            }
          }, timeout);
        } else if (spinner.state && !this.spinner) { // Показать лоадер
          this.spinner =  true;
          // Задать время при запросе фронт -> сервер.
          this.requestTime = new Date().getTime();
          this.globals.fixBody(this.renderer, true);
        }
      }
    });
  }

}
