import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable, ReplaySubject} from 'rxjs';
import {MatSelect, MatSelectChange} from '@angular/material';

@Component({
  selector: 'app-custom-mat-select',
  templateUrl: './custom-mat-select.component.html',
  styleUrls: ['./custom-mat-select.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CustomMatSelectComponent<T> implements OnInit, AfterViewInit, OnDestroy {

  private selectedSearchSubscription;
  private selectedObjectsArrayObservableSubscription;

  @Input() public bindValue: string;
  @Input() public bindLabel: string;
  @Input() public isResetSelection?: boolean;

  /* Поле объекта, по которому будет сравнение в фильтре(нужно только для поиска на фронте) */
  @Input() public selectedObjectName: string;

  /* Текущее значение модели(value) */
  @Input() public selectedItem;

  /* Передаёт значение на выход, после того, как выбрали значение выпадающего списка */
  @Output() public selectedItemChange: EventEmitter<any> = new EventEmitter<any>();

  /* Контрол для поиска */
  public selectedSearchCtrl: FormControl = new FormControl();

  /* Массив объектов, в котором происходит поиск, а так же наполнитель выпадающего списка */
  @Input() public selectedObjectsArray: Array<T> = [];
  @Input() selectedObjectsArrayObservable?: Observable<T[]>|null; /* И подписка на его динамическое изменение из родителя */

  /* Контейнер для наполнителя(selectedObjectsArray) */
  public filteredSelected: ReplaySubject<T[]> = new ReplaySubject<T[]>(1);

  constructor() {
  }

  ngOnInit() {
    this.selectedSearchSubscription = this.selectedSearchCtrl.valueChanges.subscribe(() => {
      this.searchFilterHandler();
    });
    if (this.selectedObjectsArrayObservable) {
      this.selectedObjectsArrayObservableSubscription =
        this.selectedObjectsArrayObservable.subscribe(array => {
          this.selectedObjectsArray = array;
          this.filteredSelected.next(array.slice());
      });
    }
  }

  ngOnDestroy() {
    this.selectedSearchSubscription.unsubscribe();
    if (this.selectedObjectsArrayObservableSubscription) {
      this.selectedObjectsArrayObservableSubscription.unsubscribe();
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      /* Заполняем выпадающий список значениями */
      this.filteredSelected.next(this.selectedObjectsArray.slice());
    }, 200);
  }

  /* Запускаем поиск */
  searchFilterHandler() {
    let searchValue = this.selectedSearchCtrl.value;
    if (!searchValue) {
      this.filteredSelected.next(this.selectedObjectsArray.slice());
      return;
    }
    searchValue = searchValue.toLowerCase();
    this.filteredSelected.next(
      this.selectedObjectsArray.filter(obj =>
        obj[this.selectedObjectName].toLowerCase().indexOf(searchValue) > -1)
    );
  }

  /* Запускает передачу значения на выход, после того, как выбрали значение выпадающего списка */
  selectedItemChangeMethod(chosenSelectedItem: MatSelectChange) {
    this.selectedItemChange.emit(chosenSelectedItem.value);
    if (this.isResetSelection) {
      setTimeout(() => {
        this.selectedItem = undefined;
        // const matSelect: MatSelect = chosenSelectedItem.source;
        // matSelect.writeValue(undefined);
      }, 100);
    }
  }
}
