import {Component, OnInit, Renderer2} from '@angular/core';
import {select, Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';
import {SpinnerTop} from '@shared/interfaces/common';
import {GlobalsService} from '@shared/services/globals.service';

@Component({
  selector: 'app-spinner-top',
  templateUrl: './spinner-top.component.html',
  styleUrls: ['./spinner-top.component.scss']
})
export class SpinnerTopComponent implements OnInit {

  private requestTime = 0;
  public spinner = false;
  public scrollTop = `${GlobalsService.desktopNav}px`;
  public scrollTopProgressBar = `${GlobalsService.desktopNav - 4}px`;

  constructor(
    private store$: Store<mainRoot.FeatureState>,
    private globals: GlobalsService,
    private renderer: Renderer2,
  ) { }

  ngOnInit() {
    this.store$.pipe(select(mainRoot.selectSpinnerTop)).subscribe((spinner: SpinnerTop) => {
      if (this.globals.isNotEmptyObject(spinner)) {
        if (!spinner.state && this.spinner) { // Скрыть лоадер
          // Вычислить время после ответа сервер -> фронт
          let timeout = new Date().getTime() - this.requestTime;
          // Ставим задержку, если запрос отработал меньше, чем за delayTimeout миллисенунд
          timeout = (timeout > GlobalsService.delayTimeout) ? 0 : GlobalsService.delayTimeout;
          setTimeout(() => {
            // Снимаем фиксирование контента
            this.globals.fixBody(this.renderer, false);
            this.spinner =  false;
            if (spinner.action) {
              spinner.action();
            }
          }, timeout);
        } else if (spinner.state && !this.spinner) { // Показать лоадер
          this.spinner =  true;
          // Задать время при запросе фронт -> сервер.
          this.requestTime = new Date().getTime();
          this.globals.fixBody(this.renderer, true);
        }
      }
    });
    this.globals.documentInfo$.subscribe(doc => {
      this.scrollTop = `${this.globals.getScrollTop(doc) + 4}px`;
      this.scrollTopProgressBar = `${this.globals.getScrollTop(doc)}px`;
    });
  }
}
