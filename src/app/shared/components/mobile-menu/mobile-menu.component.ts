import {Component, Input, Output, OnInit, EventEmitter, HostListener, OnDestroy} from '@angular/core';
import {NavService} from '@shared/services/nav.service';
import {NavList, MobileLeftSidenavListItemArrowOpen, PageInfo} from '@shared/interfaces/common';
import {NavListTypes, Pages} from '@shared/enums/common';

@Component({
  selector: 'app-mobile-menu',
  templateUrl: './mobile-menu.component.html',
  styleUrls: ['./mobile-menu.component.scss']
})
export class MobileMenuComponent implements OnInit, OnDestroy {

  private pageInfoSubscription;

  public pageInfo: PageInfo;
  public listType = NavListTypes;
  /* Открыть/закрыть мобайл левый sidenav изнутри компонента */
  @Input() public mobileIsOpen: boolean;
  /* Открыть/закрыть мобайл левый sidenav снаружи компонента */
  @Output() public parentMobileIsOpen = new EventEmitter<boolean>();
  /* Открыть/закрыть выпадающий список пункта меню с arrow */
  public arrowOpen: MobileLeftSidenavListItemArrowOpen;
  public mobileLeftSidenavList: NavList[];

  constructor(
    public navService: NavService
  ) {}

  ngOnInit() {
    /* Подписаться на изменение информации о странице */
    this.pageInfoSubscription = this.navService.pageInfo$.subscribe(pageInfo => {
      this.pageInfo = pageInfo;
    });
    this.mobileIsOpen = false;
    this.arrowOpen = {
      about: this.isPageActive(Pages.about)
    };
    this.mobileLeftSidenavList = [
      {
        type: NavListTypes.arrow,
        name: 'page.about',
        click: Pages.about,
        active: Pages.about,
        children: [
          {
            click: Pages.history,
            active: Pages.about,
            name: 'page.history.name',
          },
          // {
          //   click: Pages.how,
          //   active: Pages.about,
          //   name: 'page.howBegin.name'
          // },
          // {
          //   click: Pages.history,
          //   active: Pages.about,
          //   name: 'page.whereFind.name'
          // },
        ]
      },
      // {
      //   type: NavListTypes.link,
      //   active: Pages.contacts,
      //   click: Pages.contacts,
      //   name: 'page.contacts',
      // },
      // {
      //   type: NavListTypes.link,
      //   active: Pages.demo,
      //   click: Pages.demo,
      //   name: 'page.tariffs',
      // },
      {
        type: NavListTypes.line,
        click: '',
        name: '',
      },
      // {
      //   type: NavListTypes.link,
      //   active: Pages.demo,
      //   click: Pages.demo,
      //   name: 'page.demo',
      // },
      {
        type: NavListTypes.link,
        active: Pages.login,
        click: Pages.login,
        name: 'page.login',
      },
      {
        type: NavListTypes.link,
        active: Pages.registration,
        click: Pages.registration,
        name: 'page.registration',
      },
    ];
  }

  ngOnDestroy() {
    this.pageInfoSubscription.unsubscribe();
  }

  /* Открыть/закрыть мобильное меню */
  controlMobileMenu(value: boolean) {
    this.setMobileOpens(value);
    this.arrowStateAssign();
  }

  /* Открыть/закрыть выпадающий список пункта меню с arrow */
  openArrowNavItem(item: string) {
    this.arrowOpen[item] = !this.arrowOpen[item];
  }

  public onPageRoute(url) {
    this.navService.onPageRoute(url);
    this.setMobileOpens(false);
    this.arrowStateAssign();
  }

  /* Событие ресайз браузера */
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setMobileOpens(false);
    this.arrowStateAssign();
  }

  public isPageActive(page: string) {
    return page === this.pageInfo.chosen;
  }

  public isChildActive(page: string) {
    return page === this.pageInfo.childActive;
  }

  private setMobileOpens(value: boolean) {
    if (this.mobileIsOpen) {
      this.mobileIsOpen = value;
      this.parentMobileIsOpen.emit(value);
    }
  }

  /* Назначить стрелочным подпунктам меню их состояние(true/false) в зависимости от страницы */
  private arrowStateAssign() {
    setTimeout(() => {
      for (const prop in this.arrowOpen) {
        this.arrowOpen[prop] = this.isPageActive(prop);
      }
    }, 400);
  }
}
