import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {select, Store} from '@ngrx/store';
import * as workRoot from '@app/root-store/work/reduser';
import {StructureInfo} from '@shared/interfaces/work';
import {StructureInfoType} from '@shared/enums/work';
import {
  ChangeStructureInfoTitle,
  SelectActiveStructAction,
  SelectActiveStructServerAction
} from '@app/root-store/work/actions';
import {SessionService} from '@shared/services/session.service';
import * as mainRoot from '@app/root-store/common/reduser';
import {ModalRightClickAction} from '@app/root-store/common/actions';
import {WorkService} from '@shared/services/work.service';
import {EDirection} from '@shared/enums/common';
import {fadeInAnimation} from "@shared/functions/animations";
import {GlobalsService} from "@shared/services/globals.service";
import {ISelectActiveStructInfo} from "@shared/interfaces/request";

@Component({
  selector: 'app-structure-info',
  templateUrl: './structure-info.component.html',
  styleUrls: ['./structure-info.component.scss'],
  animations: fadeInAnimation()
})
export class StructureInfoComponent implements OnInit {
  @Input() public struct: StructureInfo;
  @Input() public activeStruct: StructureInfo;
  @ViewChild('structRef') structRefElement: ElementRef;

  public Type = StructureInfoType;
  public GlobalsService = GlobalsService;
  public structOldTitle = '';

  constructor(
    private store$: Store<mainRoot.FeatureState>,
    private workService: WorkService,
    private sessionService: SessionService,
    private storeWork$: Store<workRoot.WorkState>,
    private globals: GlobalsService,
  ) { }

  ngOnInit() {
    if (this.struct && (!this.struct.readonly && this.struct.isFocus)) { /* Отрабатывает при создании */
      this.structOldTitle = this.struct.title;
      setTimeout(() => {
        const structElementRef = this.globals.getStructElementRefFromRef(this.struct, this.structRefElement);
        if (structElementRef) {
          structElementRef.nativeElement.focus();
          structElementRef.nativeElement.select();
        }
      }, 50);
    }
  }

  isActive(struct: StructureInfo) {
    return (this.activeStruct && this.activeStruct.id) === (struct && struct.id);
  }

  clickStruct(struct: StructureInfo) {
    if (this.activeStruct) {
      /**
       * struct(параметр функции) это новый элемент, this.activeStruct это старый(предыдущий)
       * Условие сработает, если мы с одного элемента перешли на другой и readonly-статус предыдущего элемента
       * был false(т.е. он был в режиме редактирования)
       */
      if ((this.activeStruct.id !== struct.id) && !this.activeStruct.readonly) {
        /* На момент клика по новому элементу activeStruct ещё старый(предыдущий) поэтому его здесь и используем */
        this.saveTitle(this.activeStruct);
      }
      if ((this.activeStruct.id === struct.id) && (struct.type === StructureInfoType.file)) {
          /* Если кликаем по активному документу, то ничего не делаем(по-другому не знаю, как сделать такое условие) */
      } else {
        this.workService.clickStructElement(struct);
      }
    } else {
      this.workService.clickStructElement(struct);
    }
  }

  openFolder(struct: StructureInfo) {
    struct.isOpen = !struct.isOpen;
    const structToActiveState: ISelectActiveStructInfo = {
      struct,
      info: struct.isOpen ? '1' : '0',
      isOpenFolder: true
    };
    this.storeWork$.dispatch(new SelectActiveStructServerAction(structToActiveState));
  }

  // changeStruct(oldVal: string, newVal: string) { // (ngModelChange)="changeStruct(structOldTitle, $event)"
  //   this.structOldTitle = oldVal;
  // }

  onRightClick(event, struct: StructureInfo) {
    /** При вызове контекстного меню инициализируется старое значение названия
     * Если методы из конт. меню(в частности rename) будут вызывать в других местах,
     * то видимо следует делать инициализацию в ngOnInit() */
    this.structOldTitle = this.struct ? this.struct.title : '';
    this.store$.dispatch(new ModalRightClickAction({
      state: true,
      type: struct.type,
      coordinates: {
        top: event.clientY,
        left: event.clientX,
      },
      struct,
      structRefElement: this.structRefElement
    }));
    if (!this.activeStruct || (this.activeStruct && this.activeStruct.id !== struct.id)) {
      this.clickStruct(struct);
    }
    return false;
  }

  /* Сохранить название(если оно было изменено) и убрать фокус, если есть */
  saveTitle(struct: StructureInfo) {
    if (!struct.readonly) { /* Если инпут в режиме редактирования(!FALSE) */
      struct.readonly = true;
      struct.isFocus = false;
      if (struct && (struct.title !== this.structOldTitle)) { /* Если название мтр-инфо менялось, то отправляем запрос на сервер */
        this.storeWork$.dispatch(new ChangeStructureInfoTitle({
          structId: struct.id,
          title: struct.title,
        }));
        this.structOldTitle = this.struct.title; /* Обновить старое состояние */
      }
    }
  }

  removeElement(structToRemove: StructureInfo) {
    this.workService.removeStructElement(structToRemove);
  }

  arrowMove(struct: StructureInfo, direction: EDirection) {
    // Пока что баг получается. На какой элемент мышкой кликнули, тот и в struct всё-время попадает.
    const activeStructByArrow = this.workService.getStructByArrowMove(struct, direction);
    if (activeStructByArrow) {
      this.clickStruct(activeStructByArrow);
    }
  }

  disableSelect(struct: StructureInfo) {
    // TODO mousedown никак не повлияет на перетаскивание(move)?
    return !struct.readonly;
  }
}
