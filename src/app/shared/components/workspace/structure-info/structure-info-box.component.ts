import {Component, Input, OnInit} from '@angular/core';
import {StructureInfo} from '@shared/interfaces/work';
import {GlobalsService} from "@shared/services/globals.service";

@Component({
  selector: 'app-structure-info-box',
  template: `
      <div *ngFor="let _struct of structureInfo">
          <app-structure-info [activeStruct]="activeStruct" [struct]="_struct"></app-structure-info>
          <div *ngIf="_struct.children"
               [class.hide]="!_struct.isOpen"
               class="ls-element-children">
              <app-structure-info-box [structureInfo]="GlobalsService.structsInfoNotRemoved(_struct.children)"
                                  [activeStruct]="activeStruct"></app-structure-info-box>
          </div>
      </div>
  `,
  styleUrls: ['./structure-info.component.scss']
})
export class StructureInfoBoxComponent implements OnInit {
  @Input() public structureInfo: Array<StructureInfo>;
  @Input() public activeStruct: StructureInfo;

  public GlobalsService = GlobalsService;

  constructor() { }

  ngOnInit() { }
}
