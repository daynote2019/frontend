import {animate, state, style, transition, trigger} from "@angular/animations";

export function fadeInAnimation() {  // https://www.freecodecamp.org/news/how-to-use-animation-with-angular-6-675b19bc3496/
  return [
    trigger('fadeIn', [
      state('void', style({
        opacity: 0
      })),
      // transition('void <=> *', animate(200)),
      transition(':enter', [
        style({ opacity: 0 }),
        animate('0.3s ease-in', style({ opacity: 1 }))
      ]),
    ])
    // trigger('fadeIn', [
    //   state('complete', style({
    //     'opacity': 1,
    //   })),
    //   state('incomplete', style({
    //     'opacity': 0,
    //   })),
    //   transition(':enter', [
    //     style({ opacity: 0 }),
    //     animate('0.3s ease-in', style({ opacity: 1 }))
    //   ]),
    //   transition(':leave', [
    //     animate('0.3s ease-out', style({ opacity: 0 }))
    //   ]),
    // ])
  ]
}

export function fadeInOutAnimation() {
  return [
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition(':enter', [
        style({ opacity: 0 }),
        animate('0.3s ease-in', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        animate('0.2s ease-out', style({ opacity: 0 }))
      ]),
    ])
  ]
}
