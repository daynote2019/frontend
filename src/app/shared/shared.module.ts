import { NgModule } from '@angular/core';
import 'hammerjs/hammer';
import { CommonModule } from '@angular/common';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {TranslateModule} from '@ngx-translate/core';
import { CustomMatSelectComponent } from '@shared/components/custom-mat-select/custom-mat-select.component';
import {StructureInfoComponent} from '@shared/components/workspace/structure-info/structure-info.component';
import {StructureInfoBoxComponent} from '@shared/components/workspace/structure-info/structure-info-box.component';
import {FormsModule} from '@angular/forms';
import { ModalBasicCenterComponent } from './components/modals/modal-basic-center/modal-basic-center.component';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule, MatInputModule, MatSelectModule} from '@angular/material';
import {NgxMatSelectSearchModule} from 'ngx-mat-select-search';
import { ReactiveFormsModule } from '@angular/forms';
import {MatTooltipModule} from '@angular/material/tooltip';
import { ModalInfoBottomRightComponent } from './components/modals/modal-info-bottom-right/modal-info-bottom-right.component';
import { ModalRightClickComponent } from './components/modals/modal-right-click/modal-right-click.component';
import { ModalManagementComponent } from './components/modals/modal-management/modal-management.component';

@NgModule({
  declarations: [
    StructureInfoComponent,
    ModalBasicCenterComponent,
    ModalInfoBottomRightComponent,
    ModalRightClickComponent,
    StructureInfoBoxComponent,
    CustomMatSelectComponent,
    ModalManagementComponent
  ],
  imports: [
    TranslateModule,
    MatButtonModule,
    CommonModule,
    AngularFontAwesomeModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    NgxMatSelectSearchModule,
    ReactiveFormsModule,
    MatTooltipModule
  ],
  exports: [
    StructureInfoComponent,
    ModalBasicCenterComponent,
    ModalInfoBottomRightComponent,
    ModalRightClickComponent,
    StructureInfoBoxComponent,
    MatFormFieldModule,
    MatInputModule,
    CustomMatSelectComponent,
    MatTooltipModule,
    ModalManagementComponent
  ]
})
export class SharedModule { }
