import {Injectable, OnDestroy, OnInit} from '@angular/core';
import {
  AdditionalWorkInfo,
  ILeftSidebarFiller,
  InternalUsers, IRemoveSectionInfoServer,
  ISectionInfo, ISectionInfoWithActiveOperation, ISelectActiveInternalUser,
  ISetStructureInfoToActiveState,
  IStructActionInfo,
  ITag,
  StructureInfo,
  Tab
} from '@shared/interfaces/work';
import {IStructureInfoResponse} from '@shared/interfaces/response';
import {HelpersService} from '@shared/services/helpers.service';
import {hostServer} from '../../../environments/environment';
import {catchError} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {ApiService} from '@shared/services/api.service';
import {CustomHttpService} from '@shared/services/custom-http.service';
import {select, Store} from '@ngrx/store';
import * as workRoot from '@app/root-store/work/reduser';
import * as authRoot from '@app/root-store/auth/reduser';
import {CustomTranslateService} from '@shared/services/custom-translate.service';
import {ELeftToolbarPressedButton, StructureInfoType} from '@shared/enums/work';
import {GlobalsService} from '@shared/services/globals.service';
import {
  ICloseTabRequest, IExpandParentsRequest,
  IRemoveStructureInfoRequest, ISelectActiveStructInfo,
  IStructureInfoRequest,
  IStructureInfoTitleRequest,
  ITagChangeRequest
} from '@shared/interfaces/request';
import {BehaviorSubject} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';
import {SessionService} from '@shared/services/session.service';
import {SessionUser} from '@shared/enums/sessions';
import {
  AddNewStructureInfoServerAction,
  OpenTabAction,
  RecoverStructureInfoServerAction,
  RemoveStructureInfoServerAction,
  SelectActiveStructAction, SelectActiveStructServerAction,
  SetTabsAction
} from '@app/root-store/work/actions';
import {ModalBasicCenterAction, ModalInfoBottomRightAction} from '@app/root-store/common/actions';
import {EDirection, IModalBasicCenterType} from '@shared/enums/common';
import * as mainRoot from '@app/root-store/common/reduser';
import {TagModel} from '@shared/models/TagModel';
import {IProfileInfo} from '@shared/interfaces/profile';
import {IAllAndActiveStructsSerializeResult} from "@shared/interfaces/common";

@Injectable({
  providedIn: 'root'
})
export class WorkService implements OnInit, OnDestroy {

  private profileInfoSubscription;
  private activeInternalUserSubscription;
  private activeStructSubscription;
  private activeSectionSubscription;
  private sectionInfosSubscription;
  private structureInfoSubscription;
  private tabsSubscription;
  private tagsSubscription;
  private hashWorkStructuresSubscr;

  /* Отслеживание инфо о прогресс-баре левой боковой панели */
  private subjectLeftSidebarProgressBarIsActive = new BehaviorSubject<boolean>(true);
  public leftSidebarProgressBarIsActive$ = this.subjectLeftSidebarProgressBarIsActive.asObservable();

  /* Отслеживание состояния(открыта/закрыта) левой боковой панели */
  private subjectLeftSidebarIsShow = new BehaviorSubject<ILeftSidebarFiller>({
    isShowSidebar: true,
    toolbarPressedButton: ELeftToolbarPressedButton.documents
  });
  public leftSidebarIsShow$ = this.subjectLeftSidebarIsShow.asObservable();

  public profileInfo: IProfileInfo;
  public sectionInfos: Array<ISectionInfo>;
  public activeStruct: StructureInfo;
  public activeSection: ISectionInfo;
  public activeInternalUser: InternalUsers;
  public structureInfo: Array<StructureInfo>;
  public tabs: Array<Tab>;
  private tags: Array<ITag> = [];
  public lang: string;
  public hashWorkStructures: string;

  constructor(
    private helpers: HelpersService,
    private http: HttpClient,
    private customHttpService: CustomHttpService,
    private globalService: GlobalsService,
    private api: ApiService,
    private store$: Store<mainRoot.FeatureState>,
    private storeWork$: Store<workRoot.WorkState>,
    private authWork$: Store<authRoot.AuthState>,
    private customTranslate: CustomTranslateService,
    private cookie: CookieService,
    private sessionService: SessionService,
  ) {
    this.profileInfoSubscription = this.storeWork$
      .pipe(select(workRoot.selectProfileInfo))
      .subscribe(info => {
        this.profileInfo = info;
      });
    this.sectionInfosSubscription = this.storeWork$
      .pipe(select(workRoot.selectSectionInfos))
      .subscribe(info => {
        this.sectionInfos = info;
      });
    this.activeStructSubscription = this.storeWork$
      .pipe(select(workRoot.selectActiveStruct))
      .subscribe(struct => {
        this.activeStruct = struct;
      });
    this.activeSectionSubscription = this.storeWork$
      .pipe(select(workRoot.selectActiveSection))
      .subscribe(section => {
        this.activeSection = section;
      });
    this.activeInternalUserSubscription = this.storeWork$
      .pipe(select(workRoot.selectActiveInternalUser))
      .subscribe(iUser => {
        this.activeInternalUser = iUser;
      });
    this.structureInfoSubscription = this.storeWork$
      .pipe(select(workRoot.selectStructureInfo))
      .subscribe(info => {
        this.structureInfo = info;
      });
    this.tabsSubscription = this.storeWork$
      .pipe(select(workRoot.selectTabs))
      .subscribe(tabs => {
        this.tabs = tabs;
      });
    this.store$.pipe(select(mainRoot.selectLang)).subscribe((lang: string) => {
      this.lang = lang;
    });
    this.tagsSubscription = this.storeWork$
      .pipe(select(workRoot.selectTags))
      .subscribe(tags => {
        this.tags = tags;
      });
    this.hashWorkStructuresSubscr = this.authWork$
      .pipe(select(authRoot.selectHashWorkStructures))
      .subscribe(hash => {
        this.hashWorkStructures = hash;
      });
  }

  ngOnInit() { }

  ngOnDestroy() {
    this.profileInfoSubscription.unsubscribe();
    this.activeInternalUserSubscription.unsubscribe();
    this.activeSectionSubscription.unsubscribe();
    this.sectionInfosSubscription.unsubscribe();
    this.activeStructSubscription.unsubscribe();
    this.structureInfoSubscription.unsubscribe();
    this.tabsSubscription.unsubscribe();
    this.tagsSubscription.unsubscribe();
    this.hashWorkStructuresSubscr.unsubscribe();
  }

  /* Методы только для обработки на фронте */

  allAndActiveStructsSerialize(allStructureInfo: Array<IStructureInfoResponse>): IAllAndActiveStructsSerializeResult {
    const allStructureInfoResult: Array<StructureInfo> = [];
    const structureInfoResult: Array<StructureInfo> = [];
    const activeSectionId = this.activeSection ? this.activeSection.id : '';
    const sectionTabs: string[] = (this.activeSection && this.activeSection.tabs) ? this.activeSection.tabs : [];
    const tabs: Tab[] =  [];
    const activeStructId: string = (this.activeSection && this.activeSection.activeStructId) ? this.activeSection.activeStructId : '';
    let isActiveStruct = false;


    /* Действия со структурами */
    if (GlobalsService.isNotEmptyArray(allStructureInfo)) {
      allStructureInfo.forEach(struct => {
        /* Назначаем локальные поля */
        struct.readonly = true;
        struct.isFocus = false;
        /* Назначаем локальное поле с тегими(которые конвертируем в нужный вид) */
        if (struct.selectedTags && GlobalsService.isNotEmptyArray(struct.selectedTags)) {
          struct.chosenTags = this.convertTags(struct.selectedTags);
        }
        if (struct.level === 0) {
          if (struct.sectionId === activeSectionId) { // Если структура с нулевым уровнем активной секции
            structureInfoResult.push(struct);
          }
        } else { // дочерние структуры
          const parentStruct = allStructureInfoResult // Найти структуру-родителя, которая уже должна быть в массиве
            .find(_struct => _struct.id === struct.parentId);
          if (parentStruct) {
            struct.parent = parentStruct; // Ссылка на объект, чтобы можно было менять родительский объект прямо в потомке
            if (parentStruct.children && parentStruct.children.length > 0) {
              parentStruct.children.push(struct);
            } else {
              parentStruct.children = [];
              parentStruct.children.push(struct);
            }
          }
        }
        allStructureInfoResult.push(struct);
        sectionTabs.forEach(_tabStr => {
          if (_tabStr === struct.id) {
            tabs.push(this.generateTabByStruct(struct));
          }
        });
        if (activeStructId === struct.id) {
          isActiveStruct = true;
          this.storeWork$.dispatch(new SelectActiveStructAction(struct));
        }
      });
    }

    this.setLeftSidebarProgressBarState(false); /* Скрыть прогресс-бар */
    if (!isActiveStruct) { /* Если нет активной структуры */
      this.storeWork$.dispatch(new SelectActiveStructAction(undefined));
      this.storeWork$.dispatch(new SelectActiveStructAction(null)); /* Запускаю специально два раза, чтобы прослушалась подписка */
    }
    this.storeWork$.dispatch(new SetTabsAction(tabs));

    return {
      structureInfo: structureInfoResult,
      allStructureInfo: allStructureInfoResult,
    };
  }

  createNewStructureInfo(clickStructType: StructureInfoType): IStructureInfoRequest {
    const structId = GlobalsService.generateUuid();
    const activeStruct = this.activeStruct;
    const activeSectionInfo = this.activeSection;
    const sectionId = (activeStruct && activeStruct.sectionId) ? activeStruct.sectionId : activeSectionInfo.id;
    let level = 0;
    let parent: StructureInfo|null = null;
    let parentId: string|null = null;
    if (activeStruct) {
      if (activeStruct.type === StructureInfoType.folder) {
        level = activeStruct.level + 1;
        parent = activeStruct;
        parentId = (activeStruct && activeStruct.id) ? activeStruct.id : null;
      } else if (activeStruct.type === StructureInfoType.file) {
        level = activeStruct.level;
        parent = (activeStruct && activeStruct.parent) ? activeStruct.parent : null;
        parentId = (activeStruct && activeStruct.parentId) ? activeStruct.parentId : null;
      }
    }

    const newStruct: IStructureInfoRequest = {
      id: structId,
      sectionId,
      level,
      parent,
      parentId,
      clickStructType,
      title: this.customTranslate.get('work.structure.new.' + clickStructType),
    };
    if (clickStructType === StructureInfoType.file) {
      newStruct.tab = {
        docId: structId,
        docParentId: parentId,
        active: true
      };
    }

    return newStruct;
  }

  /* Установить инфо о прогресс-баре левой боковой панели */
  public setLeftSidebarProgressBarState(state: boolean) {
    this.subjectLeftSidebarProgressBarIsActive.next(state);
  }

  /* Установить состояние левой боковой панели(открыта/закрыта и нажатую кнопку: Документы, тэги и др.) */
  public setLeftSidebarState(state: ILeftSidebarFiller) {
    this.subjectLeftSidebarIsShow.next(state);
  }

  public clickStructElement(struct: StructureInfo) {
    if (struct.readonly) {
      const structToActiveState: ISelectActiveStructInfo = {
        struct
      };
      if (this.activeStruct && this.activeStruct.id === struct.id) { // Если кликнули по той же структуре на которой находимся
        if (struct.type === StructureInfoType.folder) {
          // Если был клик по выбранной папке, то только открываем/закрываем её
          struct.isOpen = !struct.isOpen;
          structToActiveState.info = struct.isOpen ? '1' : '0';
          structToActiveState.isOpenFolder = true;
        }
      } else {
        if (struct.type === StructureInfoType.file) {
          this.storeWork$.dispatch(new OpenTabAction({
            docId: struct.id,
            docParentId: struct.parentId,
            struct : struct
          }));
        }
      }
      this.storeWork$.dispatch(new SelectActiveStructServerAction(structToActiveState));
    }
  }

  public removeStructElement(structToRemove: StructureInfo) {
    if (structToRemove && structToRemove.id) {
      this.store$.dispatch(new ModalBasicCenterAction({
        state: true,
        type: IModalBasicCenterType.deleteStruct,
        title: 'modalBasicCenter.moveToTrash.title',
        message: 'modalBasicCenter.moveToTrash.message',
        elementName: structToRemove.title,
        buttonActionName: 'modalBasicCenter.moveToTrash.buttonActiveName',
        action: () => {
          this.storeWork$.dispatch(new RemoveStructureInfoServerAction({
            structId: structToRemove.id,
            parentId: structToRemove.parentId,
            structType: structToRemove.type,
            success: () => {
              setTimeout(() => { /* Восстановление */
                this.store$.dispatch(new ModalInfoBottomRightAction({
                  state: true,
                  type: IModalBasicCenterType.recoverStruct,
                  elementName: structToRemove.title,
                  message: 'modalBasicCenter.moveToTrash.messageForMoved',
                  action: () => {
                    structToRemove.active = false;
                    this.storeWork$.dispatch(new RecoverStructureInfoServerAction({
                      structId: structToRemove.id,
                      parentId: structToRemove.parentId,
                      structType: structToRemove.type,
                    }));
                  }
                }));
              }, 300)
            }
          }));
        }
      }));
    }
  }

  addNewStructElement(type: StructureInfoType) {
    const struct: IStructureInfoRequest = this.createNewStructureInfo(type);
    this.storeWork$.dispatch(new AddNewStructureInfoServerAction({
      struct: struct,
      success: () => {}
    }));
  }

  getStructByArrowMove(struct: StructureInfo, direction: EDirection) {
    let resultStruct = null;
    const find = this.helpers.findInTree('children');
    const _struct: StructureInfo = find(this.structureInfo, 'id', struct.id);
    if (_struct) {
      if (_struct.parent) {
        const parent = _struct.parent;
        const children = parent.children;
        const length = parent.children.length - 1;
        const currentIndex = HelpersService.getIndexOfStructChildren(children, _struct.id);
        if (direction === EDirection.up && currentIndex === 0) {
          resultStruct = parent;
        } else if (direction === EDirection.down && currentIndex === length) {
          // Нужно найти соседа снизу родителя
        } else if (direction === EDirection.up) {
          resultStruct = children[currentIndex - 1];
        } else if (direction === EDirection.down) {
          resultStruct = children[currentIndex + 1];
        }
      }
    }
    return resultStruct;
  }

  public convertTags(selectedTags: Array<string>): Array<ITag> {
    const result: Array<ITag> = [];
    if (this.tags && GlobalsService.isNotEmptyArray(this.tags)) {
      this.tags.forEach(_tag => {
        const findTag = selectedTags.find(_tagId => _tagId === _tag.id);
        if (findTag) {
          result.push(new TagModel(_tag).deserialize());
        }
      });
    }

    return result;
  }

  public generateTabByStruct(struct: StructureInfo): Tab {
    return { docId: struct.id, docParentId: struct.parentId, struct };
  }

  /* Методы с HTTP Request */

  getAdditionalWorkInfo() {
    return this.http.post(
      hostServer + 'api/get-additional-work-info',
      {
        account: this.cookie.get(SessionUser.account)
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  addNewStructureInfo(newStruct: IStructureInfoRequest) {
    return this.http.post(
      hostServer + 'api/add-new-structure-info',
      {
        account: this.cookie.get(SessionUser.account),
        sectionId: newStruct.sectionId,
        level: newStruct.level,
        id: newStruct.id,
        parentId: newStruct.parentId,
        clickStructType: newStruct.clickStructType,
        title: newStruct.title
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  selectActiveStructInfo(payload: ISelectActiveStructInfo) {
    const _struct = payload.struct;
    return this.http.post(
      hostServer + 'api/select-active-structure-info',
      {
        account: this.cookie.get(SessionUser.account),
        sectionId: this.activeSection ? this.activeSection.id : null,
        structId: _struct ? _struct.id : null,
        clickStructType: _struct ? _struct.type : null,
        info: payload.info ? payload.info : null,
        isOpenFolder: payload.isOpenFolder ? payload.isOpenFolder : false
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  setStructureInfoToActiveState(payload: ISetStructureInfoToActiveState) {
    return this.http.post(
      hostServer + 'api/set-struct-state',
      payload,
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  removeStruct(payload: IRemoveStructureInfoRequest) {
    return this.http.post(
      hostServer + 'api/remove-struct',
      {
        account: this.cookie.get(SessionUser.account),
        sectionId: this.activeSection ? this.activeSection.id : null,
        structId: payload.structId,
        parentId: payload.parentId,
        structType: payload.structType,
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  recoverStruct(payload: IRemoveStructureInfoRequest) {
    return this.http.post(
      hostServer + 'api/recover-struct',
      {
        account: this.cookie.get(SessionUser.account),
        structId: payload.structId,
        parentId: payload.parentId,
        structType: payload.structType,
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  changeStructureInfoTitle(payload: IStructureInfoTitleRequest) {
    return this.http.post(
      hostServer + 'api/change-struct-title',
      {
        account: this.cookie.get(SessionUser.account),
        structId: payload.structId,
        title: payload.title,
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  expandParents(payload: IExpandParentsRequest) {
    return this.http.post(
      hostServer + 'api/expand-parents',
      {
        account: this.cookie.get(SessionUser.account),
        sectionId: this.activeSection ? this.activeSection.id : null,
        closedParentsIds: payload.closedParentsIds
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  closeTab(payload: ICloseTabRequest) {
    return this.http.post(
      hostServer + 'api/close-tab',
      {
        account: this.cookie.get(SessionUser.account),
        sectionId: this.activeSection ? this.activeSection.id : null,
        tabIdForClose: payload.tab.docId,
        isSwitchToAnotherStruct: payload.isSwitchToAnotherStruct,
        newActiveStructId: (payload.newActiveStruct && payload.newActiveStruct.id) ? payload.newActiveStruct.id : null
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  createNewSection(name: string) {
    return this.http.post(
      hostServer + 'api/create-new-section-info',
      {
        account: this.cookie.get(SessionUser.account),
        activeInternalUserId: this.activeInternalUser ? this.activeInternalUser.id : '',
        sectionName: name
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }
  selectSection(payload: ISectionInfo) {
    return this.http.post(
      hostServer + 'api/select-section-info',
      {
        account: this.cookie.get(SessionUser.account),
        activeInternalUserId: this.activeInternalUser ? this.activeInternalUser.id : '',
        sectionId: payload.id ? payload.id : ''
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  renameSection(section: ISectionInfo) {
    return this.http.post(
      hostServer + 'api/rename-section-info',
      {
        account: this.cookie.get(SessionUser.account),
        sectionId: section.id,
        sectionName: section.name
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  removeSection(payload: IRemoveSectionInfoServer) {
    return this.http.post(
      hostServer + 'api/remove-section-info',
      {
        account: this.cookie.get(SessionUser.account),
        sectionId: payload.section ? payload.section.id : '',
        newActiveSectionId: payload.newActiveSectionId,
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  recoverSection(section: ISectionInfo) {
    return this.http.post(
      hostServer + 'api/recover-section-info',
      {
        account: this.cookie.get(SessionUser.account),
        sectionId: section.id,
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  selectActiveInternalUser(payload: ISelectActiveInternalUser) {
    return this.http.post(
      hostServer + 'api/select-active-internal-user',
      {
        account: this.cookie.get(SessionUser.account),
        internalUserId: payload.iUser.id,
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  createInternalUser(user: InternalUsers) {
    return this.http.post(
      hostServer + 'api/create-internal-user',
      {
        account: this.cookie.get(SessionUser.account),
        internalUserName: user.name,
        internalUserIcon: user.icon,
        lang: this.lang
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  changeTagName(payload: ITagChangeRequest) {
    return this.http.post(
      hostServer + 'api/change-tag-name',
      {
        account: this.cookie.get(SessionUser.account),
        tagId: payload.tagId,
        newTagName: payload.name
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }
}
