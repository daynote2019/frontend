import { Injectable } from '@angular/core';
import {of} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CentralModalAlertType, HttpResponseType, HttpStatus, Pages} from '@shared/enums/common';
import {CentralModalAlertSelectAction, SpinnerCenterSelectAction, SpinnerTopSelectAction} from '@app/root-store/common/actions';
import {select, Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';
import * as errorRoot from '@app/root-store/errors/reduser';
import {ResponseErrorValidateAction} from '@app/root-store/errors/actions';
import {ResponseResultErrors} from '@shared/interfaces/errors';
import {NavService} from '@shared/services/nav.service';
import {SessionService} from '@shared/services/session.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  public httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private cookie: CookieService,
    protected storeMain$: Store<mainRoot.FeatureState>,
    private storeError$: Store<errorRoot.ErrorsState>,
    private navService: NavService,
    private sessionService: SessionService,
  ) { }

  public catchError(err) {
    console.log('ApiService.catchError', err);
    if (err.status === HttpStatus.Unauthorized) { /* Если пользователь неавторизован */
      this.notAuthorize();
    } else if (err.status === HttpStatus.InternalServerError) { /* InternalServerError, сюда так же входит исключения OperationException */
      console.log('Ошибка с сервера: ', HttpStatus.InternalServerError);
      const errResult: ResponseResultErrors = err.error;
      console.log('resultType', errResult.resultType);
      if (errResult && errResult.resultType) {
        if (errResult.resultType === HttpResponseType.ResponseUserIsNotAuthorize || // пользователь НЕ авторизован
          errResult.resultType === HttpResponseType.ResponseUserIsNotExistType) { // или пользователь НЕ существует
          this.notAuthorize();
        } else if (errResult.resultType === HttpResponseType.ResponseErrorValidateType || // Если ошибки валидации
          errResult.resultType === HttpResponseType.ResponseUserIsExistType) { // или пользователь существует
          this.closeSpinnerWithAction(() => {
            this.storeError$.dispatch(new ResponseErrorValidateAction(errResult.errors));
          });
        } else if (errResult.resultType === HttpResponseType.ResponseErrorMailerType) { // Если ошибка при отправке mail
          this.closeSpinnerWithAction(() => {
            const message = (errResult.errors && errResult.errors.mailer) ? errResult.errors.mailer : 'errors.mailer.fail';
            this.storeMain$.dispatch(new CentralModalAlertSelectAction({
              state: true,
              type: CentralModalAlertType.error,
              title: 'errors.mailer.title',
              message,
              button: 'errors.unknown.button',
            }));
          });
        }
      } else {
        this.unknownError();
      }
    } else { // Неизвестная ошибка
      console.log('Unknown errors.', err);
      this.unknownError();
    }
    return of<Response>();
  }

  public unknownError() {
    this.closeSpinnerWithAction(() => {
      this.storeMain$.dispatch(new CentralModalAlertSelectAction({
        state: true,
        type: CentralModalAlertType.error,
        title: 'errors.unknown.title',
        message: 'errors.unknown.message',
        button: 'errors.unknown.button',
        isRedirectIfClickBg: true,
        url: Pages.home // TODO Проверить, куда будем переходить, если авторизованы
      }));
    });
  }

  private closeSpinnerWithAction(action) {
    this.storeMain$.dispatch(new SpinnerTopSelectAction({
      state: false,
      action: () => action()
    }));
    this.storeMain$.dispatch(new SpinnerCenterSelectAction({
      state: false,
      action: () => action()
    }));
  }

  private notAuthorize() {
    console.log('Ошибка с сервера: ', HttpStatus.Unauthorized);
    this.sessionService.removeAuthResponseData();
    this.closeSpinnerWithAction(() => {});
    this.navService.onPageRoute(Pages.login);
    this.storeMain$.dispatch(new CentralModalAlertSelectAction({
      state: true,
      type: CentralModalAlertType.error,
      title: 'errors.userNotAuth.title',
      message: 'errors.userNotAuth.message',
      button: 'errors.userNotAuth.button',
    }));
  }
}
