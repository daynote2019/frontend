import { Injectable } from '@angular/core';
import {hostServer} from '../../../environments/environment';
import {catchError} from 'rxjs/operators';
import {HelpersService} from '@shared/services/helpers.service';
import {HttpClient} from '@angular/common/http';
import {CustomHttpService} from '@shared/services/custom-http.service';
import {ApiService} from '@shared/services/api.service';
import {select, Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';
import * as workRoot from '@app/root-store/work/reduser';
import {CustomTranslateService} from '@shared/services/custom-translate.service';
import {CookieService} from 'ngx-cookie-service';
import {SessionService} from '@shared/services/session.service';
import {IStateRequest, ITagForFileRequest, ITagRemoveFromAllRequest} from '@shared/interfaces/request';
import {InternalUsers, ITag} from '@shared/interfaces/work';
import {SessionUser} from "@shared/enums/sessions";

@Injectable({
  providedIn: 'root'
})
export class EditingAreaService {
  private activeInternalUserSubscription;

  public activeInternalUser: InternalUsers;

  constructor(
    private helpers: HelpersService,
    private http: HttpClient,
    private customHttpService: CustomHttpService,
    private storeWork$: Store<workRoot.WorkState>,
    private api: ApiService,
    private cookie: CookieService,
  ) {
    this.activeInternalUserSubscription = this.storeWork$
      .pipe(select(workRoot.selectActiveInternalUser))
      .subscribe(iUser => {
        this.activeInternalUser = iUser;
      });
  }

  ngOnDestroy() {
    this.activeInternalUserSubscription.unsubscribe();
  }

  changeStructInfoFavouritesStateOnServer(stateRequest: IStateRequest) {
    return this.http.post(
      hostServer + 'api/change-struct-info-favourites-state',
      stateRequest,
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  addTagOnServer(payload: ITagForFileRequest) {
    return this.http.post(
      hostServer + 'api/add-tag-to-file',
      Object.assign(payload, {account: this.cookie.get(SessionUser.account)}),
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  removeTagFromFileOnServer(payload: ITagForFileRequest) {
    return this.http.post(
      hostServer + 'api/remove-tag-from-file',
      Object.assign(payload, {account: this.cookie.get(SessionUser.account)}),
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  removeTagFromAllOnServer(tagReq: ITagRemoveFromAllRequest) {
    return this.http.post(
      hostServer + 'api/remove-tag-from-all',
      {
        account: this.cookie.get(SessionUser.account),
        activeInternalUserId: this.activeInternalUser ? this.activeInternalUser.id : '',
        tagId: tagReq.tagId
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }
}
