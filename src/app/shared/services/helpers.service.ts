import {Injectable} from '@angular/core';
import {ISetStructureInfoToActiveState, IStructActionInfo, ITag, StructureInfo, Tab} from '@shared/interfaces/work';
import {WorkState} from '@app/root-store/work/reduser';
import {ITagChangeRequest} from '@shared/interfaces/request';
import {GlobalsService} from "@shared/services/globals.service";
import {StructureInfoType} from "@shared/enums/work";

@Injectable({
  providedIn: 'root'
})
export class HelpersService {

  /**
   * ВАЖНО!!! В конструктор ничего не передавать!!!
   * Этот класс создаётся не только в конструкторе сервисов, но и при помощи new в redusers методах
   */
  constructor() { }

  /** Найти в дереве элемент и вернуть его */
  public findInTree(subKey) {
    function finder(items, key, value) {
      let foundValue;

      for (const item of items) {
        if (item[key] === value) {
          foundValue = item;
          break;
        }
      }

      if (typeof foundValue === 'undefined') {
        for (const item of items) {
          if (item[subKey] && item[subKey].length > 0) {
            foundValue = finder(item[subKey], key, value);
            if (foundValue) {
              break;
            }
          }
        }
      }

      return foundValue;
    }

    return finder;
  }

  /** Изменить статус(ы) структур информации и вернуть измененный массив */
  public setStructureInfoToActiveStateHelper(state: WorkState,
                                             structAction: ISetStructureInfoToActiveState): StructureInfo[] {
    return state.allStructureInfo.map((struct: StructureInfo) => {
      if (state.activeSection.id === struct.sectionId) { // Если структура из текущей-активной секции
        if (struct.id === structAction.structId) {
          if (structAction.isOpenFolder) {
            struct.isOpen = !struct.isOpen;
          } else {
            struct.active = true;
            state.activeStruct = struct;
          }
        } else {
          if (!structAction.isOpenFolder) {
            struct.active = false;
          }
        }
      }
      return struct;
    });
  }

  /** Удалить структуру информации */
  public removeStructureInfo(structId: string, state: WorkState) {
    function finder(structureInfos: Array<StructureInfo>): Array<StructureInfo> {
      structureInfos = structureInfos.filter(struct => {
        if (struct.id === structId) {
          state.recoverStruct = struct;
          if (struct.parent) {
            const parent = struct.parent;
            parent.children = struct.parent.children
              .filter(s => s.id !== structId);
            parent.active = true;
            state.activeStruct = parent;
            return struct;
          } else {
            state.activeStruct = null;
          }
        } else {
          return struct;
        }
      });

      for (const struct of structureInfos) {
        if (struct.children && struct.children.length > 0) {
          finder(struct.children);
        }
      }

      return structureInfos;
    }

    return finder;
  }

  /** Закрыть вкладки всех дочерних файлов */
  closeTabsForAllChildrenDocs(state: WorkState): (childrens: Array<StructureInfo>) => Array<Tab> {
    let tabs = state.tabs;
    function finder(childrens: Array<StructureInfo>): Array<Tab> {
      for (const struct of childrens) {
        tabs = tabs.filter(t => t.docId !== struct.id);
      }

      for (const struct of childrens) {
        if (struct.children && struct.children.length > 0) {
          finder(struct.children);
        }
      }

      return tabs;
    }

    return finder;
  }

  /** Сбросить активный статус выбранной структуры информации */
  public deselectStructureInfoActiveState() {
    function finder(structureInfos: Array<StructureInfo>): Array<StructureInfo> {
      for (const struct of structureInfos) {
        struct.active = false;
      }

      for (const struct of structureInfos) {
        if (struct.children && struct.children.length > 0) {
          finder(struct.children);
        }
      }

      return structureInfos;
    }

    return finder;
  }

  /* Найти всех детей структуры инфо */
  public findChildrenStructInfos() {
    const childrenPull: Array<StructureInfo> = [];
    function finder(structInfo: Array<StructureInfo>) {
      structInfo.forEach(struct => {
        if (struct.type === StructureInfoType.folder) {
          struct.isOpen = false;
        }
        childrenPull.push(struct);
        if (struct.children && struct.children.length > 0) {
          finder(struct.children);
        }
      });

      return childrenPull;
    }

    return finder;
  }

  /* Найти всех детей структуры инфо в сессии */
  public findSessionChildrenStructInfos() {
    const childrenPull: Array<IStructActionInfo> = [];
    function finder(structInfo: Array<StructureInfo>) {
      structInfo.forEach(struct => {
        childrenPull.push({id: struct.id, parentId: struct.parentId, active: struct.active, type: struct.type});
        if (struct.children && struct.children.length > 0) {
          finder(struct.children);
        }
      });

      return childrenPull;
    }

    return finder;
  }

  /* Найти всех родителей */
  public findParents(structureInfo: Array<StructureInfo>) {
    const _this = this;
    const parentsPull: Array<IStructActionInfo> = [];
    const action = (parent: StructureInfo) => {
      parentsPull.push({id: parent.id, parentId: parent.parentId, active: parent.active, type: parent.type});
      finder(parent);
    };
    function finder(struct: StructureInfo) {
      if (struct && struct.parent) {
        const parent = struct.parent;
        action(parent);
      } else if (struct && struct.parentId) {
        const find = _this.findInTree('children');
        const parent = find(structureInfo, 'id', struct.parentId);
        if (parent) {
          action(parent);
        }
      }

      return parentsPull;
    }

    return finder;
  }

  /* COMPARE */

  public static compareStructs(a: StructureInfo, b: StructureInfo) {
    if ( a.title < b.title ) {
      return -1;
    }
    if ( a.title > b.title ) {
      return 1;
    }
    return 0;
  }

  public static compareTags(a: ITag, b: ITag) {
    if ( a.name < b.name ) {
      return -1;
    }
    if ( a.name > b.name ) {
      return 1;
    }
    return 0;
  }

  public static getIndexOfStructChildren(children: StructureInfo[], structId: string): number {
    let index = null;
    for (let key in children) {
      if (children[key].id === structId) {
        index = parseInt(key, 10);
      }
    }

    return index;
  }

  /** Удалить тэг у всех структур инфо */
  public removeOneTagFromAllStructInfo(tagId: string) {
    function finder(structureInfo: Array<StructureInfo>): Array<StructureInfo> {
      for (const struct of structureInfo) {
        if (struct.chosenTags) {
          struct.chosenTags = struct.chosenTags.filter(_tag => _tag.id !== tagId);
        }
      }

      for (const struct of structureInfo) {
        if (struct.children && struct.children.length > 0) {
          finder(struct.children);
        }
      }

      return structureInfo;
    }

    return finder;
  }

  /** Поменять название тега у всех структур инфо */
  public changeTagNameFromAllStructInfo(tagInfo: ITagChangeRequest,
                                        allStructureInfo: Array<StructureInfo>) {
    return allStructureInfo.map(_struct => {
      if (_struct.chosenTags) {
        _struct.chosenTags.forEach(_tag => {
          if (_tag.id === tagInfo.tagId) {
            _tag.name = tagInfo.name;
          }
        });
      }
      return _struct;
    });
  }

  /** Выбрать структуры по имеющимся тэгам */
  selectStructuresByTags(selectedTags: Array<string>,
                         allStructures: Array<StructureInfo>): Array<StructureInfo> {
    const structureInfoByTags: Array<StructureInfo> = [];
    allStructures.forEach(_struct => {
      if (GlobalsService.isNotEmptyArray(_struct.chosenTags)) {
        _struct.chosenTags.forEach(_tag => {
          if (selectedTags.indexOf(_tag.id) !== -1) {
            const structExist = structureInfoByTags.find(_structByTags => _structByTags.id === _struct.id);
            if (!structExist) { // Исключаем дубли
              structureInfoByTags.push(_struct);
            }
          }
        });
      }
    });

    return structureInfoByTags;
  }
}
