import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {FooterInfo, PageInfo} from '@shared/interfaces/common';
import {Router} from '@angular/router';
import {Pages} from '@shared/enums/common';
import {hostClient} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NavService {

  /* Отслеживание инфо о странице */
  private subjectPageInfo = new BehaviorSubject<PageInfo>({
    isHome: true,
    title: 'page.home',
    chosen: Pages.home
  });
  public pageInfo$ = this.subjectPageInfo.asObservable();

  /* Отслеживание инфо о футере */
  private subjectFooterInfo = new BehaviorSubject<FooterInfo>({
    height: 193
  });
  public footerInfo$ = this.subjectFooterInfo.asObservable();

  constructor(private router: Router) {}

  /* Установить инфо о странице */
  public setPageInfo(pageInfo: PageInfo) {
    this.subjectPageInfo.next(pageInfo);
  }

  /* Установить инфо о футере */
  public setFooterInfo(footerInfo: FooterInfo) {
    this.subjectFooterInfo.next(footerInfo);
  }

  /* Перейти на страницу по URL */
  public onPageRoute(url: string) {
    const finalUrl = `/${url}`;
    this.router.navigateByUrl(finalUrl);
  }

  /* Сопоставить типы списка меню(берутся типы из перечисления NavListTypes) */
  public checkListType(currentType: string, listType: string) {
    return currentType === listType;
  }
}
