import {Injectable} from '@angular/core';
import {ILoginOrRegisterResponse} from '@shared/interfaces/response';
import {SessionCommon, SessionUser} from '@shared/enums/sessions';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  constructor(
    private cookie: CookieService
  ) { }

  /* Auth, request, response */

  setAuthResponseData(res: ILoginOrRegisterResponse, isAlienComp = false) {
    /* Если чужой компьютер, то запоминаем на 1 день */
    const expires = isAlienComp ? 1 : SessionCommon.expires;

    this.cookie.set(SessionUser.account, res.account, expires, '/');
    this.cookie.set(SessionUser.sessionId, res.sessionId, expires, '/');
    this.cookie.set(SessionUser.token, res.token, expires, '/');
  }

  removeAuthResponseData() {
    this.cookie.delete(SessionUser.account, '/');
    this.cookie.delete(SessionUser.sessionId, '/');
    this.cookie.delete(SessionUser.token, '/');
  }
}
