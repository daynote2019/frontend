import {Injectable} from '@angular/core';
import {catchError, tap, map} from 'rxjs/operators';
import {CookieService} from 'ngx-cookie-service';
import {FormLogin, FormRegister, FormRegisterCreateNew} from '@shared/interfaces/auth';
import {ApiService} from '@shared/services/api.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {hostServer} from '../../../environments/environment';
import {SessionUser} from '@shared/enums/sessions';
import {IApiResponse} from '@shared/interfaces/api';
import {UserAuthInfoSelectAction} from '@app/root-store/auth/actions';
import {Store} from '@ngrx/store';
import * as authRoot from '@app/root-store/auth/reduser';
import * as workRoot from '@app/root-store/work/reduser';
import {ICheckAuthResponse} from '@shared/interfaces/response';
import {SessionService} from '@shared/services/session.service';
import {NavService} from '@shared/services/nav.service';
import {Pages} from '@shared/enums/common';
import {
  SelectAdditionalWorkInfoAction,
  SetActiveInternalUserAction, SetActiveSectionAction,
  SetTabsAction,
  SetWorkInfoAction
} from '@app/root-store/work/actions';
import {WorkService} from '@shared/services/work.service';
import {Tab} from '@shared/interfaces/work';
import {CustomHttpService} from '@shared/services/custom-http.service';
import {TagModel} from '@shared/models/TagModel';
import {INetworkData} from "@shared/interfaces/common";
import {InternalUserModel} from "@shared/models/InternalUserModel";
import {SectionModel} from "@shared/models/SectionModel";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private address: INetworkData;
  public static emailMaxLength = 250;
  public static passwordMinLength = 8;
  public static passwordMaxLength = 50;
  /**
   * (?=.*[0-9]) - строка содержит хотя бы одно число;
   * (?=.*[!@#$%^&*]) - строка содержит хотя бы один спецсимвол;
   * (?=.*[a-z]) - строка содержит хотя бы одну латинскую букву в нижнем регистре;
   * (?=.*[A-Z]) - строка содержит хотя бы одну латинскую букву в верхнем регистре;
   * [0-9a-zA-Z!@#$%^&*]{6,} - строка состоит не менее, чем из 6 вышеупомянутых символов.
   */
  public static passwordPattern =
    '(?=.*[0-9])(?=.*[!@#$%^&*:;<>\?\{\}\(\)\|\/\=\+\`\~\'\"])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*:;<>\?\{\}\(\)\|\/\=\+\`\~\'\"]{8,}';
  constructor(
    private http: HttpClient,
    private api: ApiService,
    private customHttpService: CustomHttpService,
    private cookie: CookieService,
    private authMain$: Store<authRoot.AuthState>,
    private storeWork$: Store<workRoot.WorkState>,
    private sessionService: SessionService,
    private navService: NavService,
  ) { }

  /* Регистрация временного пользователя(Шаг 1) */
  public register(form: FormRegister) {
    return this.http.post(
      hostServer + 'api/register-temp',
      form,
      this.api.httpOptions
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  /* Регистрация постоянного пользователя(Шаг 2) */
  public registerCreateNew(data: FormRegisterCreateNew) {
    return this.http.post(
      hostServer + 'api/register-create-new',
      data,
      this.api.httpOptions
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  /* Авторизация (вход в систему) */
  public login(data: FormLogin) {
    const deviceInfo = this.customHttpService.getDeviceInfo();

    return this.http.post(
      hostServer + 'api/login',
      {
        isAlienComp: data.isAlienComp,
        device: {
          name: deviceInfo.name,
          browser: deviceInfo.browser,
          ipAddress: (this.address && this.address.ip) && this.address.ip || '',
          macAddress: (this.address && this.address.mac) && this.address.mac || '',
        }
      },
      {headers: this.customHttpService
          .getLoginHttpCredentialsOptions(data)}
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }

  /* Выход из системы */
  public logout() {
    return this.http.post(
      hostServer + 'api/logout',
      {
        account: this.cookie.get(SessionUser.account),
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      tap(_ => {
        this.sessionService.removeAuthResponseData();
        this.navService.onPageRoute(Pages.login);
      }),
      catchError(err => this.api.catchError(err))
    );
  }

  /* Аутентификация/авторизация */
  public checkAuth() {
    return this.http.post(
      hostServer + 'api/check-auth',
      {
        account: this.cookie.get(SessionUser.account),
      },
      {headers: this.customHttpService.getHttpCredentialsOptions()}
    ).pipe(
      tap((res: IApiResponse) => {
        const result: ICheckAuthResponse = res.result;
        console.log('checkAuth result', result);
        const activeIUser = result.internalUsers.find(_iUser => _iUser.id === result.activeInternalUserId);
        const activeSection = result.sectionInfos.find(_section => _section.id === result.activeSectionId);
        this.storeWork$.dispatch(new SetActiveInternalUserAction(activeIUser));
        this.storeWork$.dispatch(new SetActiveSectionAction(activeSection));
        setTimeout(() => {
          this.authMain$.dispatch(new UserAuthInfoSelectAction({
            email: result.email,
            account: result.account,
            device: result.device,
            employeeSettings: result.employeeSettings,
            hashWorkStructures: result.hashWorkStructures
          }));
          this.storeWork$.dispatch(
            new SetWorkInfoAction({
              employee: result.employee,
              internalUsers: result.internalUsers,
              sectionInfos: result.sectionInfos,
              tags: result.tags.map(_tag => new TagModel(_tag).deserialize()),
            })
          );
          this.storeWork$.dispatch(
            new SelectAdditionalWorkInfoAction(activeIUser)
          );
        }, 30);
      }),
      catchError(err => this.api.catchError(err))
    ).toPromise<any>();
  }

  /* Восстановление пароля TODO Это реализовано?! */
  public forgotPassword(email: string) {
    return this.http.post(
      hostServer + 'api/forgot-password',
      {email},
      this.api.httpOptions
    ).pipe(
      catchError(err => this.api.catchError(err))
    );
  }
}
