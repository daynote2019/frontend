import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {Pages} from '@shared/enums/common';
import {NavService} from '@shared/services/nav.service';
import {SessionUser} from '@shared/enums/sessions';
import {CookieService} from 'ngx-cookie-service';
import {AuthService} from '@shared/services/auth.service';
import {SpinnerCenterSelectAction, SpinnerTopSelectAction} from '@app/root-store/common/actions';
import {Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private navService: NavService,
    private cookie: CookieService,
    private authService: AuthService,
    protected storeMain$: Store<mainRoot.FeatureState>
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if (this.cookie.get(SessionUser.account) && this.cookie.get(SessionUser.token)) {
        /* Выключаем топ спиннер(во время регистрации он включается) */
        this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
        return this.authService.checkAuth().then(_ => {
          return true; // TODO с сервера нужно присылать check
        }).catch(() => {
          return this.ifNotActivate();
        });
    } else {
        return this.ifNotActivate();
      }
  }

  ifNotActivate() {
    this.storeMain$.dispatch(new SpinnerCenterSelectAction({
      state: false,
    }));
    this.navService.onPageRoute(Pages.login);

    return false;
  }
}
