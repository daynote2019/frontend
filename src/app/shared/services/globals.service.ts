import {ElementRef, Inject, Injectable, QueryList, Renderer2} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {DocumentInfo, ISelect, ModalBackground, ModalsInfo} from '@shared/interfaces/common';
import {DOCUMENT} from '@angular/common';
import {ErrorMessageFormControlField} from '@shared/enums/common';
import {FormGroup} from '@angular/forms';
import {CustomHttpService} from '@shared/services/custom-http.service';
import { v4 as uuid } from 'uuid';
import {EInternalUserIcon, StructureInfoType} from '@shared/enums/work';
import {ITag, StructureInfo} from '@shared/interfaces/work';
import {CustomTranslateService} from '@shared/services/custom-translate.service';
import {select, Store} from '@ngrx/store';
import * as workRoot from '@app/root-store/work/reduser';
import {animate, state, style, transition, trigger} from "@angular/animations";
import {IStructureInfoRequest} from "@shared/interfaces/request";

@Injectable({
  providedIn: 'root'
})
export class GlobalsService {
  public static delayTimeout = 0;
  public static matTooltipShowDelay = 100;
  public static matTooltipShowMiddleDelay = 500;
  public static matTooltipShowLongDelay = 1000;
  public static mobileNav = 40;
  public static desktopNav = 60;
  public static tabletAverage = 960;
  public static leftSidebarWidth = 210;
  public static emptyActiveStruct: StructureInfo = {
    id: '',
    sectionId: '',
    level: 0,
    parentId: '',
    type: StructureInfoType.folder,
    title: '',
    createdAt: '',
    updatedAt: '',
  };
  public internalUserIconArray: Array<ISelect> = [
    {
      name: EInternalUserIcon.account,
      title: this.customTranslate.get('user.internalUserIcon.account')
    },
    {
      name: EInternalUserIcon.business,
      title: this.customTranslate.get('user.internalUserIcon.business')
    },
    {
      name: EInternalUserIcon.common,
      title: this.customTranslate.get('user.internalUserIcon.common')
    },
    {
      name: EInternalUserIcon.music,
      title: this.customTranslate.get('user.internalUserIcon.music')
    },
  ];

  // public internalUserIconArray: Array<> = [
  //   'account-circle.svg',
  //   'business-center.svg',
  //   'group.svg',
  //   'library-music.svg',
  // ];

  /* State инфо о документе */
  private subjectDocumentInfo = new BehaviorSubject<DocumentInfo>({
    scrollTop: 0
  });
  public documentInfo$ = this.subjectDocumentInfo.asObservable();
  /* State инфо о модалках(открыты.закрыты) */
  private subjectModalsInfo = new BehaviorSubject<ModalsInfo>({
    tagsManagementModal: {
      isOpenState: false
    }
  });
  public modalsInfo$ = this.subjectModalsInfo.asObservable();
  public static generateUuid(): string {
    return uuid();
  }

  constructor(
    private customHttp: CustomHttpService,
    private customTranslate: CustomTranslateService,
    private storeWork$: Store<workRoot.WorkState>,
    @Inject(DOCUMENT) private document: Document
  ) {}

  /* Проверка, что массив НЕ пустой */
  public static isNotEmptyArray(array: any[]) {
    return array && array.length > 0;
  }

  public static structsInfoNotRemoved(strusts: StructureInfo[]) {
    return strusts.filter(_struct => !_struct.isRemoved);
  }

  /* Проверка, что объект не пустой. Если не пустой - вернёт true, если пустой - false. */
  public isNotEmptyObject(object: object|null): boolean {
    return !!object && !!Object.keys(object).length;
  }

  /* Вернуть первый элемент объекта */
  public getFirstObjectElement(object: object|null) {
    if (this.isNotEmptyObject(object)) {
      return Object.keys(object)[0];
    } else {
      return null;
    }
  }

  public static isNotNullAndUndefined(_any: any): boolean {
    return _any !== null && typeof _any !== 'undefined';
  }

  /* Скрыть/показать скролл у body */
  public fixBody(renderer: Renderer2, state: boolean) {
    if (state) { // Если state true, то скрываем скролл у body(фиксируем)
      renderer.addClass(this.document.body, 'scroll-hide');
    } else {
      renderer.removeClass(this.document.body, 'scroll-hide');
    }
  }

  /* Установить информацию о документе */
  public setDocumentInfo(documentInfo: DocumentInfo) {
    this.subjectDocumentInfo.next(documentInfo);
  }

  /* Установить информацию о модалке управления тэгами */
  public modalTagsManagementOpenClose(modalsInfo: ModalsInfo) {
    this.subjectModalsInfo.next(modalsInfo);
  }

  public getScrollTop(doc: DocumentInfo): number {
    const scrollTop = (doc.windowWidth < GlobalsService.tabletAverage) ?
      GlobalsService.mobileNav : GlobalsService.desktopNav;
    if (doc.scrollTop < scrollTop) {
      return scrollTop;
    } else {
      return doc.scrollTop;
    }
  }

  /**
   * Вернуть текст ошибки поля(FormControl) по его error коду
   * Код должен содержать ключ, напр. passwordPattern. Этот ключ будет указывать на значение 'errors.fields.notValid.passwordPattern'
   * Это значение является транслитом
   */
  public getFormControlErrorMessage(formGroup: FormGroup, formControlName: string): string {
    const formControl = formGroup.get(formControlName);
    let fieldError = this.getFirstObjectElement(formControl.errors);
    if (!fieldError) {
      return '';
    }
    /* Кастомизирование */
    if (formControlName === 'password' && fieldError === 'minlength') {
      fieldError = 'passwordMinlength';
    }
    if (formControlName === 'password' && fieldError === 'pattern') {
      fieldError = 'passwordPattern';
    }

    return ErrorMessageFormControlField[fieldError];
  }

  public isIEOrEdge() {
    const browser = this.customHttp.getDeviceInfo().browser;
    return browser.indexOf('IE') > -1 || browser.indexOf('Trident/') > -1 || browser.indexOf('Edge') > -1;
  }
  /* Найти все закрытые папки */
  public getClosedParentsIds(struct: StructureInfo): string[] {
    let result = [];
    let foundParent = struct.parent;
    while (foundParent) {
      /* Если папка закрыта, то добавляем её в массив, чтобы её потом открыть.
      Закрытые не добавляем, чтобы лишний раз не проходиться по циклу allStructs на фронте, и
      чтобы на бэке не лезть в кэш */
      if (!foundParent.isOpen) {
        result.push(foundParent.id);
      }
      foundParent = foundParent.parent;
    }

    return result;
  }

  /* Найти ref структуру-инфо из всех ref-ов по id */
  public getStructElementRefFromAllRefs(struct: StructureInfo|IStructureInfoRequest,
                                structRefElements: QueryList<ElementRef>): ElementRef {
    return structRefElements.find(el => {
      if (el.nativeElement && el.nativeElement.dataset.id) {
        return el.nativeElement.dataset.id === struct.id;
      }
    });
  }

  /* Найти ref структуру-инфо из одной ref по id */
  public getStructElementRefFromRef(struct: StructureInfo|IStructureInfoRequest,
                                structRefElement: ElementRef): ElementRef|null {
    if (structRefElement.nativeElement && structRefElement.nativeElement.dataset.id) {
      return structRefElement;
    }
    return null;
  }
}
