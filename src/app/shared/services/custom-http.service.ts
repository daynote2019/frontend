import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {DeviceDetectorService} from 'ngx-device-detector';
import {IDeviceInfo} from '@shared/interfaces/common';
import {FormLogin, UserCredentialsBase64} from '@shared/interfaces/auth';
import {SessionUser} from '@shared/enums/sessions';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class CustomHttpService {

  constructor(
    private http: HttpClient,
    private cookie: CookieService,
    private deviceService: DeviceDetectorService
  ) {}

  /** Получить аккаунт и токен в формате base64 через ":" для Authorization: Basic  */
  public getUserCredentialsBase64(): string {
    const creds: UserCredentialsBase64 = {
      sessionId: this.cookie.get(SessionUser.sessionId),
      token: this.cookie.get(SessionUser.token)
    };
    return btoa(creds.sessionId + ':' + creds.token);
  }

  getHttpCredentialsOptions(): HttpHeaders {
    const credentials: string = this.getUserCredentialsBase64();
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'WWW-Authenticate': 'Basic realm="secure site", charset="UTF-8"',
      'Authorization': 'Basic ' + credentials
    });
  }

  getLoginHttpCredentialsOptions(data: FormLogin): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'WWW-Authenticate': 'Basic realm="secure site", charset="UTF-8"',
      'Authorization': 'Basic ' + btoa(data.email + ':' + data.password)
    });
  }

  /**
   * Получить версию ОС и браузера
   * @example 'windows-10 chrome 69.0.3497.100'
   */
  getDeviceInfo(): IDeviceInfo {
    const deviceInfo = this.deviceService.getDeviceInfo();
    return {
      name: deviceInfo.os_version,
      browser: deviceInfo.browser,
    };
  }
}
