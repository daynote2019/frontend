import { Injectable } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Langs} from '@shared/interfaces/common';
import {Observable} from 'rxjs';
import { of } from 'rxjs';
import {CookieService} from 'ngx-cookie-service';
import {SessionCommon} from '@shared/enums/sessions';
import { environment } from '../../../environments/environment';
import * as en from '../../../assets/localize/en.json';
import * as ru from '../../../assets/localize/ru.json';

const dynamicLocal: any = {
  en,
  ru
};

@Injectable({
  providedIn: 'root'
})
export class CustomTranslateService {
  private langs: Array<Langs> = [
    {
      code: 'ru',
      name: 'РУС',
      title: 'Русский',
      img: 'ru_24.png'
    },
    {
      code: 'en',
      name: 'ENG',
      title: 'English',
      img: 'us_24.png'
    }
  ];

  constructor(
    private translate: TranslateService,
    private cookie: CookieService,
  ) { }

  public getLangName(lang?: string): string {
    const chooseLang = lang ? lang : this.translate.currentLang;
    return this.langs.find(l =>
      l.code === chooseLang).name;
  }

  public getLangByField(field: string, lang?: string): string {
    const chooseLang = lang ? lang : this.translate.currentLang;
    return this.langs.find(l =>
      l.code === chooseLang)[field];
  }

  public getLangs() {
    return this.langs;
  }

  public setLang(lang: string): Observable<any> {
    this.translate.use(lang);
    this.cookie.set(SessionCommon.lang, lang, SessionCommon.expires, '/');
    return of<Response>();
  }

  /* Получить перевод по ключу */
  public get(key: string) {
    let locale = this.translate.currentLang;
    locale = environment.supportedLanguages.find(loc =>
      loc === locale);
    if (!locale) {
      locale = environment.defaultLanguage;
    }
    let result = dynamicLocal[locale];
    if (!result.default) {
      return '';
    }
    result = result.default;
    key.split('.').map(index => {
      result = result[index];
    });

    return result;
  }
}
