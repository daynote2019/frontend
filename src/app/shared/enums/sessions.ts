export enum SessionCommon {
  expires = 30, // days
  lang = 'sdLang',
}
export enum SessionUser {
  account = 'sdUser.account',
  sessionId = 'sdUser.sessionId',
  token = 'sdUser.token',
  internalUser = 'sdUser.internalUser',
  internalUserElements = 'sdUser.elements',
}
export enum SessionSection {
  sectionElements = 'section.elements',
}
export enum SessionStruct {
  activeStruct = 'structureInfo.activeStruct',
  activeSectionInfo = 'structureInfo.activeSectionInfo',
}
