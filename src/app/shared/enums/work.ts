export enum EStructureInfoField {
  isFavourites = 'isFavourites',
  readonly = 'readonly',
  isFocus = 'isFocus',
}
export enum StructureInfoType {
  start = 'start',
  folder = 'folder',
  file = 'file',
}
export enum IFolderControlType {
  expandParents = 'expandParents',
}
export enum ETagType {
  file = 'file',
  text = 'text',
}
export enum EInternalUserIcon {
  account = 'account-circle.svg',
  business = 'business-center.svg',
  common = 'group.svg',
  music = 'library-music.svg',
}
export enum ELeftToolbarPressedButton {
  documents = 'documents',
  tags = 'tags',
  favourites = 'favourites',
  search = 'search',
  basket = 'basket',
}
