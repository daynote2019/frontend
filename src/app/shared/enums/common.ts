export enum NavListTypes {
  arrow = 'arrow',
  link = 'link',
  line = 'line',
  function = 'function',
}
export enum Pages {
  work = '',
  home = 'main/home',
  contacts = 'main/contacts',
  demo = 'main/demo',
  login = 'main/login',
  registration = 'main/registration',
  forgotPassword = 'main/forgot-password',
  about = 'main/about',
  history = 'main/history',
  how = 'main/how',
  profile = 'user/profile',
  settings = 'user/settings',
}
export enum CentralModalAlertType {
  success = 'success',
  error = 'error',
  info = 'info',
}
export enum IModalBasicCenterType {
  deleteStruct = 'deleteStruct',
  recoverStruct = 'recoverStruct',
  success = 'success',
  info = 'info',
  confirm = 'confirm',
  confirmInput = 'confirmInput',
  confirmInputWithSelect = 'confirmInputWithSelect',
}
export enum HttpStatus {
  InternalServerError = 500,
  Unauthorized = 401,
}
export enum HttpResponseType {
  ResponseErrorValidateType = 'type.error.validate',
  ResponseUserIsExistType = 'type.error.user.is.exist',
  ResponseUserIsNotExistType = 'type.error.user.is.not.exist',
  ResponseUserIsNotAuthorize = 'type.error.user.is.not.authorized',
  ResponseErrorMailerType = 'type.error.mailer',
  ResponseErrorUnknownType = 'type.error.unknown',
}
export enum ErrorMessageFormControlField {
  required = 'errors.fields.notBlank',
  email = 'errors.fields.notValid.email',
  emailUnique = 'errors.fields.emailUnique',
  userNotFound = 'errors.fields.notValid.userNotFound',
  minlength = 'errors.fields.notValid.minlength',
  maxlength = 'errors.fields.notValid.maxlength',
  passwordMinlength = 'errors.fields.notValid.passwordMinlength',
  passwordPattern = 'errors.fields.notValid.passwordPattern',
  wrongPassword = 'errors.fields.notValid.wrongPassword',
}
export enum AlertTypes {
  fields = 'fields',
  centralModal = 'centralModal',
}
export enum EDirection {
  up = 'UP',
  left = 'LEFT',
  down = 'DOWN',
  right = 'RIGHT',
}
