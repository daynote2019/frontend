import {ELeftToolbarPressedButton, ETagType, StructureInfoType} from '@shared/enums/work';
import {IEmployee} from '@shared/interfaces/profile';
import {ISessionInternalUserWithElems} from "@shared/interfaces/sessions";

export interface WorkInfo {
  employee?: IEmployee;
  internalUsers?: Array<InternalUsers>;
  sectionInfos?: Array<ISectionInfo>;
  structureInfo?: Array<StructureInfo>;
  allSectionInfos?: Array<ISectionInfo>;
  allStructureInfo?: Array<StructureInfo>;
  activeStruct?: StructureInfo;
  tags?: Array<ITag>;
}
export interface AdditionalWorkInfo {
  activeSectionId?: string;
}
export interface InternalUsers {
  id: string;
  name: string;
  icon: string;
  active?: boolean;
  isRemoved?: boolean;
  activeSectionId?: string // Из кук
}
export interface ISectionInfo {
  id: string;
  name: string;
  active?: boolean;
  isRemoved?: boolean; // Удалена из дерева
  internalUserId?: string;
  tabs?: string[], // Из кук
  activeStructId?: string // Из кук
}
export interface IUpdateWorkAreaElements {
  section: ISectionInfo,
  tabs: string[],
  activeStructId: string
}
export interface StructureInfo {
  id: string;
  sectionId: string;
  level: number;
  parentId: string|null;
  parent?: StructureInfo|null;
  type: StructureInfoType;
  readonly?: boolean; /* Если TRUE - нельзя редактировать */
  isFocus?: boolean;
  isRemoved?: boolean; // Удален из дерева
  active?: boolean;
  isOpen?: boolean;
  children?: Array<StructureInfo>|null;
  title: string;
  body?: string;
  chosenTags?: Array<ITag>;
  isFavourites?: boolean;
  createdAt: string;
  updatedAt: string;
}

export interface IStructActionInfo {
  id: string,
  parentId: string,
  active: boolean,
  type: StructureInfoType,
  isOpen?: boolean,
}
export interface ISetStructureInfoToActiveState {
  structId?: string;
  parentId?: string;
  type?: StructureInfoType;
  isOpenFolder?: boolean;
  isOpen?: boolean|null;
  state: boolean;
  tab?: Tab;
  activeStruct?: StructureInfo;
}
export interface Tab {
  docId: string;
  docParentId: string;
  active?: boolean;
  closeAll?: boolean;
  struct?: StructureInfo;
}
export interface IAddNewStructureInfo {
  clickStructType: StructureInfoType;
}
export interface ITag {
  id: string;
  name: string;
  type: ETagType;
  isSelected?: boolean;
  readonly?: boolean;
}
export interface ILeftSidebarFiller {
  isShowSidebar: boolean,
  toolbarPressedButton: ELeftToolbarPressedButton,
}
export interface IRemoveSectionInfoServer {
  section: ISectionInfo,
  newActiveSectionId: string|null,
  success: () => void,
}
export interface IRemoveStructureInfo {
  allStructureInfo: StructureInfo[],
  childrenOfStructureInfo: StructureInfo[]|null,
  activeStruct: StructureInfo|null
}
export interface ISelectActiveInternalUser {
  iUser: InternalUsers
}
export interface ISectionInfoWithActiveOperation {
  sections: ISectionInfo[],
  activeSection: ISectionInfo,
}

