import {ETagType, IFolderControlType, StructureInfoType} from '@shared/enums/work';
import {ITag, StructureInfo, Tab} from '@shared/interfaces/work';

export interface IDeviceResponse {
  name: string;
  browser: string;
  ipAddress: string;
  macAddress: string;
}
export interface IStructureInfoRequest {
  id: string;
  sectionId: string;
  level: number;
  parentId: string|null;
  parent: StructureInfo;
  clickStructType: StructureInfoType;
  title: string;
  isOpen?: boolean;
  tab?: Tab;
  type?: StructureInfoType;
}
export interface IStructureInfoServerRequest {
  struct: IStructureInfoRequest,
  success: () => void
}
export interface IStructureInfoTitleRequest {
  structId: string;
  title: string;
}
export interface IStructureInfoAnyValue {
  structId: string;
  fields: {name: string; value: any;}[]
}
export interface IFolderControlRequest {
  structId: string;
  parentId: string;
  action: IFolderControlType;
}
export interface IExpandParentsRequest {
  struct: StructureInfo;
  closedParentsIds: string[]
}
export interface IRemoveStructureInfoRequest {
  structId: string|null;
  parentId: string|null;
  structType: StructureInfoType|null;
  success?: () => void
}
export interface IRecoverStructureInfoRequest {
  structId: string|null;
  parentId: string|null;
  structType: StructureInfoType|null;
}
export interface IStateRequest {
  elementId: string;
  state: boolean;
}
export interface ITagForFileRequest {
  structId: string;
  tagId?: string;
  name: string;
  type: ETagType;
}
export interface ITagChangeRequest {
  tagId: string;
  name: string;
}
export interface ITagRemoveFromAllRequest {
  tagId: string;
}
export interface IChooseTagRequest {
  selectedTags: Array<string>;
}
export interface ISelectActiveStructInfo {
  struct: StructureInfo|null;
  info?: string;
  isOpenFolder?: boolean; /* TRUE - значит только лишь открыли|закрыли папку. Т.е. не нужно делать активным эту структуру */
}
export interface ICloseTabRequest {
  tab: Tab,
  isSwitchToAnotherStruct: boolean, /* Нужно ли переключиться на другую структуру(т.е. сделать активной) */
  newActiveStruct?: StructureInfo|null  /* Структура, которую нужно сделать активной. Если у закрываемого файла есть папка, то её делаем активной */
}
