import {IStructActionInfo, Tab} from '@shared/interfaces/work';

export interface ISessionSectionWithElements {
  sectionId: string;
  structActionInfo?: IStructActionInfo[],
  tabs?: Tab[],
  activeStruct: IStructActionInfo|null
}
export interface ISessionInternalUserWithElems {
  iUserId: string;
  activeSectionId: string|null
}
