export interface IEmployee {
  id: string;
  // TODO добавить profile, tariffInfo(эти уже приходят с сервера) и другие объекты
}
export interface IEmployeeSettings {
  textFontFamily: string;
}
export interface IProfileInfo {
  employee: IEmployee
}
