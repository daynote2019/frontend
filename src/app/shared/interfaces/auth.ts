import {IDeviceResponse} from '@shared/interfaces/request';
import {IDeviceInfo} from '@shared/interfaces/common';
import {IEmployeeSettings} from '@shared/interfaces/profile';

export interface FormRegister {
  email: string;
  agree: boolean;
  lang: string;
}

export interface FormRegisterCreateNew {
  tempUserToken: string;
  lang: string;
  device: IDeviceResponse;
}
export interface FormLogin {
  email: string;
  password: string;
  isAlienComp: boolean;
}
export interface UserCredentialsBase64 {
  sessionId: string;
  token: string;
}
export interface UserAuthInfo {
  account: string;
  check?: string;
  email?: string;
  device?: IDeviceInfo;
  employeeSettings?: IEmployeeSettings;
  hashWorkStructures: string;
}
