import {IDeviceInfo} from '@shared/interfaces/common';
import {StructureInfoType} from '@shared/enums/work';
import {InternalUsers, ITag, StructureInfo} from '@shared/interfaces/work';
import {IEmployee, IEmployeeSettings} from '@shared/interfaces/profile';
import {ISectionInfo} from '@shared/interfaces/work';

export interface ILoginOrRegisterResponse {
  account: string;
  sessionId: string;
  token: string;
}
export interface ICheckAuthResponse {
  account: string;
  email: string;
  device: IDeviceInfo;
  employee: IEmployee;
  employeeSettings: IEmployeeSettings;
  internalUsers: InternalUsers[];
  activeInternalUserId: string;
  activeSectionId: string;
  sectionInfos: ISectionInfo[];
  tags: ITag[]|null;
  hashWorkStructures: string;
}
export interface IAdditionalWorkInfoResponse {
  internalUsers: InternalUsers[];
  allSections: Array<ISectionInfo>;
  allStructures: Array<StructureInfo>;
}
export interface IAuthResponse {
  account: string;
  sessionId: string;
  token: string;
  employee: IEmployee;
  email: string;
  devices: IDeviceInfo[];
  tags: ITag[]|null;
  employeeSettings: IEmployeeSettings;
  internalUsers: InternalUsers[];
  internalUser: InternalUsers;
  sectionInfos: ISectionInfo[];
  sectionInfo: ISectionInfo;
  structureInfo: Array<IStructureInfoResponse>;
  allSections: Array<ISectionInfo>;
  allStructures: Array<StructureInfo>;
  activeStruct: IStructureInfoResponse;
  hashWorkStructures: string;
}

export interface IStructureInfoResponse {
  id: string;
  sectionId: string;
  level: number;
  parentId: string|null;
  parent?: StructureInfo|null;
  type: StructureInfoType;
  readonly?: boolean;
  isFocus?: boolean;
  isRemoved?: boolean; // Удален из дерева
  active?: boolean;
  isOpen?: boolean;
  title: string;
  body?: string;
  selectedTags?: Array<string>; // То, что пришло с сервера
  chosenTags?: Array<ITag>; // В это будет формироваться по id
  isFavourites?: boolean;
  createdAt: string;
  updatedAt: string;
}
export interface IAddTagResponse {
  tag: ITag;
}
