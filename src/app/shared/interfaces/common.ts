import {CentralModalAlertType, IModalBasicCenterType, NavListTypes} from '@shared/enums/common';
import {ISectionInfo, ITag, StructureInfo} from '@shared/interfaces/work';
import {ETagType, StructureInfoType} from '@shared/enums/work';
import {SetStructureInfoByTag} from "@app/root-store/work/actions";
import {ElementRef, QueryList} from "@angular/core";

export interface IEmptyObject {}
export interface Langs {
  code: string;
  name: string;
  title: string;
  img: string;
}
export interface ISelect {
  name: string;
  title?: string;
  id?: string;
}
export interface PageInfo {
  isHome: boolean;
  title: string;
  chosen: string; // Для каждого подпункта "О нас" в это поле передавать about
  childActive?: string;
}
export interface FooterInfo {
  height: number;
}
export interface FooterTopInfoItem {
  isLink: boolean;
  name: string;
  url?: string;
}
export interface FooterTopInfo {
  header: string;
  items: FooterTopInfoItem[];
}
export interface MobileLeftSidenavListItemArrowOpen {
  about?: boolean;
  lang?: boolean;
  users?: boolean;
}
export interface NavList {
  click?: string;
  name: string;
  description?: string;
  type?: NavListTypes;
  active?: string;
  children?: NavList[];
  img?: string;
  action?: () => void;
}
export interface CentralModalAlert {
  state?: boolean;
  type?: CentralModalAlertType;
  title?: string;
  message?: string;
  button?: string;
  url?: string;
  isRedirectIfClickBg?: boolean;
}
export interface IModalBasicCenter {
  state?: boolean;
  type?: IModalBasicCenterType;
  title?: string;
  message?: string;
  inputLabel?: string;
  inputValue?: string;
  notConfirmedInputSubmit?: boolean;
  confirmInputCheck?: (confirmInput: string) => {};
  confirmInputCheckErrorTextTranslate?: string;
  elementName?: string;
  buttonActionName?: string;
  buttonCancelName?: string;
  selectArray?: ISelect[];
  selectValue?: string;
  selectLabel?: string;
  selectBindValue?: string;
  selectBindLabel?: string;
  cancel?: boolean;
  action?: (inputValue?: any, selectValue?: any) => void;
}
export interface IModalRightClick {
  state: boolean;
  type?: StructureInfoType;
  coordinates?: ICoordinates,
  struct?: StructureInfo,
  structRefElements?: QueryList<ElementRef>,
  structRefElement?: ElementRef
}
export interface ICoordinates {
  top?: number;
  left?: number;
}
export interface SpinnerTop {
  state?: boolean;
  action?: () => void;
}
export interface SpinnerCenter {
  state?: boolean;
  action?: () => void;
}
export interface ModalBackground {
  state: boolean;
  isClickable?: boolean;
  light?: boolean;
  action?: () => void;
}
export interface DocumentInfo {
  scrollTop?: number;
  windowWidth?: number;
  windowHeight?: number;
  windowOuterHeight?: number;
  editingAreaWidth?: number;
}
export interface ModalsInfo {
  tagsManagementModal?: TagModalInfo;
}
export interface TagModalInfo {
  isOpenState: boolean;
}
export interface IDeviceInfo {
  name: string;
  browser: string;
  ipAddress?: string;
  activeNow?: string;
  lastVisit?: string;
}
export interface INetworkData {
  ip: string;
  mac: string;
}
export interface ITagForFile {
  structId: string;
  tag: ITag;
}
export interface IStructureInfoByTag {
  selectedTags: Array<string>;
}
export interface IAllAndActiveStructsSerializeResult {
  newStruct?: StructureInfo;
  structureInfo?: Array<StructureInfo>;
  allStructureInfo?: Array<StructureInfo>;
}
export interface IAllAndActiveSections {
  section: ISectionInfo[],
  allSections: ISectionInfo[],
}
