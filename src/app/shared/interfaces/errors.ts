import {AlertTypes} from '@shared/enums/common';

export interface ResponseValidateErrors {
  alertType?: AlertTypes;
  user?: string;
  tempUserToken?: string;
  email?: string;
  password?: string;
  mailer?: string;
}

export interface ResponseResultErrors {
  errors: ResponseValidateErrors;
  resultType: string;
}
