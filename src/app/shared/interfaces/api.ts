export interface IApiResponse {
  result: any;
  state?: string;
  errors?: string[];
}
