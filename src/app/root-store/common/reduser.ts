import { Actions, ActionTypes } from './actions';
import { setLang } from './methods';
import {createFeatureSelector, createSelector, MemoizedSelector} from '@ngrx/store';
import {CentralModalAlert, IModalBasicCenter, IModalRightClick, INetworkData, SpinnerCenter, SpinnerTop} from '@shared/interfaces/common';

export interface FeatureState {
  spinnerTop: SpinnerTop;
  spinnerCenter: SpinnerCenter;
  lang: string;
  centralModalAlert: CentralModalAlert;
  modalBasicCenter: IModalBasicCenter;
  modalInfoBottomRight: IModalBasicCenter;
  modalRightClick: IModalRightClick;
  networkData: INetworkData;
}

export const initialState: FeatureState = {
  spinnerTop: {},
  spinnerCenter: {},
  lang: 'ru',
  centralModalAlert: {
    state: false
  },
  modalBasicCenter: {
    state: false
  },
  modalInfoBottomRight: {
    state: false
  },
  modalRightClick: {
    state: false
  },
  networkData: {
    ip: '',
    mac: ''
  }
};

export function mainReducer(state = initialState, action: Actions): FeatureState {
  switch (action.type) {
    case ActionTypes.SPINNER_TOP_SELECT: {
      return {
        ...state,
        spinnerTop: action.payload
      };
    }
    case ActionTypes.SPINNER_CENTER_SELECT: {
      return {
        ...state,
        spinnerCenter: action.payload
      };
    }
    case ActionTypes.LANG_SELECT: {
      return {
        ...state,
        lang: action.payload.lang
      };
    }
    case ActionTypes.CENTRAL_MODAL_ALERT: {
      return {
        ...state,
        centralModalAlert: action.payload
      };
    }
    case ActionTypes.MODAL_BASIC_CENTER: {
      return {
        ...state,
        modalBasicCenter: action.payload
      };
    }
    case ActionTypes.MODAL_INFO_BOTTOM_RIGHT: {
      return {
        ...state,
        modalInfoBottomRight: action.payload
      };
    }
    case ActionTypes.MODAL_RIGHT_CLICK: {
      return {
        ...state,
        modalRightClick: action.payload
      };
    }
    case ActionTypes.NETWORK_DATA: {
      return {
        ...state,
        networkData: action.payload
      };
    }
    default: {
      return state;
    }
  }
}

export const selectFeatureState: MemoizedSelector<object, FeatureState> = createFeatureSelector<FeatureState>('mainReducer');

export const getLang = (state: FeatureState): string => state.lang;
export const getSpinnerTop = (state: FeatureState): SpinnerTop => state.spinnerTop;
export const getSpinnerCenter = (state: FeatureState): SpinnerTop => state.spinnerCenter;
export const getCentralModalAlert = (state: FeatureState): CentralModalAlert => state.centralModalAlert;
export const getModalBasicCenter = (state: FeatureState): IModalBasicCenter => state.modalBasicCenter;
export const getModalInfoBottomRight = (state: FeatureState): IModalBasicCenter => state.modalInfoBottomRight;
export const getModalRightClick = (state: FeatureState): IModalRightClick => state.modalRightClick;
export const getNetworkData = (state: FeatureState): INetworkData => state.networkData;

export const selectLang: MemoizedSelector<object, string> = createSelector(selectFeatureState, getLang);
export const selectSpinnerTop: MemoizedSelector<object, SpinnerTop> = createSelector(selectFeatureState, getSpinnerTop);
export const selectSpinnerCenter: MemoizedSelector<object, SpinnerCenter> = createSelector(selectFeatureState, getSpinnerCenter);
export const selectCentralModalAlert: MemoizedSelector<object, CentralModalAlert> = createSelector(selectFeatureState, getCentralModalAlert);
export const selectModalBasicCenter: MemoizedSelector<object, IModalBasicCenter> = createSelector(selectFeatureState, getModalBasicCenter);
export const selectModalInfoBottomRight: MemoizedSelector<object, IModalBasicCenter> = createSelector(selectFeatureState, getModalInfoBottomRight);
export const selectModalRightClick: MemoizedSelector<object, IModalRightClick> = createSelector(selectFeatureState, getModalRightClick);
export const selectNetworkData: MemoizedSelector<object, INetworkData> = createSelector(selectFeatureState, getNetworkData);
