import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of as observableOf } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import {CustomTranslateService} from '@shared/services/custom-translate.service';

import * as mainActions from './actions';
import * as errorsActions from '../errors/actions';

@Injectable()
export class MainStoreEffects {
  constructor(
    private customTranslateService: CustomTranslateService,
    private actions$: Actions,
  ) {}

  /**
   * Выбрать язык
   * @param string lang
   */
  @Effect()
  LangRequestEffect$ = this.actions$.pipe(
    ofType<mainActions.LangSelectAction>(mainActions.ActionTypes.LANG_SELECT),
    switchMap(action =>
      this.customTranslateService.setLang(action.payload.lang)
        .pipe( map(_ => {}) )
    )
  );
}
