import { Action } from '@ngrx/store';
import {
  CentralModalAlert, IModalBasicCenter, IModalRightClick, INetworkData, SpinnerCenter,
  SpinnerTop
} from '@shared/interfaces/common';

export enum ActionTypes {
  EMPTY_SUCCESS = '[Common] Empty Success',
  LANG_SELECT = '[Common] Lang select',
  SPINNER_TOP_SELECT = '[Common] Spinner top select',
  SPINNER_CENTER_SELECT = '[Common] Spinner center select',
  CENTRAL_MODAL_ALERT = '[Common] Central modal alert',
  MODAL_BASIC_CENTER = '[Common] Modal basic center',
  MODAL_INFO_BOTTOM_RIGHT = 'MODAL_INFO_BOTTOM_RIGHT',
  MODAL_RIGHT_CLICK = 'MODAL_RIGHT_CLICK',
  NETWORK_DATA = '[Common] Network Data',
}

export class EmptySuccessAction implements Action {
  readonly type = ActionTypes.EMPTY_SUCCESS;
}

export class LangSelectAction implements Action {
  readonly type = ActionTypes.LANG_SELECT;
  constructor(public payload: { lang: string }) {}
}

export class SpinnerTopSelectAction implements Action {
  readonly type = ActionTypes.SPINNER_TOP_SELECT;
  constructor(public payload: SpinnerTop) {}
}

export class SpinnerCenterSelectAction implements Action {
  readonly type = ActionTypes.SPINNER_CENTER_SELECT;
  constructor(public payload: SpinnerCenter) {}
}

export class CentralModalAlertSelectAction implements Action {
  readonly type = ActionTypes.CENTRAL_MODAL_ALERT;
  constructor(public payload: CentralModalAlert) {}
}

export class ModalBasicCenterAction implements Action {
  readonly type = ActionTypes.MODAL_BASIC_CENTER;
  constructor(public payload: IModalBasicCenter) {}
}

export class ModalInfoBottomRightAction implements Action {
  readonly type = ActionTypes.MODAL_INFO_BOTTOM_RIGHT;
  constructor(public payload: IModalBasicCenter) {}
}


export class ModalRightClickAction implements Action {
  readonly type = ActionTypes.MODAL_RIGHT_CLICK;
  constructor(public payload: IModalRightClick) {}
}

export class NetworkDataSelectAction implements Action {
  readonly type = ActionTypes.NETWORK_DATA;
  constructor(public payload: INetworkData) {}
}

export type Actions = EmptySuccessAction | LangSelectAction | CentralModalAlertSelectAction |
  SpinnerTopSelectAction | SpinnerCenterSelectAction | NetworkDataSelectAction |
  ModalBasicCenterAction | ModalInfoBottomRightAction | ModalRightClickAction;
