import {FeatureState} from '@app/root-store/common/reduser';
import {ITag, StructureInfo} from '@shared/interfaces/work';
import {HelpersService} from '@shared/services/helpers.service';
import {ITagForFile} from '@shared/interfaces/common';
import {ITagChangeRequest} from '@shared/interfaces/request';

/** Записать в хранилище все табы */
export function setLang(lang: string, state: FeatureState) {
  return lang;
}

/* TAGS */

/* Добавить один тэг в массив тэгов */
export function addTagToAllTags(newTagInfo: ITagForFile, tags: ITag[]): ITag[] {
  if (newTagInfo.tag) {
    const newTag = newTagInfo.tag;
    const findTag = tags.find(_tag => _tag.id === newTag.id);
    if (!findTag) {
      tags.push(newTag); /* Добавить тэг в массив всех тэгов */
    }
  }

  return Object.assign([], tags);
}
/* Удалить один тэг из массива тэгов */
export function removeOneTagFromAllTags(newTagInfo: ITagForFile, tags: ITag[]): ITag[] {
  if (newTagInfo.tag) {
    return tags.filter(_tag => _tag.id !== newTagInfo.tag.id);
  }
  return tags;
}
/* Изменить имя тэга */
export function changeTagNameFromAllTags(tagInfo: ITagChangeRequest, tags: ITag[]): ITag[] {
  tags.forEach(_tag => {
    if (_tag.id === tagInfo.tagId) {
      _tag.name = tagInfo.name;
    }
  });
  return Object.assign([], tags);
}
/* Поменять название тега у всех структур инфо */
export function changeTagNameFromAllStructInfo(tagInfo: ITagChangeRequest,
                                               allStructureInfo: StructureInfo[]): StructureInfo[] {
  const helpers = new HelpersService();
  return helpers.changeTagNameFromAllStructInfo(tagInfo, allStructureInfo);
}
/* Удалить тэг у структуры инфо */
export function changeTagNameFromActiveStruct(tagInfo: ITagChangeRequest,
                                              activeStruct: StructureInfo): StructureInfo {
  if (activeStruct) {
    const _activeStruct = Object.assign({}, activeStruct);
    if (_activeStruct.chosenTags) {
      _activeStruct.chosenTags = _activeStruct.chosenTags
        .map(_tag => {
          if (_tag.id === tagInfo.tagId) {
            _tag.name = tagInfo.name;
          }
          return _tag;
        });
    }

    return _activeStruct;
  }

  return activeStruct;
}
/* Добавить тэг в структуру инфо */
export function addTagIdToStructInfo(newTagInfo: ITagForFile,
                                     allStructureInfo: StructureInfo[]): StructureInfo[] {
  const _allStructureInfo = allStructureInfo;
  if (newTagInfo.tag) {
    // const helpers = new HelpersService();
    // const find = helpers.findInTree('children');
    // const struct: StructureInfo = find(structureInfos, 'id', newTagInfo.structId);
    const foundStruct = _allStructureInfo.find(_struct => _struct.id === newTagInfo.structId);
    let foundTAg = null;
    if (foundStruct) {
      if (foundStruct.chosenTags) {
        foundTAg = foundStruct.chosenTags.find(_tag => _tag.id === newTagInfo.tag.id);
      } else {
        foundStruct.chosenTags = [];
      }
      if (!foundTAg) {
        foundStruct.chosenTags.push(newTagInfo.tag);
      }
    }
  }

  return _allStructureInfo;
}
/* Удалить тэг у структуры инфо */
export function removeOneTagFromStructInfo(newTagInfo: ITagForFile,
                                           allStructureInfo: StructureInfo[]): StructureInfo[] {
  const _allStructureInfo = allStructureInfo;
  if (newTagInfo.tag) {
    const foundStruct: StructureInfo = _allStructureInfo.find(_struct => _struct.id === newTagInfo.structId);
    if (foundStruct && foundStruct.chosenTags) {
      foundStruct.chosenTags = foundStruct.chosenTags.filter(_tag => _tag.id !== newTagInfo.tag.id);
    }
  }

  return _allStructureInfo;
}
/* Удалить тэг у всех структур инфо */
export function removeOneTagFromAllStructInfo(tagId: string,
                                     structureInfos: StructureInfo[]): StructureInfo[] {
  let _structureInfos = structureInfos;
  if (tagId) {
    const helpers = new HelpersService();
    const finder = helpers.removeOneTagFromAllStructInfo(tagId);
    _structureInfos = finder(_structureInfos);
  }

  return _structureInfos;
}
/* Добавить тэг к активной структуре(если она не успела поменяться) */
export function addTagIdToActiveStruct(newTagInfo: ITagForFile,
                                       activeStruct: StructureInfo): StructureInfo {
  if (activeStruct && newTagInfo.tag) {
    if (activeStruct.id === newTagInfo.structId) {
      const _activeStruct = Object.assign({}, activeStruct);
      let findTAg = null;
      if (_activeStruct.chosenTags) {
        findTAg = _activeStruct.chosenTags.find(_tag => _tag.id === newTagInfo.tag.id);
      } else {
        _activeStruct.chosenTags = [];
      }
      if (!findTAg) {
        _activeStruct.chosenTags.push(newTagInfo.tag);
      }

      return _activeStruct;
    }
  }

  return activeStruct;
}
/* Удалить тэг у активной структуры(если она не успела поменяться) */
export function removeOneTagFromActiveStruct(activeStruct: StructureInfo,
                                             newTagInfo: ITagForFile|null,
                                             tagId: string|null): StructureInfo {
  if (activeStruct) {
    const _activeStruct = Object.assign({}, activeStruct);

    if (newTagInfo && newTagInfo.tag) {
      if (activeStruct.id === newTagInfo.structId) {
        if (_activeStruct.chosenTags) {
          _activeStruct.chosenTags = _activeStruct.chosenTags.filter(_tag => _tag.id !== newTagInfo.tag.id);
        }
      }
    } else if (tagId) {
      if (_activeStruct.chosenTags) {
        _activeStruct.chosenTags = _activeStruct.chosenTags.filter(_tag => _tag.id !== tagId);
      }
    }

    return _activeStruct;
  }

  return activeStruct;
}
