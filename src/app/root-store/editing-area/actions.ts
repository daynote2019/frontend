import { Action } from '@ngrx/store';
import {IStateRequest, ITagForFileRequest, ITagRemoveFromAllRequest} from '@shared/interfaces/request';

export enum ActionTypes {
  CHANGE_STRUCT_INFO_FAVOURITES_STATE = 'CHANGE_STRUCT_INFO_FAVOURITES_STATE',
  ADD_TAG_SERVER_ACTION = 'ADD_TAG_SERVER_ACTION',
  REMOVE_TAG_FROM_FILE_SERVER_ACTION = 'REMOVE_TAG_FROM_FILE_SERVER_ACTION',
  REMOVE_TAG_FROM_ALL_ON_SERVER_ACTION = 'REMOVE_TAG_FROM_ALL_ON_SERVER_ACTION',
}

export class ChangeStructInfoFavouritesState implements Action {
  readonly type = ActionTypes.CHANGE_STRUCT_INFO_FAVOURITES_STATE;
  constructor(public payload: IStateRequest) {}
}
export class AddTagServerAction implements Action {
  readonly type = ActionTypes.ADD_TAG_SERVER_ACTION;
  constructor(public payload: ITagForFileRequest) {}
}
export class RemoveTagFromFileServerAction implements Action {
  readonly type = ActionTypes.REMOVE_TAG_FROM_FILE_SERVER_ACTION;
  constructor(public payload: ITagForFileRequest) {}
}
export class RemoveTagFromAllOnServerAction implements Action {
  readonly type = ActionTypes.REMOVE_TAG_FROM_ALL_ON_SERVER_ACTION;
  constructor(public payload: ITagRemoveFromAllRequest) {}
}

export type Actions = ChangeStructInfoFavouritesState | AddTagServerAction | RemoveTagFromAllOnServerAction;
