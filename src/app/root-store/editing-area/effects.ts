import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {Action, select, Store} from '@ngrx/store';
import { Observable, of as observableOf } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import * as editingAreaActions from './actions';
import {IApiResponse} from '@shared/interfaces/api';
import {IAddTagResponse, IAuthResponse} from '@shared/interfaces/response';
import {EmptySuccessAction, ModalInfoBottomRightAction, SpinnerTopSelectAction} from '@app/root-store/common/actions';
import {EditingAreaService} from '@shared/services/editing-area.service';
import * as mainRoot from '@app/root-store/common/reduser';
import {ApiService} from '@shared/services/api.service';
import {
  AddTagAction,
  ChangeStructureInfoAnyValue,
  RemoveTagFromAllAction,
  RemoveTagFromFileAction,
  SetStructureInfoByTag
} from '@app/root-store/work/actions';
import {EStructureInfoField} from '@shared/enums/work';
import * as workRoot from '@app/root-store/work/reduser';
import * as editingAreaRoot from '@app/root-store/editing-area/reduser';
import {IModalBasicCenterType} from '@shared/enums/common';
import {TagModel} from '@shared/models/TagModel';

@Injectable()
export class EditingAreaEffects {
  private selectedTagsSubscription;
  private selectedTags;

  constructor(
    private actions$: Actions,
    private api: ApiService,
    private store$: Store<mainRoot.FeatureState>,
    private editingAreaService: EditingAreaService,
    protected storeMain$: Store<mainRoot.FeatureState>,
    private storeWork$: Store<workRoot.WorkState>,
    private storeEditingArea$: Store<editingAreaRoot.EditingAreaState>,
  ) {
    this.selectedTagsSubscription = this.storeWork$
      .pipe(select(workRoot.selectSelectedTags))
      .subscribe(selectedTags => {
        this.selectedTags = selectedTags;
      });
  }

  ngOnDestroy() {
    console.log('selectedTagsSubscription.unsubscribe');
    this.selectedTagsSubscription.unsubscribe();
  }

  /**
   * Поменять статус "Избранное" в структуре информации
   * @param IStateRequest payload
   */
  @Effect()
  ChangeStructInfoFavouritesRequestEffect$ = this.actions$.pipe(
    ofType<editingAreaActions.ChangeStructInfoFavouritesState>(
      editingAreaActions.ActionTypes.CHANGE_STRUCT_INFO_FAVOURITES_STATE),
    switchMap(action =>
      this.editingAreaService.changeStructInfoFavouritesStateOnServer(action.payload)
        .pipe(
          map((res: IApiResponse) => {
            const result: IAuthResponse = res.result;
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            this.storeWork$.dispatch(new ChangeStructureInfoAnyValue({
              structId: action.payload.elementId,
              fields: [
                {name: EStructureInfoField.isFavourites, value: action.payload.state}
              ]
            }));
            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Добавить тэг для файла
   * @param ITagForFileRequest payload
   */
  @Effect()
  AddTagRequestEffect$ = this.actions$.pipe(
    ofType<editingAreaActions.AddTagServerAction>(
      editingAreaActions.ActionTypes.ADD_TAG_SERVER_ACTION),
    switchMap(action =>
      this.editingAreaService.addTagOnServer(action.payload)
        .pipe(
          map((res: IApiResponse) => {
            const result: IAddTagResponse = res.result;
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            this.storeWork$.dispatch(new AddTagAction({
              structId: action.payload.structId,
              tag: new TagModel(result.tag).deserialize()
            }));
            this.store$.dispatch(new ModalInfoBottomRightAction({
              state: true,
              type: IModalBasicCenterType.info,
              title: 'info.title',
              message: 'info.tags.isAdd',
            }));
            setTimeout(() => {
              this.storeWork$.dispatch(
                new SetStructureInfoByTag({
                  selectedTags: this.selectedTags
                })
              );
            }, 50);
            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Удалить тэг у файла
   * @param ITagForFileRequest payload
   */
  @Effect()
  RemoveTagFromFileRequestEffect$ = this.actions$.pipe(
    ofType<editingAreaActions.RemoveTagFromFileServerAction>(
      editingAreaActions.ActionTypes.REMOVE_TAG_FROM_FILE_SERVER_ACTION),
    switchMap(action =>
      this.editingAreaService.removeTagFromFileOnServer(action.payload)
        .pipe(
          map((res: IApiResponse) => {
            const result: IAddTagResponse = res.result;
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            this.storeWork$.dispatch(new RemoveTagFromFileAction({
              structId: action.payload.structId,
              tag: {
                id: action.payload.tagId,
                name: action.payload.name,
                type: action.payload.type,
                isSelected: false,
              }
            }));
            this.store$.dispatch(new ModalInfoBottomRightAction({
              state: true,
              type: IModalBasicCenterType.info,
              title: 'info.title',
              message: 'info.tags.isRemove',
            }));
            setTimeout(() => {
              this.storeWork$.dispatch(
                new SetStructureInfoByTag({
                  selectedTags: this.selectedTags
                })
              );
            }, 50);
            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Удалить тэг везде
   * @param ITagRemoveFromAllRequest payload
   */
  @Effect()
  RemoveTagFromAllOnServerRequestEffect$ = this.actions$.pipe(
    ofType<editingAreaActions.RemoveTagFromAllOnServerAction>(
      editingAreaActions.ActionTypes.REMOVE_TAG_FROM_ALL_ON_SERVER_ACTION),
    switchMap(action =>
      this.editingAreaService.removeTagFromAllOnServer(action.payload)
        .pipe(
          map((res: IApiResponse) => {
            const result: IAddTagResponse = res.result;
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            this.storeWork$.dispatch(new RemoveTagFromAllAction({
              tagId: action.payload.tagId
            }));
            this.store$.dispatch(new ModalInfoBottomRightAction({
              state: true,
              type: IModalBasicCenterType.info,
              title: 'info.title',
              message: 'info.tags.isRemove',
            }));
            setTimeout(() => {
              this.storeWork$.dispatch(
                new SetStructureInfoByTag({
                  selectedTags: this.selectedTags
                })
              );
            }, 50);
            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );
}
