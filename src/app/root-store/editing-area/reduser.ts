import { Actions, ActionTypes } from './actions';
import {createFeatureSelector, createSelector, MemoizedSelector} from '@ngrx/store';

export interface EditingAreaState {

}

export const initialState: EditingAreaState = {

};

export function editingAreaReducer(state = initialState, action: Actions): EditingAreaState {
  switch (action.type) {
    default: {
      return state;
    }
  }
}

export const selectFeatureState: MemoizedSelector<object, EditingAreaState> = createFeatureSelector<EditingAreaState>('editingAreaReducer');
