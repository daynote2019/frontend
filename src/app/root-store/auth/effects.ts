import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {switchMap, map, tap, catchError} from 'rxjs/operators';

import {CentralModalAlertSelectAction, SpinnerTopSelectAction, EmptySuccessAction} from '../common/actions';
import * as authActions from './actions';
import {AuthService} from '@shared/services/auth.service';
import {ApiService} from '@shared/services/api.service';
import * as mainRoot from '@app/root-store/common/reduser';
import {CentralModalAlertType, Pages} from '@shared/enums/common';
import {NavService} from '@shared/services/nav.service';
import {ILoginOrRegisterResponse} from '@shared/interfaces/response';
import {SessionService} from '@shared/services/session.service';
import {IApiResponse} from '@shared/interfaces/api';

@Injectable()
export class AuthStoreEffects {
  constructor(
    private authService: AuthService,
    private navService: NavService,
    private actions$: Actions,
    private api: ApiService,
    private sessionService: SessionService,
    protected storeMain$: Store<mainRoot.FeatureState>,
  ) {}

  /**
   * Регистрация временного пользователя(Шаг 1)
   * @param FormRegister form
   */
  @Effect()
  RegisterRequestEffect$ = this.actions$.pipe(
    ofType<authActions.RegisterAction>(authActions.ActionTypes.REGISTER),
    switchMap(action =>
      this.authService.register(action.form)
        .pipe(
          map(_ => new SpinnerTopSelectAction({
            state: false,
            action: () => {
              return this.storeMain$.dispatch(new CentralModalAlertSelectAction({
                state: true,
                type: CentralModalAlertType.success,
                title: 'centralModalAlert.registerStepOneSuccess.title',
                message: 'centralModalAlert.registerStepOneSuccess.message',
                button: 'centralModalAlert.registerStepOneSuccess.button',
                url: Pages.login,
              }));
            }
          }))
        )
    )
  );

  /**
   * Регистрация постоянного пользователя(Шаг 2)
   * @param FormRegister data
   */
  @Effect()
  RegisterCreateNewRequestEffect$ = this.actions$.pipe(
    ofType<authActions.RegisterCreateNewAction>(authActions.ActionTypes.REGISTER_CREATE_NEW),
    switchMap(action =>
      this.authService.registerCreateNew(action.data)
        .pipe(
          tap((res: IApiResponse) => {
            const result: ILoginOrRegisterResponse = res.result;
            this.sessionService.setAuthResponseData(result);
            this.navService.onPageRoute('');
          }),
          map(_ => new EmptySuccessAction()),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Авторизация (вход в систему)
   * @param FormRegister form
   */
  @Effect()
  LoginRequestEffect$ = this.actions$.pipe(
    ofType<authActions.LoginAction>(authActions.ActionTypes.LOGIN),
    switchMap(action =>
      this.authService.login(action.form)
        .pipe(
          tap((res: IApiResponse) => {
            const result: ILoginOrRegisterResponse = res.result;
            this.sessionService.setAuthResponseData(result, action.form.isAlienComp);
            this.navService.onPageRoute('');
          }),
          map(_ => new EmptySuccessAction()),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Выход из системы
   */
  @Effect()
  LogoutRequestEffect$ = this.actions$.pipe(
    ofType<authActions.LogoutAction>(authActions.ActionTypes.LOGOUT),
    switchMap(_ =>
      this.authService.logout()
        .pipe(
          map(_ => new EmptySuccessAction()),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Восстановление пароля
   * @param string email
   */
  @Effect()
  ForgotPasswordRequestEffect$ = this.actions$.pipe(
    ofType<authActions.ForgotPasswordAction>(authActions.ActionTypes.FORGOT_PASSWORD),
    switchMap(action =>
      this.authService.forgotPassword(action.email)
        .pipe(
          map((res: IApiResponse) => {
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            return new CentralModalAlertSelectAction({
              state: true,
              type: CentralModalAlertType.success,
              title: 'centralModalAlert.forgotPassword.title',
              message: 'centralModalAlert.forgotPassword.message',
              button: 'centralModalAlert.forgotPassword.button',
              url: Pages.login,
            });
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );
}
