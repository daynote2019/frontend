import { Actions, ActionTypes } from './actions';
import {createFeatureSelector, createSelector, MemoizedSelector} from '@ngrx/store';
import {IDeviceInfo} from '@shared/interfaces/common';
import {IEmployeeSettings} from '@shared/interfaces/profile';

export interface AuthState {
  email: string;
  account: string;
  device: IDeviceInfo|null;
  employeeSettings: IEmployeeSettings;
  hashWorkStructures: string;
}

export const initialState: AuthState = {
  email: '',
  account: '',
  device: null,
  employeeSettings: {
    textFontFamily: 'base'
  },
  hashWorkStructures: '',
};

export function authReducer(state = initialState, action: Actions): AuthState {
  switch (action.type) {
    case ActionTypes.USER_AUTH_INFO: {
      return {
        ...state,
        email: action.userInfo.email,
        account: action.userInfo.account,
        device: action.userInfo.device,
        employeeSettings: action.userInfo.employeeSettings,
        hashWorkStructures: action.userInfo.hashWorkStructures,
      };
    }
    default: {
      return state;
    }
  }
}

export const selectAuthState: MemoizedSelector<object, AuthState> = createFeatureSelector<AuthState>('authReducer');

export const getUserEmail = (state: AuthState): string => state.email;
export const selectUserEmail: MemoizedSelector<object, string> = createSelector(selectAuthState, getUserEmail);

export const getEmployeeSettings = (state: AuthState): IEmployeeSettings => state.employeeSettings;
export const selectEmployeeSettings: MemoizedSelector<object, IEmployeeSettings> = createSelector(selectAuthState, getEmployeeSettings);

export const getHashWorkStructures = (state: AuthState): string => state.hashWorkStructures;
export const selectHashWorkStructures: MemoizedSelector<object, string> = createSelector(selectAuthState, getHashWorkStructures);
