import { Action } from '@ngrx/store';
import {FormLogin, FormRegister, FormRegisterCreateNew, UserAuthInfo} from '@shared/interfaces/auth';

export enum ActionTypes {
  REGISTER = '[auth] REGISTER temp user post',
  REGISTER_CREATE_NEW = '[auth] REGISTER create new post',
  LOGIN = '[auth] LOGIN post',
  LOGOUT = '[auth] LOGOUT post',
  FORGOT_PASSWORD = '[auth] FORGOT_PASSWORD post',
  USER_AUTH_INFO = '[auth] USER_AUTH_INFO select',
}

export class RegisterAction implements Action {
  readonly type = ActionTypes.REGISTER;
  constructor(public form: FormRegister) {}
}

export class RegisterCreateNewAction implements Action {
  readonly type = ActionTypes.REGISTER_CREATE_NEW;
  constructor(public data: FormRegisterCreateNew) {}
}

export class LoginAction implements Action {
  readonly type = ActionTypes.LOGIN;
  constructor(public form: FormLogin) {}
}

export class LogoutAction implements Action {
  readonly type = ActionTypes.LOGOUT;
  constructor() {}
}

export class ForgotPasswordAction implements Action {
  readonly type = ActionTypes.FORGOT_PASSWORD;
  constructor(public email: string) {}
}

export class UserAuthInfoSelectAction implements Action {
  readonly type = ActionTypes.USER_AUTH_INFO;
  constructor(public userInfo: UserAuthInfo) {}
}

export type Actions = RegisterAction | RegisterCreateNewAction | UserAuthInfoSelectAction |
  ForgotPasswordAction | LogoutAction;
