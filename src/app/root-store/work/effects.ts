import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {switchMap, map, tap, catchError} from 'rxjs/operators';

import {SpinnerTopSelectAction, EmptySuccessAction, ModalInfoBottomRightAction} from '../common/actions';
import * as workActions from './actions';
import {ApiService} from '@shared/services/api.service';
import * as mainRoot from '@app/root-store/common/reduser';
import * as workRoot from '@app/root-store/work/reduser';
import {NavService} from '@shared/services/nav.service';
import {IApiResponse} from '@shared/interfaces/api';
import {WorkService} from '@shared/services/work.service';
import {
  AddNewStructureInfoAction,
  AddSectionAction, RecoverStructureInfoAction,
  RemoveSectionInfoAction,
  RemoveStructureInfoAction, SelectActiveInternalUser, SelectActiveStructAction, SetActiveSectionAction,
} from './actions';
import {IAdditionalWorkInfoResponse, IAuthResponse} from '@shared/interfaces/response';
import {IModalBasicCenterType} from '@shared/enums/common';
import {AddInternalUserAction} from './actions';
import {RecoverSectionInfoAction} from "./actions";
import {SetAdditionalWorkInfoAction} from "./actions";
import {SelectActiveStructServerAction} from "./actions";
import {UpdateWorkAreaElementsByActiveSectionAction} from "./actions";

@Injectable()
export class WorkStoreEffects {
  constructor(
    private navService: NavService,
    private actions$: Actions,
    private api: ApiService,
    private workService: WorkService,
    protected storeMain$: Store<mainRoot.FeatureState>,
    protected storeWork$: Store<workRoot.WorkState>
  ) {}

  /**
   * Подгрузить дополнительные элементы рабочей области
   * @param AdditionalWorkInfo additionalWorkInfo
   */
  @Effect()
  AdditionalWorkInfoRequestEffect$ = this.actions$.pipe(
    ofType<workActions.SelectAdditionalWorkInfoAction>(workActions.ActionTypes.SELECT_ADDITIONAL_WORK_INFO),
    switchMap(action =>
      this.workService.getAdditionalWorkInfo()
        .pipe(
          map((res: IApiResponse) => {
            const result: IAdditionalWorkInfoResponse = res.result;
            const allAndActiveStructs = this.workService.allAndActiveStructsSerialize(result.allStructures);
            const allSections = result.allSections; /* Чтобы во всех секциях и секциях активного вн. юзера были одинаковые объекты */
            return new SetAdditionalWorkInfoAction({
              internalUsers: result.internalUsers,
              sectionInfos: allSections.filter(_section => // Обновляем секции потому, что в checkAuth были загружены без tabs и activeStructId
                _section.internalUserId === (action.activeInternalUser && action.activeInternalUser.id) &&
                !_section.isRemoved),
              allSectionInfos: allSections,
              structureInfo: allAndActiveStructs.structureInfo,
              allStructureInfo: allAndActiveStructs.allStructureInfo,
            });
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Добавить новую структуру информации
   * @param IStructureInfoRequest newStruct
   */
  @Effect()
  AddNewStructureInfoRequestEffect$ = this.actions$.pipe(
    ofType<workActions.AddNewStructureInfoServerAction>(workActions.ActionTypes.ADD_NEW_STRUCTURE_INFO_SERVER),
    switchMap(action =>
      this.workService.addNewStructureInfo(action.payload.struct)
        .pipe(
          map((res: IApiResponse) => {
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            this.storeMain$.dispatch(new AddNewStructureInfoAction(action.payload.struct));
            if (action.payload.success) {
              action.payload.success();
            }
            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Назначить активную структуру инфо
   * @param ISelectActiveStructInfo payload
   */
  @Effect()
  SelectActiveStructRequestEffect$ = this.actions$.pipe(
    ofType<workActions.SelectActiveStructServerAction>(workActions.ActionTypes.SELECT_ACTIVE_STRUCT_SERVER),
    switchMap(action =>
      this.workService.selectActiveStructInfo(action.payload)
        .pipe(
          map((res: IApiResponse) => {
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            if (!action.payload.isOpenFolder) { /* Если только открыли/закрыли папку, то НЕ делаем файл активным */
              this.storeMain$.dispatch(new SelectActiveStructAction(action.payload.struct));
            }
            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Удалить структуру информации
   * @param IRemoveStructureInfoRequest payload
   */
  @Effect()
  RemoveStructRequestEffect$ = this.actions$.pipe(
    ofType<workActions.RemoveStructureInfoServerAction>(workActions.ActionTypes.REMOVE_STRUCTURE_INFO_SERVER),
    switchMap(action =>
      this.workService.removeStruct(action.payload)
        .pipe(
          map((res: IApiResponse) => {
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            this.storeWork$.dispatch(new RemoveStructureInfoAction(action.payload));
            if (action.payload.success) {
              action.payload.success();
            }
            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Восстановить структуру информации
   * @param IRecoverStructureInfoRequest payload
   */
  @Effect()
  RecoverStructRequestEffect$ = this.actions$.pipe(
    ofType<workActions.RecoverStructureInfoServerAction>(workActions.ActionTypes.RECOVER_STRUCTURE_INFO_SERVER),
    switchMap(action =>
      this.workService.recoverStruct(action.payload)
        .pipe(
          map((res: IApiResponse) => {
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            this.storeMain$.dispatch(new RecoverStructureInfoAction(action.payload));
            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Поменять название структуры информации
   * @param IStructureInfoTitleRequest payload
   */
  @Effect()
  ChangeStructureInfoTitleRequestEffect$ = this.actions$.pipe(
    ofType<workActions.ChangeStructureInfoTitle>(workActions.ActionTypes.CHANGE_STRUCTURE_INFO_TITLE),
    switchMap(action =>
      this.workService.changeStructureInfoTitle(action.payload)
        .pipe(
          map((res: IApiResponse) => {
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Открыть все родительские папки
   * @param IExpandParentsRequest payload
   */
  @Effect()
  ExpandParentsRequestEffect$ = this.actions$.pipe(
    ofType<workActions.ExpandParents>(workActions.ActionTypes.EXPAND_PARENTS),
    switchMap(action =>
      this.workService.expandParents(action.payload)
        .pipe(
          map((res: IApiResponse) => {
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Закрыть вкладку
   * @param ICloseTabRequest payload
   */
  @Effect()
  CloseTabRequestEffect$ = this.actions$.pipe(
    ofType<workActions.CloseTabAction>(workActions.ActionTypes.CLOSE_TAB),
    switchMap(action =>
      this.workService.closeTab(action.payload)
        .pipe(
          map((res: IApiResponse) => {
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Создать новую секцию
   * @param string name
   */
  @Effect()
  CreateSectionRequestEffect$ = this.actions$.pipe(
    ofType<workActions.CreateSectionAction>(workActions.ActionTypes.CREATE_SECTION_INFO_ON_SERVER),
    switchMap(action =>
      this.workService.createNewSection(action.name)
        .pipe(
          map((res: IApiResponse) => {
            const result: IAuthResponse = res.result;
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            this.storeWork$.dispatch(new AddSectionAction(result.sectionInfo));
            setTimeout(() => {
              this.storeMain$.dispatch(new ModalInfoBottomRightAction({
                state: true,
                type: IModalBasicCenterType.info,
                title: 'info.title',
                message: 'info.sectionInfo.newSectionCreated',
              }));
            }, 300);
            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Выбрать секцию
   * @param ISectionInfo payload
   */
  @Effect()
  SelectSectionRequestEffect$ = this.actions$.pipe(
    ofType<workActions.SelectSectionServerAction>(workActions.ActionTypes.SELECT_SECTION_SERVER),
    switchMap(action =>
      this.workService.selectSection(action.payload)
        .pipe(
          map((res: IApiResponse) => {
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            this.storeWork$.dispatch(new SetActiveSectionAction(action.payload));
            this.storeWork$.dispatch(
              new UpdateWorkAreaElementsByActiveSectionAction(action.payload)
            );

            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Изменить название секции
   * @param ISectionInfo payload
   */
  @Effect()
  RenameSectionRequestEffect$ = this.actions$.pipe(
    ofType<workActions.RenameSectionAction>(workActions.ActionTypes.RENAME_SECTION_INFO),
    switchMap(action =>
      this.workService.renameSection(action.payload)
        .pipe(
          map((res: IApiResponse) => {
            const result: IAuthResponse = res.result;
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Удалить секцию и её структуры инфо
   * @param IRemoveSectionInfoServer payload
   */
  @Effect()
  RemoveSectionRequestEffect$ = this.actions$.pipe(
    ofType<workActions.RemoveSectionInfoServerAction>(workActions.ActionTypes.REMOVE_SECTION_INFO_SERVER),
    switchMap(action =>
      this.workService.removeSection(action.payload)
        .pipe(
          map((res: IApiResponse) => {
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            this.storeWork$.dispatch(new RemoveSectionInfoAction(action.payload));
            action.payload.success();
            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Восстановить секцию и её структуры инфо
   * @param ISectionInfo payload
   */
  @Effect()
  RecoverSectionRequestEffect$ = this.actions$.pipe(
    ofType<workActions.RecoverSectionInfoServerAction>(workActions.ActionTypes.RECOVER_SECTION_INFO_SERVER),
    switchMap(action =>
      this.workService.recoverSection(action.payload)
        .pipe(
          map((res: IApiResponse) => {
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            this.storeWork$.dispatch(new RecoverSectionInfoAction(action.payload));
            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Выбрать вн. пользователя
   * @param InternalUsers payload
   */
  @Effect()
  SelectActiveInternalUserRequestEffect$ = this.actions$.pipe(
    ofType<workActions.SelectInternalUserServerAction>(workActions.ActionTypes.SELECT_INTERNAL_USER_SERVER),
    switchMap(action =>
      this.workService.selectActiveInternalUser(action.payload)
        .pipe(
          map((res: IApiResponse) => {
            const result: IAuthResponse = res.result;
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            this.storeWork$.dispatch(new SelectActiveInternalUser(action.payload));
            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Создать вн. пользователя
   * @param InternalUsers payload
   */
  @Effect()
  CreateInternalUserRequestEffect$ = this.actions$.pipe(
    ofType<workActions.CreateInternalUserAction>(workActions.ActionTypes.CREATE_INTERNAL_USER),
    switchMap(action =>
      this.workService.createInternalUser(action.payload)
        .pipe(
          map((res: IApiResponse) => {
            const result: IAuthResponse = res.result;
            const internalUser = result.internalUser;
            const sectionInfo = result.sectionInfo;
            this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: false }));
            this.storeWork$.dispatch(new AddInternalUserAction(internalUser, sectionInfo));
            setTimeout(() => {
              this.storeMain$.dispatch(new ModalInfoBottomRightAction({
                state: true,
                type: IModalBasicCenterType.info,
                title: 'info.title',
                message: 'info.internalUser.newUserCreated',
              }));
            }, 300);
            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );

  /**
   * Изменить название тэга
   * @param ITagChangeRequest payload
   */
  @Effect()
  ChangeTagNameRequestEffect$ = this.actions$.pipe(
    ofType<workActions.ChangeTagNameAction>(workActions.ActionTypes.CHANGE_TAG_NAME_ACTION),
    switchMap(action =>
      this.workService.changeTagName(action.payload)
        .pipe(
          map((res: IApiResponse) => {
            const result: IAuthResponse = res.result;

            return new EmptySuccessAction();
          }),
          catchError(err => this.api.catchError(err))
        )
    )
  );
}
