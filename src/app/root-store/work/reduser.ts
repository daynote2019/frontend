import {Actions, ActionTypes} from './actions';
import {createFeatureSelector, createSelector, MemoizedSelector} from '@ngrx/store';
import {
  addOneStructureInfo,
  closeTab,
  expandParentsMethod,
  openTab,
  removeStructureInfo,
  setStructureInfoToActiveState,
  sortStructureInfo,
  changeStructureInfoAnyValue,
  selectRecoverStructureInfo,
  addSectionInfo,
  renameSectionInfo,
  removeSectionInfo,
  setActiveInternalUser,
  addInternalUser,
  setStructureInfoByTagsMethod,
  updateWorkAreaElementsByActiveSection,
  recoverSectionInfo,
  closeTabsForAllChildrenDocs,
  openCloseFolderMethod, changeAdditionalInfoForSectionInfoMethod, changeAdditionalInfoForInternalUserMethod
} from './methods';
import {InternalUsers, IRemoveStructureInfo, ISectionInfo, ITag, StructureInfo, Tab} from '@shared/interfaces/work';
import {GlobalsService} from '@shared/services/globals.service';
import {
  addTagIdToActiveStruct,
  addTagIdToStructInfo,
  removeOneTagFromActiveStruct,
  removeOneTagFromStructInfo,
  addTagToAllTags,
  changeTagNameFromAllTags, changeTagNameFromActiveStruct, removeOneTagFromAllStructInfo, changeTagNameFromAllStructInfo
} from '@app/root-store/common/methods';
import {IEmployee, IProfileInfo} from '@shared/interfaces/profile';
import {StructureInfoType} from "@shared/enums/work";
import {IAllAndActiveSections} from "@shared/interfaces/common";

export interface WorkState {
  employee: IEmployee;
  structureInfoByTags: Array<StructureInfo>; // Могут быть из разных вн. юзеров и секций
  internalUsers: Array<InternalUsers>;
  sectionInfos: Array<ISectionInfo>;
  structureInfo: Array<StructureInfo>;
  allSectionInfos: Array<ISectionInfo>;
  allStructureInfo: Array<StructureInfo>;
  tabs: Array<Tab>;
  tags: ITag[]|null;
  selectedTags: string[]|null;
  activeStruct: StructureInfo|null;
  recoverStruct: StructureInfo|null;
  activeSection: ISectionInfo|null;
  activeInternalUser: InternalUsers|null;
  leftSidebarWidth: number;
}

export const initialState: WorkState = {
  employee: { id: '' },
  structureInfoByTags: [],
  internalUsers: [],
  sectionInfos: [],
  structureInfo: [],
  allSectionInfos: [],
  allStructureInfo: [],
  tabs: [],
  tags: [],
  selectedTags: [],
  activeStruct: null,
  recoverStruct: null,
  activeSection: null,
  activeInternalUser: null,
  leftSidebarWidth: GlobalsService.leftSidebarWidth,
};

export function workReducer(state = initialState, action: Actions): WorkState {
  switch (action.type) {
    case ActionTypes.SET_WORK_INFO: {
      return {
        ...state,
        internalUsers: action.workInfo.internalUsers ? action.workInfo.internalUsers : state.internalUsers,
        sectionInfos: action.workInfo.sectionInfos ? action.workInfo.sectionInfos : state.sectionInfos,
        // activeInternalUser: action.workInfo.internalUsers ? findActiveElement(action.workInfo.internalUsers) : state.activeInternalUser, // Активный вн. юзер добавляется только из массива вн. юзеров
        // activeSection: action.workInfo.sectionInfos ? findActiveElement(action.workInfo.sectionInfos) : state.activeSection, // Активная секция добавляется только из массива секций
        activeStruct: action.workInfo.activeStruct ? action.workInfo.activeStruct : state.activeStruct,
        tags: action.workInfo.tags ? action.workInfo.tags : state.tags,
      };
    }
    case ActionTypes.SET_ADDITIONAL_WORK_INFO: {
      return {
        ...state,
        internalUsers: action.workInfo.internalUsers ? action.workInfo.internalUsers : state.internalUsers,
        structureInfo: action.workInfo.structureInfo ? action.workInfo.structureInfo : state.structureInfo,
        allSectionInfos: action.workInfo.allSectionInfos ? action.workInfo.allSectionInfos : state.allSectionInfos,
        sectionInfos: action.workInfo.sectionInfos ? action.workInfo.sectionInfos : state.sectionInfos,
        allStructureInfo: action.workInfo.allStructureInfo ? action.workInfo.allStructureInfo : state.allStructureInfo,
      };
    }
    case ActionTypes.SET_STRUCTURE_INFO_TO_ACTIVE_STATE: {
      return {
        ...state,
        allStructureInfo: setStructureInfoToActiveState(action.payload, state),
        tabs: openTab(action.payload.tab, state)
      };
    }
    case ActionTypes.ADD_NEW_STRUCTURE_INFO: {
      const allAndActiveStructs = addOneStructureInfo(action.payload, state);
      let newTab: Tab = null;
      if (action.payload && action.payload.tab) {
        const _tab = action.payload.tab;
        newTab = {
          docId: _tab.docId,
          docParentId: _tab.docParentId,
          active: _tab.active,
          struct: allAndActiveStructs.newStruct
        };
      }
      const tabs = openTab(newTab, state);

      return {
        ...state,
        structureInfo: allAndActiveStructs.structureInfo,
        allStructureInfo: allAndActiveStructs.allStructureInfo,
        tabs: tabs,
        sectionInfos: changeAdditionalInfoForSectionInfoMethod(allAndActiveStructs.newStruct, tabs, state)
      };
    }
    case ActionTypes.REMOVE_STRUCTURE_INFO: {
      const result: IRemoveStructureInfo = removeStructureInfo(action.payload, state);
      const tabs = (action.payload.structType &&
        action.payload.structType === StructureInfoType.folder) ?
        closeTabsForAllChildrenDocs(result.childrenOfStructureInfo, state) :
        closeTab({
          docId: action.payload.structId,
          docParentId: action.payload.parentId
        }, state);
      return {
        ...state,
        tabs: tabs,
        allStructureInfo: result.allStructureInfo,
        sectionInfos: changeAdditionalInfoForSectionInfoMethod(result.activeStruct, tabs, state)
      };
    }
    case ActionTypes.DESELECT_ACTIVE_STRUCT: {
      return {
        ...state,
        // allStructureInfo: deselectStructureInfoActiveState(state),
        activeStruct: null
      };
    }
    case ActionTypes.SELECT_ACTIVE_STRUCT: {
      return {
        ...state,
        activeStruct: action.struct,
        sectionInfos: changeAdditionalInfoForSectionInfoMethod(action.struct, state.tabs, state) /* Могут ли вкладки измениться после отработки этого метода? */
      };
    }
    case ActionTypes.OPEN_CLOSE_FOLDER: {
      return {
        ...state,
        allStructureInfo: openCloseFolderMethod(action.struct, state.allStructureInfo),
      };
    }
    case ActionTypes.RECOVER_STRUCTURE_INFO: {
      return {
        ...state,
        allStructureInfo: selectRecoverStructureInfo(action.payload, state),
      };
    }
    case ActionTypes.CHANGE_STRUCTURE_INFO_TITLE: {
      return {
        ...state,
        structureInfo: sortStructureInfo(action.payload, state),
      };
    }
    case ActionTypes.CHANGE_STRUCTURE_INFO_ANY_VALUE: {
      return {
        ...state,
        allStructureInfo: changeStructureInfoAnyValue(action.payload, state),
      };
    }
    case ActionTypes.SET_TABS: {
      return {
        ...state,
        tabs: action.tabs
      };
    }
    case ActionTypes.OPEN_TAB: {
      return {
        ...state,
        tabs: openTab(action.tab, state)
      };
    }
    case ActionTypes.CLOSE_TAB: {
      if (action.payload.isSwitchToAnotherStruct) {
        state.activeStruct = action.payload.newActiveStruct
      }
      const tabs = closeTab(action.payload.tab, state);
      return {
        ...state,
        tabs: tabs,
        sectionInfos: changeAdditionalInfoForSectionInfoMethod(state.activeStruct, tabs, state)
      };
    }
    case ActionTypes.EXPAND_PARENTS: {
      return {
        ...state,
        allStructureInfo: expandParentsMethod(action.payload, state)
      };
    }
    case ActionTypes.SET_LEFT_SIDEBAR_WIDTH: {
      return {
        ...state,
        leftSidebarWidth: action.width
      };
    }
    case ActionTypes.SET_ACTIVE_SECTION: {
      const section: ISectionInfo|null = action.payload;
      return {
        ...state,
        activeSection: action.payload,
        internalUsers: changeAdditionalInfoForInternalUserMethod((section && section.id) ? section.id : '', state)
      };
    }
    case ActionTypes.UPDATE_WORK_AREA_ELEMENTS_BY_ACTIVE_SECTION: {
      return {
        ...state,
        structureInfo: updateWorkAreaElementsByActiveSection(action.payload, state)
      };
    }
    case ActionTypes.ADD_SECTION_INFO: {
      const allAndActiveSections: IAllAndActiveSections = addSectionInfo(action.payload, state);
      const activeSection = action.payload ? action.payload : this.activeSection;
      return {
        ...state,
        sectionInfos: allAndActiveSections.section,
        allSectionInfos: allAndActiveSections.allSections,
        activeSection: activeSection,
        structureInfo: [],
        activeStruct: null,
        tabs: [],
        internalUsers: changeAdditionalInfoForInternalUserMethod((activeSection && activeSection.id) ? activeSection.id : '', state)
      };
    }
    case ActionTypes.RENAME_SECTION_INFO: {
      return {
        ...state,
        allSectionInfos: renameSectionInfo(action.payload, state)
      };
    }
    case ActionTypes.REMOVE_SECTION_INFO: {
      return {
        ...state,
        sectionInfos: removeSectionInfo(action.payload.section, state),
        internalUsers: changeAdditionalInfoForInternalUserMethod(action.payload.newActiveSectionId, state)
      };
    }
    case ActionTypes.RECOVER_SECTION_INFO: {
      return {
        ...state,
        sectionInfos: recoverSectionInfo(action.payload, state)
      };
    }
    case ActionTypes.SET_ACTIVE_INTERNAL_USER: {
      return {
        ...state,
        internalUsers: setActiveInternalUser(action.payload, state),
        activeInternalUser: action.payload,
      };
    }
    case ActionTypes.SELECT_ACTIVE_INTERNAL_USER: {
      const activeIUser = action.payload.iUser;
      const _activeInternalUser: InternalUsers = activeIUser;
      let _activeSection = null;
      const sectionInfos = state.allSectionInfos.filter(_section => {
        if (_section.internalUserId === activeIUser.id && !_section.isRemoved) {
          if (_section.id === activeIUser.activeSectionId) {
            _activeSection = _section;
          }
          return _section;
        }
      });
      return {
        ...state,
        internalUsers: setActiveInternalUser(_activeInternalUser, state),
        activeInternalUser: _activeInternalUser,
        sectionInfos: sectionInfos,
        activeSection: _activeSection,
        structureInfo: updateWorkAreaElementsByActiveSection(_activeSection, state)
      };
    }
    case ActionTypes.ADD_INTERNAL_USER: {
      const _newAndActiveSection = action.sectionInfo;
      const _newInternalUser = action.internalUser;
      const _allSectionInfos = state.allSectionInfos;
      console.log(1, action);
      _allSectionInfos.push(_newAndActiveSection);
      return {
        ...state,
        internalUsers: addInternalUser(_newInternalUser, state),
        activeInternalUser: _newInternalUser,
        sectionInfos: [_newAndActiveSection],
        allSectionInfos: _allSectionInfos,
        activeSection: _newAndActiveSection,
        structureInfo: [],
        activeStruct: null,
        tabs: []
      };
    }
    case ActionTypes.ADD_TAG_ACTION: {
      return {
        ...state,
        tags: addTagToAllTags(action.payload, state.tags),
        allStructureInfo: addTagIdToStructInfo(action.payload, state.allStructureInfo),
        // activeStruct: addTagIdToActiveStruct(action.payload, state.activeStruct),
      };
    }
    case ActionTypes.REMOVE_TAG_FROM_FILE_ACTION: {
      return {
        ...state,
        allStructureInfo: removeOneTagFromStructInfo(action.payload, state.allStructureInfo),
        // activeStruct: removeOneTagFromActiveStruct(state.activeStruct, action.payload, null)
      };
    }
    case ActionTypes.REMOVE_TAG_FROM_ALL_ACTION: {
      return {
        ...state,
        structureInfo: removeOneTagFromAllStructInfo(action.payload.tagId, state.structureInfo),
        // activeStruct: removeOneTagFromActiveStruct(state.activeStruct, null, action.payload.tagId),
        tags: state.tags.filter(_tag => _tag.id !== action.payload.tagId)
      };
    }
    case ActionTypes.CHANGE_TAG_NAME_ACTION: {
      return {
        ...state,
        tags: changeTagNameFromAllTags(action.payload, state.tags),
        allStructureInfo: changeTagNameFromAllStructInfo(action.payload, state.allStructureInfo),
        // activeStruct: changeTagNameFromActiveStruct(action.payload, state.activeStruct)
      };
    }
    case ActionTypes.SET_STRUCT_INFOS_BY_TAG_ACTION: {
      return {
        ...state,
        structureInfoByTags: setStructureInfoByTagsMethod(action.payload, state),
        selectedTags: action.payload.selectedTags
      };
    }
    default: {
      return state;
    }
  }
}

export const selectWorkState: MemoizedSelector<object, WorkState> = createFeatureSelector<WorkState>('workReducer');

export const getInternalUsers = (state: WorkState): Array<InternalUsers> => state.internalUsers;
export const selectInternalUsers: MemoizedSelector<object, Array<InternalUsers>> = createSelector(selectWorkState, getInternalUsers);

export const getSectionInfos = (state: WorkState): Array<ISectionInfo> => state.sectionInfos;
export const selectSectionInfos: MemoizedSelector<object, Array<ISectionInfo>> = createSelector(selectWorkState, getSectionInfos);

export const getAllStructureInfo = (state: WorkState): Array<StructureInfo> => state.allStructureInfo;
export const selectAllStructureInfo: MemoizedSelector<object, Array<StructureInfo>> = createSelector(selectWorkState, getAllStructureInfo);

export const getStructureInfo = (state: WorkState): Array<StructureInfo> => state.structureInfo;
export const selectStructureInfo: MemoizedSelector<object, Array<StructureInfo>> = createSelector(selectWorkState, getStructureInfo);

export const getStructureInfoByTags = (state: WorkState): Array<StructureInfo> => state.structureInfoByTags;
export const selectStructureInfoByTags: MemoizedSelector<object, Array<StructureInfo>> = createSelector(selectWorkState, getStructureInfoByTags);

export const getTabs = (state: WorkState): Array<Tab> => state.tabs;
export const selectTabs: MemoizedSelector<object, Array<Tab>> = createSelector(selectWorkState, getTabs);

export const getTags = (state: WorkState): Array<ITag> => state.tags;
export const selectTags: MemoizedSelector<object, Array<ITag>> = createSelector(selectWorkState, getTags);

export const getSelectedTags = (state: WorkState): Array<string> => state.selectedTags;
export const selectSelectedTags: MemoizedSelector<object, Array<string>> = createSelector(selectWorkState, getSelectedTags);

export const getActiveStruct = (state: WorkState): StructureInfo|null => state.activeStruct;
export const selectActiveStruct: MemoizedSelector<object, StructureInfo|null> = createSelector(selectWorkState, getActiveStruct);

export const getRecoverStruct = (state: WorkState): StructureInfo|null => state.recoverStruct;
export const selectRecoverStruct: MemoizedSelector<object, StructureInfo|null> = createSelector(selectWorkState, getRecoverStruct);

export const getActiveSection = (state: WorkState): ISectionInfo|null => state.activeSection;
export const selectActiveSection: MemoizedSelector<object, ISectionInfo|null> = createSelector(selectWorkState, getActiveSection);

export const getActiveInternalUser = (state: WorkState): InternalUsers|null => state.activeInternalUser;
export const selectActiveInternalUser: MemoizedSelector<object, InternalUsers|null> = createSelector(selectWorkState, getActiveInternalUser);

export const getLeftSidebarWidth = (state: WorkState): number => state.leftSidebarWidth;
export const selectLeftSidebarWidth: MemoizedSelector<object, number> = createSelector(selectWorkState, getLeftSidebarWidth);

export const getProfileInfo = (state: WorkState): IProfileInfo => {
  return {
    employee: state.employee
  }};
export const selectProfileInfo: MemoizedSelector<object, IProfileInfo> = createSelector(selectWorkState, getProfileInfo);
