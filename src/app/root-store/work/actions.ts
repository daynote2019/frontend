import { Action } from '@ngrx/store';
import {
  AdditionalWorkInfo,
  InternalUsers,
  IRemoveSectionInfoServer,
  ISectionInfo, ISelectActiveInternalUser,
  ISetStructureInfoToActiveState,
  ITag,
  IUpdateWorkAreaElements,
  StructureInfo,
  Tab,
  WorkInfo
} from '@shared/interfaces/work';
import {
  IChooseTagRequest, ICloseTabRequest, IExpandParentsRequest,
  IRecoverStructureInfoRequest,
  IRemoveStructureInfoRequest, ISelectActiveStructInfo,
  IStructureInfoAnyValue,
  IStructureInfoRequest,
  IStructureInfoServerRequest,
  IStructureInfoTitleRequest,
  ITagChangeRequest,
  ITagRemoveFromAllRequest
} from '@shared/interfaces/request';
import {IStructureInfoByTag, ITagForFile} from '@shared/interfaces/common';

export enum ActionTypes {
  SET_WORK_INFO = '[work] SET_WORK_INFO post',
  SET_ADDITIONAL_WORK_INFO = 'SET_ADDITIONAL_WORK_INFO',
  SELECT_ADDITIONAL_WORK_INFO = '[work] SELECT_ADDITIONAL_WORK_INFO post',
  ADD_NEW_STRUCTURE_INFO = '[work] ADD_NEW_STRUCTURE_INFO',
  ADD_NEW_STRUCTURE_INFO_SERVER = '[work] ADD_NEW_STRUCTURE_INFO_SERVER',
  REMOVE_STRUCTURE_INFO = '[work] REMOVE_STRUCTURE_INFO post',
  REMOVE_STRUCTURE_INFO_SERVER = '[work] REMOVE_STRUCTURE_INFO_SERVER post',
  RECOVER_STRUCTURE_INFO = 'RECOVER_STRUCTURE_INFO',
  RECOVER_STRUCTURE_INFO_SERVER = 'RECOVER_STRUCTURE_INFO_SERVER',
  SET_STRUCTURE_INFO_TO_ACTIVE_STATE = '[work] SET_STRUCTURE_INFO_TO_ACTIVE_STATE post',
  DESELECT_ACTIVE_STRUCT = '[work] DESELECT_ACTIVE_STRUCT post',
  SELECT_ACTIVE_STRUCT = 'SELECT_ACTIVE_STRUCT',
  SELECT_ACTIVE_STRUCT_SERVER = 'SELECT_ACTIVE_STRUCT_SERVER',
  OPEN_CLOSE_FOLDER = 'OPEN_CLOSE_FOLDER',
  CHANGE_STRUCTURE_INFO_TITLE = 'CHANGE_STRUCTURE_INFO_TITLE',
  CHANGE_STRUCTURE_INFO_ANY_VALUE = 'CHANGE_STRUCTURE_INFO_ANY_VALUE',
  SET_TABS = '[work] SET_TABS post',
  OPEN_TAB = '[work] OPEN_TAB post',
  CLOSE_TAB = '[work] CLOSE_TAB post',
  EXPAND_PARENTS = '[work] EXPAND_PARENTS post',
  SET_LEFT_SIDEBAR_WIDTH = 'SET_LEFT_SIDEBAR_WIDTH',
  SET_ACTIVE_SECTION = 'SET_ACTIVE_SECTION',
  SELECT_SECTION_SERVER = 'SELECT_SECTION_SERVER',
  UPDATE_WORK_AREA_ELEMENTS_BY_ACTIVE_SECTION = 'UPDATE_WORK_AREA_ELEMENTS_BY_ACTIVE_SECTION',
  CREATE_SECTION_INFO_ON_SERVER = 'CREATE_SECTION_INFO_ON_SERVER', // Создать секцию на сервере(имеется ввиду этот экшен только для эффектов)
  ADD_SECTION_INFO = 'ADD_SECTION_INFO',
  RENAME_SECTION_INFO = 'RENAME_SECTION_INFO',
  REMOVE_SECTION_INFO = 'REMOVE_SECTION_INFO',
  REMOVE_SECTION_INFO_SERVER = 'REMOVE_SECTION_INFO_SERVER',
  RECOVER_SECTION_INFO = 'RECOVER_SECTION_INFO',
  RECOVER_SECTION_INFO_SERVER = 'RECOVER_SECTION_INFO_SERVER',
  SELECT_ACTIVE_INTERNAL_USER = 'SELECT_ACTIVE_INTERNAL_USER',
  SELECT_INTERNAL_USER_SERVER = 'SELECT_INTERNAL_USER_SERVER',
  SET_ACTIVE_INTERNAL_USER = 'SET_ACTIVE_INTERNAL_USER',
  CREATE_INTERNAL_USER = 'CREATE_INTERNAL_USER',
  ADD_INTERNAL_USER = 'ADD_INTERNAL_USER',
  ADD_TAG_ACTION = 'ADD_TAG_ACTION',
  REMOVE_TAG_FROM_FILE_ACTION = 'REMOVE_TAG_FROM_FILE_ACTION',
  REMOVE_TAG_FROM_ALL_ACTION = 'REMOVE_TAG_FROM_ALL_ACTION',
  CHANGE_TAG_NAME_ACTION = 'CHANGE_TAG_NAME_ACTION',
  SET_STRUCT_INFOS_BY_TAG_ACTION = 'SET_STRUCT_INFOS_BY_TAG_ACTION',
}

export class SetWorkInfoAction implements Action {
  readonly type = ActionTypes.SET_WORK_INFO;
  constructor(public workInfo: WorkInfo) {}
}
export class SetAdditionalWorkInfoAction implements Action {
  readonly type = ActionTypes.SET_ADDITIONAL_WORK_INFO;
  constructor(public workInfo: WorkInfo) {}
}
export class SelectAdditionalWorkInfoAction implements Action {
  readonly type = ActionTypes.SELECT_ADDITIONAL_WORK_INFO;
  constructor(public activeInternalUser: InternalUsers|null) {}
}
export class SetStructureInfoToActiveStateAction implements Action {
  readonly type = ActionTypes.SET_STRUCTURE_INFO_TO_ACTIVE_STATE;
  constructor(public payload: ISetStructureInfoToActiveState) {}
}
export class AddNewStructureInfoServerAction implements Action {
  readonly type = ActionTypes.ADD_NEW_STRUCTURE_INFO_SERVER;
  constructor(public payload: IStructureInfoServerRequest) {}
}
export class AddNewStructureInfoAction implements Action {
  readonly type = ActionTypes.ADD_NEW_STRUCTURE_INFO;
  constructor(public payload: IStructureInfoRequest) {}
}
export class RemoveStructureInfoServerAction implements Action {
  readonly type = ActionTypes.REMOVE_STRUCTURE_INFO_SERVER;
  constructor(public payload: IRemoveStructureInfoRequest) {}
}
export class RemoveStructureInfoAction implements Action {
  readonly type = ActionTypes.REMOVE_STRUCTURE_INFO;
  constructor(public payload: IRemoveStructureInfoRequest) {}
}
export class RecoverStructureInfoServerAction implements Action {
  readonly type = ActionTypes.RECOVER_STRUCTURE_INFO_SERVER;
  constructor(public payload: IRecoverStructureInfoRequest) {}
}
export class RecoverStructureInfoAction implements Action {
  readonly type = ActionTypes.RECOVER_STRUCTURE_INFO;
  constructor(public payload: IRecoverStructureInfoRequest) {}
}
export class DeselectActiveStructAction implements Action {
  readonly type = ActionTypes.DESELECT_ACTIVE_STRUCT;
  constructor() {}
}
export class SelectActiveStructServerAction implements Action {
  readonly type = ActionTypes.SELECT_ACTIVE_STRUCT_SERVER;
  constructor(public payload: ISelectActiveStructInfo) {}
}
export class SelectActiveStructAction implements Action {
  readonly type = ActionTypes.SELECT_ACTIVE_STRUCT;
  constructor(public struct: StructureInfo|null) {}
}
export class OpenCloseFolderAction implements Action {
  readonly type = ActionTypes.OPEN_CLOSE_FOLDER;
  constructor(public struct: StructureInfo) {}
}
export class ChangeStructureInfoTitle implements Action {
  readonly type = ActionTypes.CHANGE_STRUCTURE_INFO_TITLE;
  constructor(public payload: IStructureInfoTitleRequest) {}
}
export class ChangeStructureInfoAnyValue implements Action {
  readonly type = ActionTypes.CHANGE_STRUCTURE_INFO_ANY_VALUE;
  constructor(public payload: IStructureInfoAnyValue) {}
}
export class ExpandParents implements Action {
  readonly type = ActionTypes.EXPAND_PARENTS;
  constructor(public payload: IExpandParentsRequest) {}
}
export class SetTabsAction implements Action {
  readonly type = ActionTypes.SET_TABS;
  constructor(public tabs: Array<Tab>) {}
}
export class OpenTabAction implements Action {
  readonly type = ActionTypes.OPEN_TAB;
  constructor(public tab: Tab) {}
}
export class CloseTabAction implements Action {
  readonly type = ActionTypes.CLOSE_TAB;
  constructor(public payload: ICloseTabRequest) {}
}
export class SetLeftSidebarWidth implements Action {
  readonly type = ActionTypes.SET_LEFT_SIDEBAR_WIDTH;
  constructor(public width: number) {}
}
export class SetActiveSectionAction implements Action {
  readonly type = ActionTypes.SET_ACTIVE_SECTION;
  constructor(public payload: ISectionInfo) {}
}
export class SelectSectionServerAction implements Action {
  readonly type = ActionTypes.SELECT_SECTION_SERVER;
  constructor(public payload: ISectionInfo) {}
}
export class UpdateWorkAreaElementsByActiveSectionAction implements Action {
  readonly type = ActionTypes.UPDATE_WORK_AREA_ELEMENTS_BY_ACTIVE_SECTION;
  constructor(public payload: ISectionInfo|null) {}
}
export class CreateSectionAction implements Action {
  readonly type = ActionTypes.CREATE_SECTION_INFO_ON_SERVER ;
  constructor(public name: string) {}
}
export class AddSectionAction implements Action {
  readonly type = ActionTypes.ADD_SECTION_INFO ;
  constructor(public payload: ISectionInfo) {}
}
export class RenameSectionAction implements Action {
  readonly type = ActionTypes.RENAME_SECTION_INFO ;
  constructor(public payload: ISectionInfo) {}
}
export class RemoveSectionInfoServerAction implements Action {
  readonly type = ActionTypes.REMOVE_SECTION_INFO_SERVER ;
  constructor(public payload: IRemoveSectionInfoServer) {}
}
export class RemoveSectionInfoAction implements Action {
  readonly type = ActionTypes.REMOVE_SECTION_INFO ;
  constructor(public payload: IRemoveSectionInfoServer) {}
}
export class RecoverSectionInfoServerAction implements Action {
  readonly type = ActionTypes.RECOVER_SECTION_INFO_SERVER ;
  constructor(public payload: ISectionInfo) {}
}
export class RecoverSectionInfoAction implements Action {
  readonly type = ActionTypes.RECOVER_SECTION_INFO ;
  constructor(public payload: ISectionInfo) {}
}
export class SetActiveInternalUserAction implements Action {
  readonly type = ActionTypes.SET_ACTIVE_INTERNAL_USER;
  constructor(public payload: InternalUsers) {}
}
export class SelectInternalUserServerAction implements Action {
  readonly type = ActionTypes.SELECT_INTERNAL_USER_SERVER;
  constructor(public payload: ISelectActiveInternalUser) {}
}
export class SelectActiveInternalUser implements Action {
  readonly type = ActionTypes.SELECT_ACTIVE_INTERNAL_USER;
  constructor(public payload: ISelectActiveInternalUser) {}
}
export class CreateInternalUserAction implements Action {
  readonly type = ActionTypes.CREATE_INTERNAL_USER;
  constructor(public payload: InternalUsers) {}
}
export class AddInternalUserAction implements Action {
  readonly type = ActionTypes.ADD_INTERNAL_USER;
  constructor(public internalUser: InternalUsers, public sectionInfo: ISectionInfo) {}
}
export class AddTagAction implements Action {
  readonly type = ActionTypes.ADD_TAG_ACTION;
  constructor(public payload: ITagForFile) {}
}
export class RemoveTagFromFileAction implements Action {
  readonly type = ActionTypes.REMOVE_TAG_FROM_FILE_ACTION;
  constructor(public payload: ITagForFile) {}
}
export class RemoveTagFromAllAction implements Action {
  readonly type = ActionTypes.REMOVE_TAG_FROM_ALL_ACTION;
  constructor(public payload: ITagRemoveFromAllRequest) {}
}
export class ChangeTagNameAction implements Action {
  readonly type = ActionTypes.CHANGE_TAG_NAME_ACTION;
  constructor(public payload: ITagChangeRequest) {}
}
export class SetStructureInfoByTag implements Action {
  readonly type = ActionTypes.SET_STRUCT_INFOS_BY_TAG_ACTION;
  constructor(public payload: IStructureInfoByTag) {}
}

export type Actions = SetWorkInfoAction | SetAdditionalWorkInfoAction | SelectAdditionalWorkInfoAction | OpenTabAction |
  AddNewStructureInfoAction | RemoveStructureInfoAction | RecoverStructureInfoAction |
  SetStructureInfoToActiveStateAction | DeselectActiveStructAction |
  SelectActiveStructAction | OpenCloseFolderAction | ChangeStructureInfoTitle |
  ChangeStructureInfoAnyValue | CloseTabAction | SetTabsAction | ExpandParents | SetLeftSidebarWidth |
  SetActiveSectionAction | UpdateWorkAreaElementsByActiveSectionAction | CreateSectionAction | AddSectionAction |
  RenameSectionAction | RemoveSectionInfoAction | RecoverSectionInfoAction | SelectActiveInternalUser | SetActiveInternalUserAction |
  AddInternalUserAction | AddTagAction | RemoveTagFromFileAction | ChangeTagNameAction | RemoveTagFromAllAction |
  SetStructureInfoByTag;
