import {
  InternalUsers,
  IRemoveStructureInfo,
  ISectionInfo,
  ISetStructureInfoToActiveState,
  IUpdateWorkAreaElements,
  StructureInfo,
  Tab
} from '@shared/interfaces/work';
import {WorkState} from '@app/root-store/work/reduser';
import {HelpersService} from '@shared/services/helpers.service';
import {StructureInfoType} from '@shared/enums/work';
import {
  IExpandParentsRequest,
  IFolderControlRequest,
  IRecoverStructureInfoRequest,
  IRemoveStructureInfoRequest,
  IStructureInfoAnyValue,
  IStructureInfoRequest,
  IStructureInfoTitleRequest
} from '@shared/interfaces/request';
import {GlobalsService} from '@shared/services/globals.service';
import {
  IAllAndActiveSections,
  IAllAndActiveStructsSerializeResult,
  IStructureInfoByTag
} from '@shared/interfaces/common';

/** Добавить элемент структуры информации(папку, документ) */
export function addOneStructureInfo(newStruct: IStructureInfoRequest,
                                    state: WorkState): IAllAndActiveStructsSerializeResult {
  const structureInfo = state.structureInfo;
  const allStructureInfo = state.allStructureInfo;
  const helpers = new HelpersService();
  // const find = helpers.findInTree('children');
  const isOpen = newStruct.clickStructType === StructureInfoType.folder;
  const struct: StructureInfo = {
    id: newStruct.id,
    sectionId: newStruct.sectionId,
    level: newStruct.level,
    parentId: newStruct.parentId,
    type: newStruct.clickStructType,
    isOpen,
    title: newStruct.title,
    body: '',
    readonly: false, /* Чтобы появился фокус у элемента */
    isFocus: true, /* Чтобы появился фокус у элемента */
    isRemoved: false,
    createdAt: '', // TODO Пока не разобрался в каком формате буду на фронте хранить время.
    updatedAt: ''
  };
  if (newStruct.parent !== null) {
    // const parentStruct: StructureInfo = find(structureInfo, 'id', newStruct.parentId);
    const parentStruct: StructureInfo = newStruct.parent;
    parentStruct.isOpen = true;
    struct.parent = parentStruct;
    if (parentStruct.children && parentStruct.children.length > 0) {
      parentStruct.children.push(struct);
    } else {
      parentStruct.children = [];
      parentStruct.children.push(struct);
    }
  } else {
    structureInfo.push(struct);
  }
  allStructureInfo.push(struct);
  state.activeStruct = struct;
  // const actualActiveState = {
  //   structId: struct.id,
  //   type: struct.type,
  //   state: true
  // };
  // setStructureInfoToActiveState(actualActiveState, state); // Актуализировать состояние

  return {
    newStruct: struct,
    structureInfo: structureInfo,
    allStructureInfo: allStructureInfo
  };
}
/* Открыть/закрыть папку */
export function openCloseFolderMethod(struct: StructureInfo, allStructsInfo: StructureInfo[]): StructureInfo[] {
  const foundFolder = allStructsInfo.find(_struct => _struct.id === struct.id);
  /* Пока решил не запускать этот метод потому, что значение структуры-инфо меняется здесь [(ngModel)]="_struct.title" */
  if (foundFolder) {
    foundFolder.isOpen = struct.isOpen;
  }
  return allStructsInfo;
}
/** Удалить элемент структуры информации */
export function removeStructureInfo(structAction: IRemoveStructureInfoRequest,
                                    state: WorkState): IRemoveStructureInfo {
  const allStructureInfo = state.allStructureInfo;
  let allChildren: Array<StructureInfo> = [];
  const foundStruct = allStructureInfo.find(_struct =>
    _struct.id === structAction.structId);
  if (foundStruct) {
    foundStruct.isRemoved = true;
    if (foundStruct.type === StructureInfoType.folder) {
      foundStruct.isOpen = false;
    }
    /* Закрыть активные вкладки детей(документ) */
    if (foundStruct.children) {
      const helpers = new HelpersService();
      const find = helpers.findChildrenStructInfos();
      allChildren = find(foundStruct.children);
    }
    /* Назначить активную структуру */
    if (foundStruct.parent) {
      const parent = foundStruct.parent;
      parent.active = true;
      state.activeStruct = parent;
    } else { // Чтобы, при восстановлении указать null для activeStruct(для подписки)
      state.activeStruct = undefined;
    }
  }
  return {
    allStructureInfo,
    childrenOfStructureInfo: allChildren,
    activeStruct: state.activeStruct
  };
}
/** Сбросить активный статус у активной структуры информации */
export function deselectStructureInfoActiveState(state: WorkState, activeSection?: ISectionInfo) {
  const _activeSection = activeSection ? activeSection : state.activeSection;
  const allStructureInfo = state.allStructureInfo;
  /* Так как в одной секции активная структура может быть одна, то выполняем поиск элемента, а не фильтр эл-ов */
  const foundStruct = allStructureInfo.find(_struct =>
    (_activeSection && (_activeSection.id === _struct.sectionId)) && _struct.active);
  if (foundStruct) {
    foundStruct.active = false;
  }
  return allStructureInfo;
}
/** Закрыть все дочерние папки и сбросить активный статус у активной структуры информации */
export function closeChildrenFolders(state: WorkState,
                                                                        activeSection: ISectionInfo) {
  state.allStructureInfo.forEach(_struct => {
    if (activeSection.id === _struct.sectionId && _struct.type === StructureInfoType.folder) {
      _struct.isOpen = false;
    }
  });
}
/** Сбросить активный статус у активной структуры информации */
export function selectRecoverStructureInfo(structInfo: IRecoverStructureInfoRequest, state: WorkState) {
  const allStructureInfo = state.allStructureInfo;
  const foundStruct = allStructureInfo.find(_struct =>
    _struct.id === structInfo.structId);
  if (foundStruct) {
    foundStruct.isRemoved = false;
  }

  return allStructureInfo;
}
/**
 * Поменять состояние структуры информации
 * Сделать активными/неактивными папку или документ, открыть/закрыть папку
 */
export function setStructureInfoToActiveState(structAction: ISetStructureInfoToActiveState,
                                              state: WorkState): StructureInfo[] {
  const helpers = new HelpersService();
  return helpers.setStructureInfoToActiveStateHelper(state, structAction);
}
/**
 * Сортировка структуры информации в рамках его родителя
 * Здесь работаем именно с structureInfo потому, что после изменения названия, объекты нужно сортировать
 * Как я понимаю, если мы меняем structureInfo, то в allStructureInfo тоже должен поменяться этот объект
 * */
export function sortStructureInfo(struct: IStructureInfoTitleRequest, state: WorkState) {
  const structureInfo = state.structureInfo;
  const helpers = new HelpersService();
  const find = helpers.findInTree('children');
  const foundStruct: StructureInfo = find(structureInfo, 'id', struct.structId);

  if (foundStruct && foundStruct.parent) {
    const parent = foundStruct.parent;
    parent.children.sort(HelpersService.compareStructs);
  } else {
    structureInfo.sort(HelpersService.compareStructs);
  }

  return structureInfo;
}
/** Изменить какое-то значение структуры информации */
export function changeStructureInfoAnyValue(anyValues: IStructureInfoAnyValue, state: WorkState) {
  const allStructureInfo = state.allStructureInfo;
  const foundedStruct: StructureInfo = allStructureInfo.find(_struct => _struct.id === anyValues.structId);
  if (foundedStruct) {
    if (anyValues.fields) {
      anyValues.fields.forEach(field => {
        if (GlobalsService.isNotNullAndUndefined(foundedStruct[field.name])) {
          foundedStruct[field.name] = field.value;
        }
      });
    }
  }

  return allStructureInfo;
}
/** Добавить в хранилище одну вкладку */
export function openTab(tab: Tab, state: WorkState) {
  if (!tab) {
    return state.tabs;
  }
  let tabs = state.tabs;
  let tabIsExist = false;
  tabs.forEach(_tab => {
    const condition = _tab.docId === tab.docId;
    if (condition) { tabIsExist = true }
    _tab.active = condition;
  });
  if (!tabIsExist) {
    tabs.push(tab);
  }

  return tabs;
}
/** Закрыть вкладку */
export function closeTab(tab: Tab, state: WorkState) {
  const tabs = state.tabs;
  return tabs.filter(t => (t.docId !== tab.docId));
}
/** Закрыть вкладки всех дочерних файлов */
export function closeTabsForAllChildrenDocs(children: StructureInfo[], state: WorkState) {
  let tabs = state.tabs;
  children.forEach(_struct => {
    tabs = tabs.filter(t => t.docId !== _struct.id);
  });
  return tabs;
}
/** Открыть родительские папки начиная с потомка */
export function expandParentsMethod(payload: IExpandParentsRequest, state: WorkState) {
  const allStructureInfo = state.allStructureInfo;
  let foundParent = allStructureInfo.find(_struct =>
    _struct.id === payload.struct.parentId);
  while (foundParent) {
    foundParent.isOpen = true;
    foundParent = foundParent.parent;
  }

  return allStructureInfo;
}
/** Найти активный элемент секции или вн. пользователя */
export function findActiveElement(elements: any): any|null {
  if (!elements && !elements.length) {
    return null;
  }

  return elements.find(el => el.active);
}
/* Функция для того, чтобы сработала подписка activeSectionSubscription */
export function repaintSection(section: ISectionInfo,
                                 sectionInfos: ISectionInfo[]): ISectionInfo[] {
  if (section) {
    return sectionInfos.map(_section => {
      _section.tabs = section.tabs;
      return _section;
    });
  }

  return sectionInfos;
}
export function addSectionInfo(section: ISectionInfo,
                               state: WorkState): IAllAndActiveSections {
  let sectionInfos: ISectionInfo[] = state.sectionInfos;
  let allSectionInfos: ISectionInfo[] = state.allSectionInfos;
  if (section) {
    sectionInfos.push(section);
    allSectionInfos.push(section);
    // sectionInfos = repaintSection(section, sectionInfos);
  }

  return {
    section: sectionInfos,
    allSections: allSectionInfos
  };
}
export function renameSectionInfo(selectedSection: ISectionInfo, state: WorkState): ISectionInfo[] {
  const allSectionInfos: ISectionInfo[] = state.allSectionInfos;
  if (selectedSection) {
    const foundSection = allSectionInfos.find(_section => _section.id === selectedSection.id);
    if (foundSection) {
      foundSection.name = selectedSection.name;
    }
    // sectionInfos.forEach(_section => {
    //   if (_section.id === section.id) {
    //     _section.name = section.name;
    //     if (state.activeSection) {
    //       if (_section.id === state.activeSection.id) {
    //         state.activeSection.name = section.name;
    //       }
    //     }
    //   }
    // })
  }

  return allSectionInfos;
}
export function removeSectionInfo(sectionToRemove: ISectionInfo, state: WorkState): ISectionInfo[] {
  const sectionInfos: ISectionInfo[] = state.sectionInfos;
  if (!sectionToRemove) {
    return sectionInfos;
  }

  return sectionInfos.filter(_section => {
    if (_section.id !== sectionToRemove.id) { /* Возвращаем все секции, кроме удаляемой */
      return _section;
    }
    /** Очистить активную стр-инфо, вкладки и закрыть дочерние папки.
     * Объект должен обновиться и в allSectionInfos потому, что эта ссылка на него */
    _section.isRemoved = true;
    _section.activeStructId = '';
    _section.tabs = [];
    closeChildrenFolders(state, _section);
    // const foundSection = allSectionInfos.find(_section => _section.id === sectionToRemove.id);
    // if (foundSection) {
    //   foundSection.isRemoved = true;
    //   closeChildrenFolders(state, foundSection);
    // }
  });
}
export function recoverSectionInfo(sectionToRecover: ISectionInfo, state: WorkState): ISectionInfo[] {
  const allSectionInfos: ISectionInfo[] = state.allSectionInfos;
  const sectionInfos: ISectionInfo[] = state.sectionInfos;
  if (!sectionToRecover) {
    return sectionInfos;
  }
  const foundRecoverSection = allSectionInfos.find(_section =>_section.id === sectionToRecover.id);
  if (foundRecoverSection) {
    foundRecoverSection.isRemoved = false;
    /* Если ещё не успели переключиться на другого вн. юзера, то добавить в массив секций активного вн. юзера */
    if (state.activeInternalUser && state.activeInternalUser.id === foundRecoverSection.internalUserId) {
      sectionInfos.push(foundRecoverSection);
    }
  }

  return sectionInfos;
}
export function changeAdditionalInfoForSectionInfoMethod(activeStruct: StructureInfo|null, tabs: Tab[],
                                                       state: WorkState): ISectionInfo[] {
  return state.sectionInfos.map(_section => {
    if (_section.id === (state.activeSection ? state.activeSection.id : '')) {
      _section.activeStructId = activeStruct ? activeStruct.id : '';
      _section.tabs = tabs.map(_tab => _tab.docId);
    }
    return _section;
  });
}
export function addInternalUser(newIUsers: InternalUsers, state: WorkState): InternalUsers[] {
  let iUsers: InternalUsers[] = state.internalUsers;
  if (newIUsers) {
    iUsers.push(newIUsers);
    // iUsers = setActiveInternalUser(newIUsers, state);
  }

  return iUsers;
}
export function setActiveInternalUser(user: InternalUsers, state: WorkState): InternalUsers[] {
  const internalUsers: InternalUsers[] = state.internalUsers;
  if (user) {
    return internalUsers.map(_user => {
      _user.active = _user.id === user.id;
      return _user;
    });
  }

  return internalUsers;
}
export function changeAdditionalInfoForInternalUserMethod(activeSection: string|null, state: WorkState): InternalUsers[] {
  return state.internalUsers.map(_iUser => {
    if (activeSection && (_iUser.id === (state.activeInternalUser ? state.activeInternalUser.id : ''))) {
      _iUser.activeSectionId = activeSection;
    }
    return _iUser;
  });
}
export function setStructureInfoByTagsMethod(payload: IStructureInfoByTag,
                                             state: WorkState): Array<StructureInfo> {
  const helpers = new HelpersService();
  const result = helpers.selectStructuresByTags(payload.selectedTags, state.allStructureInfo);
  return result.sort(HelpersService.compareStructs);
}
/* Обновить элементы рабочей области по активной сессии */
export function updateWorkAreaElementsByActiveSection(section: ISectionInfo|null,
                                                state: WorkState): StructureInfo[] {
  if (!section) {
    return [];
  }
  const allStructureInfo = state.allStructureInfo;
  const sectionTabs: string[] = section.tabs? section.tabs : [];
  const tabs: Tab[] =  [];
  let activeStruct: StructureInfo|null = null;
  const structures = allStructureInfo.filter(_struct => {
    if ((_struct.sectionId === section.id) && !_struct.isRemoved) { /* Элементы активной секции, неудаленные */
      if (section.activeStructId === _struct.id) {
        activeStruct = _struct;
      }
      if (_struct.type === StructureInfoType.file) {
        sectionTabs.forEach(_tabStr => {
          if (_tabStr === _struct.id) {
            tabs.push(generateTabByStruct(_struct));
          }
        });
      }
      if (_struct.level === 0) { /* Возвращаем нулевые эл-ты, дочерние будут в дереве */
        return _struct;
      }
    }
  });
  state.tabs = tabs;
  state.activeStruct = activeStruct;

  return structures;
}

function generateTabByStruct(struct: StructureInfo): Tab {
  return { docId: struct.id, docParentId: struct.parentId, struct };
}
