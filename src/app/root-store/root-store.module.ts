import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import {mainReducer} from './common/reduser';
import {authReducer} from './auth/reduser';
import {workReducer} from './work/reduser';
import {editingAreaReducer} from './editing-area/reduser';
import {errorsReducer} from './errors/reduser';
import {MainStoreEffects} from './common/effects';
import {AuthStoreEffects} from './auth/effects';
import {WorkStoreEffects} from './work/effects';
import {EditingAreaEffects} from './editing-area/effects';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forRoot({
      mainReducer,
      errorsReducer,
      authReducer,
      workReducer,
      editingAreaReducer,
    }),
    EffectsModule.forRoot([
      MainStoreEffects,
      AuthStoreEffects,
      WorkStoreEffects,
      EditingAreaEffects,
    ]),
  ]
})
export class RootStoreModule { }

// npm install @ngrx/store @ngrx/effects --save
