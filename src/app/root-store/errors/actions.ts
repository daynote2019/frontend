import { Action } from '@ngrx/store';
import {ResponseValidateErrors} from '@shared/interfaces/errors';

export enum ActionTypes {
  RESPONSE_ERROR_VALIDATION = '[Errors] Response error',
}

export class ResponseErrorValidateAction implements Action {
  readonly type = ActionTypes.RESPONSE_ERROR_VALIDATION;
  constructor(public errors: ResponseValidateErrors) {}
}

export type Actions = ResponseErrorValidateAction;
