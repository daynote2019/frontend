import { Actions, ActionTypes } from './actions';
import {createFeatureSelector, createSelector, MemoizedSelector} from '@ngrx/store';
import {ResponseValidateErrors} from '@shared/interfaces/errors';

export interface ErrorsState {
  responseValidate: ResponseValidateErrors;
}

export const initialState: ErrorsState = {
  responseValidate: {},
};

export function errorsReducer(state = initialState, action: Actions): ErrorsState {
  switch (action.type) {
    case ActionTypes.RESPONSE_ERROR_VALIDATION: {
      return {
        ...state,
        responseValidate: action.errors
      };
    }
    default: {
      return state;
    }
  }
}

export const selectErrorsState: MemoizedSelector<object, ErrorsState> =
  createFeatureSelector<ErrorsState>('errorsReducer');

export const getResponseValidateErrors = (state: ErrorsState): ResponseValidateErrors => state.responseValidate;
export const selectResponseValidateErrors: MemoizedSelector<object, ResponseValidateErrors> =
  createSelector(selectErrorsState, getResponseValidateErrors);
