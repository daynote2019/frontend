import {Component, OnInit} from '@angular/core';
import {GlobalsService} from '@shared/services/globals.service';
import {WorkService} from '@shared/services/work.service';
import {ILeftSidebarFiller} from '@shared/interfaces/work';
import {ELeftToolbarPressedButton} from '@shared/enums/work';

@Component({
  selector: 'app-left-toolbar',
  templateUrl: './left-toolbar.component.html',
  styleUrls: ['./left-toolbar.component.scss']
})
export class LeftToolbarComponent implements OnInit {

  private leftSidebarStateSubscription;

  public isIEOrEdge = false;
  public leftSidebarState: ILeftSidebarFiller;
  public EPressedButton = ELeftToolbarPressedButton;
  public activeButtons: {
    [ELeftToolbarPressedButton.documents]: boolean,
    [ELeftToolbarPressedButton.tags]: boolean,
    [ELeftToolbarPressedButton.favourites]: boolean,
    [ELeftToolbarPressedButton.search]: boolean,
    [ELeftToolbarPressedButton.basket]: boolean,
  };

  constructor(
    private globals: GlobalsService,
    private workService: WorkService
  ) {
    this.activeButtons = {
      [ELeftToolbarPressedButton.documents]: true,
      [ELeftToolbarPressedButton.tags]: false,
      [ELeftToolbarPressedButton.favourites]: false,
      [ELeftToolbarPressedButton.search]: false,
      [ELeftToolbarPressedButton.basket]: false,
    };
  }

  ngOnInit() {
    this.isIEOrEdge = this.globals.isIEOrEdge();
    /* Подписаться на изменение состояния левой боковой панели */
    this.leftSidebarStateSubscription = this.workService
      .leftSidebarIsShow$
      .subscribe(state => {
        this.leftSidebarState = state;
        if (state && state.toolbarPressedButton) {
          this.changeActiveButton(ELeftToolbarPressedButton[state.toolbarPressedButton]);
        }
      });
  }

  ngOnDestroy() {
    this.leftSidebarStateSubscription.unsubscribe();
  }

  clickButton(button: ELeftToolbarPressedButton) {
    this.changeActiveButton(ELeftToolbarPressedButton[button]);
    this.workService.setLeftSidebarState({
      isShowSidebar: true,
      toolbarPressedButton: ELeftToolbarPressedButton[button]
    });
  }

  private changeActiveButton(buttonName: ELeftToolbarPressedButton) {
    Object.keys(this.activeButtons).forEach((_buttonName: string) => {
      this.activeButtons[_buttonName] = _buttonName === buttonName;
    });
  }
}
