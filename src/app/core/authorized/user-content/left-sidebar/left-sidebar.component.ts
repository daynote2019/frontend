import {Component, ElementRef, OnDestroy, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ResizeEvent} from 'angular-resizable-element';
import {ILeftSidebarFiller, StructureInfo} from '@shared/interfaces/work';
import {select, Store} from '@ngrx/store';
import * as workRoot from '@app/root-store/work/reduser';
import {
  CloseTabAction,
  DeselectActiveStructAction,
  SelectActiveStructServerAction,
  SetLeftSidebarWidth,
} from '@app/root-store/work/actions';
import {ELeftToolbarPressedButton, StructureInfoType} from '@shared/enums/work';
import {WorkService} from '@shared/services/work.service';
import * as mainRoot from '@app/root-store/common/reduser';
import {SessionService} from '@shared/services/session.service';
import {GlobalsService} from '@shared/services/globals.service';

@Component({
  selector: 'app-left-sidebar',
  templateUrl: './left-sidebar.component.html',
  styleUrls: ['./left-sidebar.component.scss',
    '../../../../shared/components/workspace/structure-info/structure-info.component.scss'],
  styles: [`
    mwlResizable {
      box-sizing: border-box; // required for the enableGhostResize option to work
    }
  `]
})
export class LeftSidebarComponent implements OnInit, OnDestroy {
  private activeStructSubscription;
  private structureInfoSubscription;
  private progressBarSubscription;
  private leftSidebarStateSubscription;

  public isLoad = true;
  public leftSidebarState: ILeftSidebarFiller;
  public StructureInfoType = StructureInfoType;
  public structureInfo: Array<StructureInfo>;
  public activeStruct: StructureInfo|null;
  public sidebarWidth: number = GlobalsService.leftSidebarWidth;

  public ELeftToolbarPressedButton = ELeftToolbarPressedButton;
  public GlobalsService = GlobalsService;

  constructor(
    private store$: Store<mainRoot.FeatureState>,
    private storeWork$: Store<workRoot.WorkState>,
    private workService: WorkService,
    private sessionService: SessionService,
    private globals: GlobalsService,
  ) { }

  ngOnInit() {
    this.activeStructSubscription = this.storeWork$
      .pipe(select(workRoot.selectActiveStruct))
      .subscribe(struct => {
        this.activeStruct = struct;
        // console.log(1, struct);
      });
    this.structureInfoSubscription = this.storeWork$
      .pipe(select(workRoot.selectStructureInfo))
      .subscribe(info => {
        this.structureInfo = info;
      });
    /* Подписаться на изменение информации о прогресс-баре */
    this.progressBarSubscription = this.workService
      .leftSidebarProgressBarIsActive$
      .subscribe(isLoad => {
        this.isLoad = isLoad;
      });
    /* Подписаться на изменение состояния левой боковой панели */
    this.leftSidebarStateSubscription = this.workService
      .leftSidebarIsShow$
      .subscribe(state => {
        this.leftSidebarState = state;
      });
  }

  ngOnDestroy() {
    this.activeStructSubscription.unsubscribe();
    this.structureInfoSubscription.unsubscribe();
    this.progressBarSubscription.unsubscribe();
    this.leftSidebarStateSubscription.unsubscribe();
  }

  closeLeftSidebar() {
    this.workService.setLeftSidebarState({
      isShowSidebar: false,
      toolbarPressedButton: this.leftSidebarState.toolbarPressedButton
    });
  }

  addNewStructureInfo(type: StructureInfoType) {
    this.workService.addNewStructElement(type);
  }

  removeElement() {
    this.workService.removeStructElement(this.activeStruct);
  }

  deselectActiveStruct() {
    if (this.activeStruct) {
      this.storeWork$.dispatch(new SelectActiveStructServerAction({
        struct: null
      }));
    }
  }

  resizeLeftSidebar($event: ResizeEvent) {
    let sidebarWidthResult = this.sidebarWidth;
    if ($event && $event.edges && ($event.edges.right !== null && typeof $event.edges.right !== 'undefined')) {
      const offset: number = (typeof $event.edges.right === 'number') ? $event.edges.right : 0;
      const sidebarWidth = sidebarWidthResult + offset;
      if (sidebarWidth > 500) {
        sidebarWidthResult = 500;
      } else if (sidebarWidth < GlobalsService.leftSidebarWidth) {
        sidebarWidthResult = GlobalsService.leftSidebarWidth;
      } else {
        sidebarWidthResult = sidebarWidth;
      }
    }
    this.sidebarWidth = sidebarWidthResult;
    this.storeWork$.dispatch(new SetLeftSidebarWidth(sidebarWidthResult));
  }

  onRightClickInput(event) {
    return false;
  }

  openTagsManagementModal() {
    this.globals.modalTagsManagementOpenClose({
      tagsManagementModal: {
        isOpenState: true
      }
    });
  }
}
