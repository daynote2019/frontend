import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import * as workRoot from '@app/root-store/work/reduser';
import * as editingAreaRoot from '@app/root-store/editing-area/reduser';
import {ITag, StructureInfo} from '@shared/interfaces/work';
import * as mainRoot from '@app/root-store/common/reduser';
import {ETagType, IFolderControlType, StructureInfoType} from '@shared/enums/work';
import {GlobalsService} from '@shared/services/globals.service';
import {WorkService} from '@shared/services/work.service';
import {ExpandParents} from '@app/root-store/work/actions';
import {AddTagServerAction, ChangeStructInfoFavouritesState, RemoveTagFromFileServerAction} from '@app/root-store/editing-area/actions';
import {ModalBasicCenterAction} from '@app/root-store/common/actions';
import {IModalBasicCenterType} from '@shared/enums/common';
import {fadeInAnimation} from "@shared/functions/animations";

@Component({
  selector: 'app-editing-area',
  templateUrl: './editing-area.component.html',
  styleUrls: ['./editing-area.component.scss',
    '../../../../shared/components/modals/modal-right-click/modal-right-click.component.scss'],
  animations: fadeInAnimation()
})
export class EditingAreaComponent implements OnInit {
  private activeStructSubscription;
  private structureInfoSubscription;
  private tagsSubscription;

  public structureInfo: Array<StructureInfo>;
  public activeStruct: StructureInfo|null;
  public tags: Array<ITag> = [];

  public structureInfoElem: Array<StructureInfo> = [];

  public Type = StructureInfoType;
  public GlobalsService = GlobalsService;

  public tagsByFileType: Array<ITag> = [];

  constructor(
    private store$: Store<mainRoot.FeatureState>,
    private storeWork$: Store<workRoot.WorkState>,
    private storeEditingArea$: Store<editingAreaRoot.EditingAreaState>,
    private workService: WorkService,
  ) { }

  ngOnInit() {
    this.structureInfoSubscription = this.storeWork$
      .pipe(select(workRoot.selectStructureInfo))
      .subscribe(info => {
        this.structureInfo = info;
      });
    this.activeStructSubscription = this.storeWork$
      .pipe(select(workRoot.selectActiveStruct))
      .subscribe(struct => {
        this.activeStruct = struct;
        this.structureInfoElem = [];
          setTimeout(() => {
            if (this.activeStruct) {
              if (this.activeStruct.type === StructureInfoType.folder) {
                this.structureInfoElem = this.activeStruct.children ?
                  GlobalsService.isNotEmptyArray(this.activeStruct.children) &&
                  GlobalsService.structsInfoNotRemoved(this.activeStruct.children) : [];
              }
              this.setTagsByFileType();
            } else { /* Показать все элементы с уровнем 0 */
              this.structureInfoElem = GlobalsService.isNotEmptyArray(this.structureInfo) ?
                GlobalsService.structsInfoNotRemoved(this.structureInfo) : [];
            }
          }, 100)
      });
    this.tagsSubscription = this.storeWork$
      .pipe(select(workRoot.selectTags))
      .subscribe(tags => {
        this.tags = tags;
        setTimeout(() => {
          this.setTagsByFileType();
        }, 100)
      });
  }

  ngOnDestroy() {
    this.activeStructSubscription.unsubscribe();
    this.structureInfoSubscription.unsubscribe();
    this.tagsSubscription.unsubscribe();
  }

  clickStruct(struct: StructureInfo) {
    this.workService.clickStructElement(struct);
    if (struct.parent && !struct.parent.isOpen) {
      /* Раскрыть родительскую папку(если она есть и закрыта) */
      this.storeWork$.dispatch(new ExpandParents({
        struct,
        closedParentsIds: [struct.parent.id]
      }));
    }
  }

  changeFavouritesState() {
    if (this.activeStruct) {
      const state = !this.activeStruct.isFavourites;
      this.storeEditingArea$.dispatch(
        new ChangeStructInfoFavouritesState({
          elementId: this.activeStruct.id,
          state
        })
      );
      this.activeStruct.isFavourites = state;
    }
  }

  onRightClick(event, struct: StructureInfo) {
    return false;
  }

  isFavourites() {
    let result = 'assets/img/common/star-empty.svg';
    if (this.activeStruct && this.activeStruct.isFavourites) {
      result = 'assets/img/common/star.svg';
    }

    return result;
  }

  favouritesText() {
    let result = 'tooltip.editingArea.addToFavorites';
    if (this.activeStruct && this.activeStruct.isFavourites) {
      result = 'tooltip.editingArea.removeFromFavorites';
    }

    return result;
  }

  isTags() {
    let result = 'assets/img/common/tagEmpty.svg';
    if (this.activeStruct && GlobalsService.isNotEmptyArray(this.activeStruct.chosenTags)) {
      result = 'assets/img/common/tag.svg';
    }

    return result;
  }

  addTag() {
    if (this.activeStruct) {
      this.store$.dispatch(new ModalBasicCenterAction({
        state: true,
        type: IModalBasicCenterType.confirmInputWithSelect,
        title: 'modalBasicCenter.addTag.title',
        message: 'modalBasicCenter.addTag.message',
        inputLabel: 'modalBasicCenter.addTag.inputLabel',
        notConfirmedInputSubmit: true,
        selectLabel: 'modalBasicCenter.addTag.selectLabel',
        selectArray: this.tags.filter(_tag => {
          const findTag = this.tagsByFileType.find(tag => tag.id === _tag.id);
          if (!findTag) { return _tag }
        }),
        selectBindLabel: 'name',
        selectBindValue: 'id',
        buttonActionName: 'modalBasicCenter.addTag.buttonActiveName',
        action: (newTagName?: string, existTagName?: string) => {
          const tagName = (GlobalsService.isNotNullAndUndefined(existTagName) && existTagName !== '') ?
            existTagName : newTagName;
          this.storeEditingArea$.dispatch(new AddTagServerAction({
            structId: this.activeStruct.id,
            name: tagName,
            type: ETagType.file
          }));
        }
      }));
    }
  }

  removeTagFromFile(tag: ITag) {
    this.storeEditingArea$.dispatch(new RemoveTagFromFileServerAction({
      structId: this.activeStruct.id,
      tagId: tag.id,
      name: tag.name,
      type: tag.type
    }));
    this.tagsByFileType = this.tagsByFileType.filter(_tag => _tag.id !== tag.id);
  }

  private setTagsByFileType() {
    this.tagsByFileType = [];
    if (this.activeStruct && this.activeStruct.type === StructureInfoType.file) {
      if (this.activeStruct.chosenTags &&
        GlobalsService.isNotEmptyArray(this.activeStruct.chosenTags)) {
        this.activeStruct.chosenTags.forEach(_tag => {
          if (_tag.type === ETagType.file) {
            this.tagsByFileType.push(_tag);
          }
        })
      }
    }
  }
}
