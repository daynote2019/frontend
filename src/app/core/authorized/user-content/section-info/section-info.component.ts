import {Component, ElementRef, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import * as workRoot from '@app/root-store/work/reduser';
import {ISectionInfo} from '@shared/interfaces/work';
import {GlobalsService} from '@shared/services/globals.service';
import {ModalBasicCenterAction} from '@app/root-store/common/actions';
import * as mainRoot from '@app/root-store/common/reduser';
import {IModalBasicCenterType} from '@shared/enums/common';
import {CreateSectionAction} from '@app/root-store/work/actions';

@Component({
  selector: 'app-section-info',
  templateUrl: './section-info.component.html',
  styleUrls: ['./section-info.component.scss',
    '../../../../shared/components/modals/modal-right-click/modal-right-click.component.scss']
})
export class SectionInfoComponent implements OnInit {

  private sectionInfosSubscription;
  private activeSectionSubscription;

  public GlobalService = GlobalsService;
  public sectionInfos: ISectionInfo[] = null;
  public activeSection: ISectionInfo = null;

  constructor(
    private eRef: ElementRef,
    private storeWork$: Store<workRoot.WorkState>,
    private store$: Store<mainRoot.FeatureState>,
  ) { }

  ngOnInit() {
    this.sectionInfosSubscription = this.storeWork$
      .pipe(select(workRoot.selectSectionInfos))
      .subscribe(sections => {
        // this.sectionInfos = sections.filter(_section => !_section.isRemoved);
        this.sectionInfos = sections;
      });
    this.activeSectionSubscription = this.storeWork$
      .pipe(select(workRoot.selectActiveSection))
      .subscribe(activeSection => {
        this.activeSection = activeSection;
      });
  }

  ngOnDestroy() {
    this.sectionInfosSubscription.unsubscribe();
    this.activeSectionSubscription.unsubscribe();
  }

  addSection() {
    this.store$.dispatch(new ModalBasicCenterAction({
      state: true,
      type: IModalBasicCenterType.confirmInput,
      title: 'modalBasicCenter.addSectionInfo.title',
      message: 'modalBasicCenter.addSectionInfo.message',
      inputLabel: 'modalBasicCenter.addSectionInfo.inputLabel',
      confirmInputCheck: (sectionNewName: string) => { // TODO В переименовывании секции это есть? тоже нужно добавить там.
        const foundSection = this.sectionInfos.find(_section =>
          !_section.isRemoved && _section.name === sectionNewName);
        return !!foundSection; // Если имя совпадает, то присылаем TRUE, чтобы валидатор показал ошибку
      },
      confirmInputCheckErrorTextTranslate: 'modalBasicCenter.addSectionInfo.sectionNameAlreadyExists',
      buttonActionName: 'modalBasicCenter.addSectionInfo.buttonActiveName',
      action: (name?: string) => {
        this.storeWork$.dispatch(new CreateSectionAction(name));
      }
    }));
  }
}
