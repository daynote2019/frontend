import {Component, ElementRef, HostListener, Input, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as workRoot from '@app/root-store/work/reduser';
import {ISectionInfo} from '@shared/interfaces/work';
import {
  RecoverSectionInfoServerAction,
  RemoveSectionInfoServerAction,
  RenameSectionAction, SelectSectionServerAction,
} from '@app/root-store/work/actions';
import {SessionService} from '@shared/services/session.service';
import {ModalBasicCenterAction, ModalInfoBottomRightAction} from '@app/root-store/common/actions';
import {IModalBasicCenterType} from '@shared/enums/common';
import * as mainRoot from '@app/root-store/common/reduser';
import {GlobalsService} from "@shared/services/globals.service";

@Component({
  selector: 'app-one-section',
  template: `
    <div class="si-sections-main" 
         (contextmenu)="openControlModal($event)">
        <div class="si-sections-item si-section-right-border" 
             [class.active]="section.id === (activeSection && activeSection.id)" 
             (click)="choose()">
            <p class="si-sections-item-text">{{section.name}}</p>
        </div>
        <div class="ls-triangle ls-triangle-open"
             (click)="openControlModal($event)"></div>
        <div *ngIf="isShowControlModal"
             class="modal-right-click base-modal-common si-control-list">
          <div class="element" (click)="choose()">
            <span class="text">{{ 'modalRightClick.element.choose' | translate }}</span>
          </div>
          <div class="element" (click)="rename()">
            <span class="text">{{ 'modalRightClick.element.rename' | translate }}</span>
          </div>
          <div class="element" (click)="remove()">
            <span class="text">{{ 'modalRightClick.element.remove' | translate }}</span>
          </div>
        </div>
    </div>
  `,
  styleUrls: ['./section-info.component.scss',
    '../../../../shared/components/modals/modal-right-click/modal-right-click.component.scss']
})
export class OneSectionComponent implements OnInit {
  @Input() public section: ISectionInfo;
  @Input() public sectionInfos: ISectionInfo[] = null;
  @Input() public activeSection: ISectionInfo = null;
  public isShowControlModal = false;

  constructor(
    private eRef: ElementRef,
    private storeWork$: Store<workRoot.WorkState>,
    private sessionService: SessionService,
    private store$: Store<mainRoot.FeatureState>,
  ) { }

  ngOnInit() {}

  @HostListener('document:click', ['$event'])
  @HostListener('document:contextmenu', ['$event'])
  clickout(event) {
    if(this.eRef.nativeElement.contains(event.target)) {} else {
      this.isShowControlModal = false;
    }
  }

  choose() {
    this.isShowControlModal = false;
    /* Если выбрали другую секцию(не ту на которой находимся) */
    if (this.activeSection && (this.activeSection.id !== this.section.id)) {
      this.chooseLocal(this.section);
    }
  }

  rename() {
    this.isShowControlModal = false;
    this.store$.dispatch(new ModalBasicCenterAction({
      state: true,
      type: IModalBasicCenterType.confirmInput,
      title: 'modalBasicCenter.renameSectionInfo.title',
      message: 'modalBasicCenter.renameSectionInfo.message',
      inputLabel: 'modalBasicCenter.renameSectionInfo.inputLabel',
      inputValue: this.section.name,
      buttonActionName: 'modalBasicCenter.renameSectionInfo.buttonActiveName',
      action: (newSectionName?: string) => {
        if (newSectionName !== this.section.name) {
          this.storeWork$.dispatch(new RenameSectionAction({
            id: this.section.id,
            name: newSectionName
          }));
        }
      }
    }));
  }

  remove() {
    this.isShowControlModal = false;
    if (this.sectionInfos && this.sectionInfos.length > 1) {
      let sectionToActive: ISectionInfo = null;
      /* Если удаляем секцию на которой сейчас находимся */
      if (this.activeSection && (this.activeSection.id === this.section.id)) {
        const filteredSections = this.sectionInfos.filter(_el => _el.id !== this.section.id);
        if (GlobalsService.isNotEmptyArray(filteredSections)) {
          sectionToActive = filteredSections[filteredSections.length-1]; /* Вычислили новую активную секцию */
        }
      }
      this.store$.dispatch(new ModalBasicCenterAction({
        state: true,
        type: IModalBasicCenterType.deleteStruct,
        title: 'modalBasicCenter.moveToTrash.title',
        message: 'modalBasicCenter.moveToTrash.messageForSection',
        elementName: this.section ? this.section.name : '',
        buttonActionName: 'modalBasicCenter.moveToTrash.buttonActiveName',
        action: () => {
          this.storeWork$.dispatch(new RemoveSectionInfoServerAction({
            section: this.section,
            newActiveSectionId: sectionToActive ? sectionToActive.id : null,
            success: () => {
              if (sectionToActive) { /* Если удаляем секцию на которой сейчас находимся */
                this.chooseLocal(sectionToActive);
              }
              setTimeout(() => {
                this.store$.dispatch(new ModalInfoBottomRightAction({
                  state: true,
                  type: IModalBasicCenterType.recoverStruct,
                  title: 'info.title',
                  message: 'info.sectionInfo.sectionRemoved',
                  action: () => {
                    this.storeWork$.dispatch(new RecoverSectionInfoServerAction(this.section));
                  }
                }));
              }, 300);
            }
          }));
        }
      }));
    } else {
      this.store$.dispatch(new ModalInfoBottomRightAction({
        state: true,
        type: IModalBasicCenterType.info,
        title: 'info.title',
        message: 'info.sectionInfo.sectionCannotBeRemoved',
      }));
    }
  }

  openControlModal(event) {
    this.isShowControlModal = true;
    return false;
  }

  private chooseLocal(section: ISectionInfo) {
    this.storeWork$.dispatch(new SelectSectionServerAction(section));
  }
}
