import {Component, OnDestroy, OnInit} from '@angular/core';
import {StructureInfo, Tab} from '@shared/interfaces/work';
import {select, Store} from '@ngrx/store';
import * as workRoot from '@app/root-store/work/reduser';
import {
  CloseTabAction,
  ExpandParents,
  OpenTabAction,
  SelectActiveStructServerAction,
} from '@app/root-store/work/actions';
import {HelpersService} from '@shared/services/helpers.service';
import {ELeftToolbarPressedButton} from '@shared/enums/work';
import {WorkService} from '@shared/services/work.service';
import {SessionService} from '@shared/services/session.service';
import {ISelectActiveStructInfo} from "@shared/interfaces/request";
import {GlobalsService} from "@shared/services/globals.service";

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit, OnDestroy {

  private structureInfoSubscription;
  private tabsSubscription;
  private activeStructSubscription;

  public tabs: Array<Tab>;
  public structureInfo: Array<StructureInfo>;
  public activeStruct: StructureInfo;

  constructor(
    private storeWork$: Store<workRoot.WorkState>,
    private helpersService: HelpersService,
    private workService: WorkService,
    private sessionService: SessionService,
    private globalService: GlobalsService,
  ) {
    this.structureInfoSubscription = this.storeWork$
      .pipe(select(workRoot.selectStructureInfo))
      .subscribe(info => {
      this.structureInfo = info;
    });
  }

  ngOnInit() {
    this.tabsSubscription = this.storeWork$
      .pipe(select(workRoot.selectTabs))
      .subscribe(tabs => {
        this.tabs = tabs;
      });
    this.activeStructSubscription = this.storeWork$
      .pipe(select(workRoot.selectActiveStruct))
      .subscribe(struct => {
        this.activeStruct = struct;
    });
  }

  ngOnDestroy() {
    this.structureInfoSubscription.unsubscribe();
    this.tabsSubscription.unsubscribe();
    this.activeStructSubscription.unsubscribe();
  }

  isActive(tab: Tab) {
    return (this.activeStruct && this.activeStruct.id) === (tab && tab.docId);
  }

  clickTab(tab: Tab) {
    if (this.activeStruct && this.activeStruct.id === tab.docId) { /* Если таб уже активный и кликнули по нему опять */
      this.workService.setLeftSidebarState({ /* Открыть левый сайдбар */
        isShowSidebar: true,
        toolbarPressedButton: ELeftToolbarPressedButton.documents
      });
      /* Проверить есть ли закрытые родительские папки */
      const closedParentsIds = this.globalService.getClosedParentsIds(tab.struct);
      if (GlobalsService.isNotEmptyArray(closedParentsIds)) {
        /* Если хотя бы одна папка закрыта, раскрываем всю иерархию до самой первой родительской папки(вверх) */
        this.storeWork$.dispatch(new ExpandParents({
          struct: tab.struct,
          closedParentsIds: this.globalService.getClosedParentsIds(tab.struct)
        }));
      }
    } else {
      const structToActiveState: ISelectActiveStructInfo = {
        struct: tab.struct,
      };
      this.storeWork$.dispatch(new OpenTabAction({
        docId: tab.docId,
        docParentId: tab.docParentId,
        struct : tab.struct
      }));
      this.storeWork$.dispatch(new SelectActiveStructServerAction(structToActiveState));
    }
  }

  closeTab(tab: Tab) {
    if ((this.activeStruct && this.activeStruct.id) &&
      tab.docId === this.activeStruct.id) { // Если закрыли вкладку на которой находимся(активная)
      this.storeWork$.dispatch(new CloseTabAction({
        tab,
        isSwitchToAnotherStruct: true,
        newActiveStruct: (tab.struct && tab.struct.parent) ? tab.struct.parent : null
      }));
    } else {
      this.storeWork$.dispatch(new CloseTabAction({
        tab,
        isSwitchToAnotherStruct: false
      }));
    }
  }

  onRightClick(event, tab: Tab) {
    // TODO при нажатии на правую кнопку мышки нужно открывать модалку-меню
    return false;
  }

  disableSelect() {
    // TODO mousedown никак не повлияет на перетаскивание(move)?
    return false;
  }
}
