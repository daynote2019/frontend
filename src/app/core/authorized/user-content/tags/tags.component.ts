import {Component, Input, OnInit} from '@angular/core';
import {HelpersService} from '@shared/services/helpers.service';
import {ETagType} from '@shared/enums/work';
import {GlobalsService} from '@shared/services/globals.service';
import {ITag, StructureInfo} from '@shared/interfaces/work';
import {Subject} from 'rxjs';
import {select, Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';
import * as workRoot from '@app/root-store/work/reduser';
import {SetStructureInfoByTag} from "@app/root-store/work/actions";
import {fadeInAnimation} from "@shared/functions/animations";

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss'],
  animations: fadeInAnimation()
})
export class TagsComponent implements OnInit {
  @Input() public activeStruct: StructureInfo;

  private tagsSubscription;
  private structureInfoByTagsSubscription;

  public tags: Array<ITag> = []; /* Этот массив не меняется */
  public structureInfoByTags: Array<StructureInfo>;
  public filteredTags: Array<ITag> = [];
  private filteredTagsSubject: Subject<Array<ITag>> = new Subject<Array<ITag>>();
  public chosenTagTypeState: {
    [ETagType.file]: boolean,
    [ETagType.text]: boolean
  };
  public chosenTagType: ETagType;

  public ETagType = ETagType;
  public GlobalsService = GlobalsService;

  constructor(
    private store$: Store<mainRoot.FeatureState>,
    private storeWork$: Store<workRoot.WorkState>
  ) {
    this.chosenTagTypeState = {
      [ETagType.file]: true,
      [ETagType.text]: false
    };
    this.chosenTagType = ETagType.file;
  }

  ngOnInit() {
    this.tagsSubscription = this.storeWork$
      .pipe(select(workRoot.selectTags))
      .subscribe(tags => {
        this.tags = tags;
        this.filteredTags = tags.filter(_tag => _tag.type === ETagType.file)
          .sort(HelpersService.compareTags);
        this.filteredTagsSubject.next(this.filteredTags.filter(_tag => !_tag.isSelected));
      });
    this.structureInfoByTagsSubscription = this.storeWork$
      .pipe(select(workRoot.selectStructureInfoByTags))
      .subscribe(infoByTags => {
        this.structureInfoByTags = infoByTags;
      });
  }

  ngOnDestroy() {
    this.tagsSubscription.unsubscribe();
    this.structureInfoByTagsSubscription.unsubscribe();
  }

  changeChosenTagTypeState(tagType: ETagType) {
    this.chosenTagType = tagType;
    Object.keys(this.chosenTagTypeState).forEach((_tagType: string) => {
      this.chosenTagTypeState[_tagType] = _tagType === tagType;
    });
    this.filteredTags = this.tags
      .filter(_tag => _tag.type === tagType)
      .sort(HelpersService.compareTags);
    this.filteredTagsSubject.next(this.filteredTags.filter(_tag => !_tag.isSelected));
    this.chooseTagAction();
  }

  chooseTag(tagId: string) {
    this.filteredTags = this.filteredTags.map(_tag =>
      this.markTagIsSelectToState(_tag, tagId, true)); /* В this.tag так же меняется объект, ибо один и тот же */
    this.filteredTagsSubject.next(this.filteredTags.filter(_tag => !_tag.isSelected));
    this.chooseTagAction();
  }

  removeTagFromSelectedById(tagId: string) {
    this.filteredTags = this.filteredTags.map(_tag =>
      this.markTagIsSelectToState(_tag, tagId, false)).sort(HelpersService.compareTags);
    this.filteredTagsSubject.next(this.filteredTags.filter(_tag => !_tag.isSelected));
    this.chooseTagAction();
  }

  filteredTagsPipe(filteredTags: Array<ITag>, state: boolean) {
    return filteredTags.filter(_tag => _tag.isSelected === state); /* false - не выбранные */
  }

  private markTagIsSelectToState(tag: ITag, tagId: string, state: boolean) {
    if (tag.id === tagId) {
      tag.isSelected = state;
    }
    return tag;
  }

  private chooseTagAction() {
    setTimeout(() => {
      this.storeWork$.dispatch(new SetStructureInfoByTag({
        selectedTags: this.filteredTags.filter(_tag => _tag.isSelected)
          .map(_tag => _tag.id)
      }))
    }, 20);
  }
}
