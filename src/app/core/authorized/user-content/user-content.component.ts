import {Component, OnDestroy, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import * as workRoot from '@app/root-store/work/reduser';
import {ISectionInfo} from '@shared/interfaces/work';
import {GlobalsService} from '@shared/services/globals.service';

@Component({
  selector: 'app-user-content',
  templateUrl: './user-content.component.html',
  styleUrls: ['./user-content.component.scss']
})
export class UserContentComponent implements OnInit, OnDestroy {

  private activeSectionSubscription;
  private editingAreaWidthSubscription;

  public activeSection: ISectionInfo;
  public editingAreaWidth: string = '0';

  constructor(
    private storeWork$: Store<workRoot.WorkState>,
    private globals: GlobalsService,
  ) { }

  ngOnInit() {
    this.activeSectionSubscription = this.storeWork$
      .pipe(select(workRoot.selectActiveSection))
      .subscribe(section => {
        this.activeSection = section;
      });
    this.editingAreaWidthSubscription = this.globals.documentInfo$.subscribe(doc => {
      this.editingAreaWidth = doc.editingAreaWidth.toString() + 'px';
    });
  }

  ngOnDestroy() {
    this.activeSectionSubscription.unsubscribe();
    this.editingAreaWidthSubscription.unsubscribe();
  }

}
