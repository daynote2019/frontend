import {Component, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {GlobalsService} from '@shared/services/globals.service';
import {MobileLeftSidenavListItemArrowOpen} from '@shared/interfaces/common';
import {Pages} from '@shared/enums/common';
import {InternalUsers, ISectionInfo} from '@shared/interfaces/work';
import {select, Store} from '@ngrx/store';
import * as workRoot from '@app/root-store/work/reduser';
import {
  CreateInternalUserAction,
  SelectInternalUserServerAction
} from '@app/root-store/work/actions';
import {SessionService} from '@shared/services/session.service';
import {ModalBasicCenterAction} from '@app/root-store/common/actions';
import {IModalBasicCenterType} from '@shared/enums/common';
import * as mainRoot from '@app/root-store/common/reduser';
import {NavService} from '@shared/services/nav.service';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['../../unauthorized/main-top/main-top.component.scss', './top-nav.component.scss']
})
export class TopNavComponent implements OnInit, OnDestroy {

  private internalUsersSubscription;
  private activeInternalUserSubscription;

  /* Посылает зачение в <left-menu> */
  public mobileIsOpen = false;
  public closeList: MobileLeftSidenavListItemArrowOpen;
  public activeInternalUser: InternalUsers;
  public internalUsers: Array<InternalUsers>;

  public EPages = Pages;
  public GlobalsService = GlobalsService;

  constructor(
    private globals: GlobalsService,
    private renderer: Renderer2,
    public navService: NavService,
    private storeWork$: Store<workRoot.WorkState>,
    private sessionService: SessionService,
    private store$: Store<mainRoot.FeatureState>,
  ) { }

  ngOnInit() {
    this.internalUsersSubscription = this.storeWork$
      .pipe(select(workRoot.selectInternalUsers))
      .subscribe(internalUsers => {
        this.internalUsers = internalUsers;
      });
    this.activeInternalUserSubscription = this.storeWork$
      .pipe(select(workRoot.selectActiveInternalUser))
      .subscribe(internalUser => {
        this.activeInternalUser = internalUser;
      });
    this.closeList = {
      users: false,
    };
  }

  ngOnDestroy() {
    this.internalUsersSubscription.unsubscribe();
    this.activeInternalUserSubscription.unsubscribe();
  }

  /* Открыть/закрыть мобильное меню
   * Вызывается так же из <left-menu> */
  controlLeftMenu(value: boolean) {
    this.mobileIsOpen = value;
    if (value) { // Если меню открыто, то скрываем скролл у body(фиксируем)
      this.globals.fixBody(this.renderer, true);
    } else {
      this.globals.fixBody(this.renderer, false);
    }
  }

  mouseEnter(item) {
    this.closeList[item] = true;
  }

  mouseLeave(item) {
    this.closeList[item] = false;
  }

  /* Выбрать внутреннего пользователя */
  selectInternalUser(iUser: InternalUsers) {
    if (this.activeInternalUser && this.activeInternalUser.id !== iUser.id) {
      this.storeWork$.dispatch(new SelectInternalUserServerAction({iUser}));
    }
    this.closeList.users = false;
  }

  /* Добавить нового пользователя */
  addInternalUser() {
    this.closeList.users = false;
    this.store$.dispatch(new ModalBasicCenterAction({
      state: true,
      type: IModalBasicCenterType.confirmInputWithSelect,
      title: 'modalBasicCenter.createInternalUser.title',
      message: 'modalBasicCenter.createInternalUser.message',
      inputLabel: 'modalBasicCenter.createInternalUser.inputLabel',
      selectLabel: 'modalBasicCenter.createInternalUser.selectLabel',
      selectValue: 'account-circle.svg',
      selectArray: this.globals.internalUserIconArray,
      buttonActionName: 'modalBasicCenter.createInternalUser.buttonActiveName',
      action: (userName: string, iconName) => {
        this.storeWork$.dispatch(new CreateInternalUserAction({
          id: '',
          name: userName,
          icon: iconName,
          active: true,
          isRemoved: false
        }));
      }
    }));
  }

  onRightClick(event) {
    return false;
  }

  onPageRoute(url) {
    this.closeList.users = false;
    this.navService.onPageRoute(url);
  }
}
