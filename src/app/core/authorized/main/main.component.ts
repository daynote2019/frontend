import { Component, OnInit } from '@angular/core';
import {SpinnerCenterSelectAction} from '@app/root-store/common/actions';
import {Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(
    protected storeMain$: Store<mainRoot.FeatureState>
  ) { }

  ngOnInit() {
    this.storeMain$.dispatch(new SpinnerCenterSelectAction({
      state: false,
    }));
  }

}
