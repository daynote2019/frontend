import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {MatButtonModule, MatFormFieldModule, MatInputModule, MatProgressBarModule, MatSelectModule} from '@angular/material';

import { MainComponent } from './main/main.component';
import { UserComponent } from './user.component';
import {AuthGuard} from '@shared/services/auth.guard';
import { TopNavComponent } from './top-nav/top-nav.component';
import { LeftMenuComponent } from './left-menu/left-menu.component';
import { ProfileComponent } from './profile/profile.component';
import { SettingsComponent } from './settings/settings.component';
import { UserContentComponent } from './user-content/user-content.component';
import { LeftToolbarComponent } from './user-content/left-toolbar/left-toolbar.component';
import { LeftSidebarComponent } from './user-content/left-sidebar/left-sidebar.component';
import { TabsComponent } from './user-content/tabs/tabs.component';
import { DocsToolbarComponent } from './user-content/docs-toolbar/docs-toolbar.component';
import { SharedModule } from '@shared/shared.module';
import { ResizableModule } from 'angular-resizable-element';
import { EditingAreaComponent } from './user-content/editing-area/editing-area.component';
import {FormsModule} from '@angular/forms';
import { SectionInfoComponent } from './user-content/section-info/section-info.component';
import { OneSectionComponent } from './user-content/section-info/one-section.component';
import { TagsComponent } from './user-content/tags/tags.component';

const routes: Routes = [
  { path: '', component: MainComponent, canActivate: [AuthGuard] },
  { path: 'user', component: UserComponent, canActivate: [AuthGuard],
    children: [
      { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
      { path: 'settings', component: SettingsComponent, canActivate: [AuthGuard] },
    ]
  },
];

@NgModule({
  declarations: [
    MainComponent,
    UserComponent,
    TopNavComponent,
    LeftMenuComponent,
    ProfileComponent,
    SettingsComponent,
    UserContentComponent,
    LeftToolbarComponent,
    LeftSidebarComponent,
    TabsComponent,
    DocsToolbarComponent,
    EditingAreaComponent,
    SectionInfoComponent,
    OneSectionComponent,
    TagsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    AngularFontAwesomeModule,
    SharedModule,
    MatProgressBarModule,
    ResizableModule,
    FormsModule
  ],
  exports: [
    MatProgressBarModule
  ]
})
export class AuthorizedModule { }
