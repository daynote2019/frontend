import {Component, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {MobileLeftSidenavListItemArrowOpen, NavList, PageInfo} from '@shared/interfaces/common';
import {NavListTypes, Pages} from '@shared/enums/common';
import {NavService} from '@shared/services/nav.service';
import {LogoutAction} from '@app/root-store/auth/actions';
import {Store} from '@ngrx/store';
import * as authRoot from '@app/root-store/auth/reduser';
import {SpinnerCenterSelectAction} from '@app/root-store/common/actions';
import * as mainRoot from '@app/root-store/common/reduser';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['../../../shared/components/mobile-menu/mobile-menu.component.scss', './left-menu.component.scss']
})
export class LeftMenuComponent implements OnInit, OnDestroy {

  private pageInfoSubscription;

  public pageInfo: PageInfo;
  public listType = NavListTypes;
  /* Открыть/закрыть мобайл левый sidenav изнутри компонента */
  @Input() public mobileIsOpen: boolean;
  /* Открыть/закрыть мобайл левый sidenav снаружи компонента */
  @Output() public parentMobileIsOpen = new EventEmitter<boolean>();
  /* Открыть/закрыть выпадающий список пункта меню с arrow */
  public arrowOpen: MobileLeftSidenavListItemArrowOpen;
  public mobileLeftSidenavList: NavList[];

  public EPages = Pages;

  constructor(
    public navService: NavService,
    private storeAuth$: Store<authRoot.AuthState>,
    private storeMain$: Store<mainRoot.FeatureState>,
  ) {}

  ngOnInit() {
    /* Подписаться на изменение информации о странице */
    this.pageInfoSubscription = this.navService.pageInfo$.subscribe(pageInfo => {
      this.pageInfo = pageInfo;
    });
    this.mobileIsOpen = false;
    this.arrowOpen = {
      about: this.isPageActive(Pages.about)
    };
    // TODO что будет в этом меню?
    this.mobileLeftSidenavList = [
      {
        type: NavListTypes.link,
        active: Pages.profile,
        click: Pages.profile,
        name: 'page.profile',
      },
      {
        type: NavListTypes.link,
        active: Pages.settings,
        click: Pages.settings,
        name: 'page.settings',
      },
      {
        type: NavListTypes.line,
        click: '',
        name: '',
      },
      {
        type: NavListTypes.function,
        name: 'auth.logout',
        action: () => {
          /* Logout */
          this.storeAuth$.dispatch(new LogoutAction());
          this.storeMain$.dispatch(new SpinnerCenterSelectAction({state: true}));
        },
      },
    ];
  }

  ngOnDestroy() {
    this.pageInfoSubscription.unsubscribe();
  }

  /* Открыть/закрыть мобильное меню */
  controlMobileMenu(value: boolean) {
    this.setMobileOpens(value);
    this.arrowStateAssign();
  }

  /* Открыть/закрыть выпадающий список пункта меню с arrow */
  openArrowNavItem(item: string) {
    this.arrowOpen[item] = !this.arrowOpen[item];
  }

  public onPageRoute(url: string) {
    this.navService.onPageRoute(url);
    this.setMobileOpens(false);
    this.arrowStateAssign();
  }

  /* Событие ресайз браузера */
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setMobileOpens(false);
    this.arrowStateAssign();
  }

  public isPageActive(page: string) {
    return page === this.pageInfo.chosen;
  }

  public isChildActive(page: string) {
    return page === this.pageInfo.childActive;
  }

  private setMobileOpens(value: boolean) {
    if (this.mobileIsOpen) {
      this.mobileIsOpen = value;
      this.parentMobileIsOpen.emit(value);
    }
  }

  /* Назначить стрелочным подпунктам меню их состояние(true/false) в зависимости от страницы */
  private arrowStateAssign() {
    setTimeout(() => {
      for (const prop in this.arrowOpen) {
        this.arrowOpen[prop] = this.isPageActive(prop);
      }
    }, 400);
  }
}
