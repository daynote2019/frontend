import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';
import {NavService} from '@shared/services/nav.service';
import {GlobalsService} from '@shared/services/globals.service';
import {AbstractPageComponent} from '@app/core/unauthorized/pages/abstract-page.component';
import {Pages} from '@shared/enums/common';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent extends AbstractPageComponent implements OnInit {

  constructor(
    protected storeMain$: Store<mainRoot.FeatureState>,
    public navService: NavService,
    public globals: GlobalsService,
  ) {
    super(storeMain$);
    navService.setPageInfo({
      isHome: false,
      title: 'page.settings',
      chosen: Pages.settings
    });
  }

  ngOnInit() {
  }

}
