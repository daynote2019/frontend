import { Component, OnInit } from '@angular/core';
import {AbstractPageComponent} from '@app/core/unauthorized/pages/abstract-page.component';
import {Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';
import {Pages} from '@shared/enums/common';
import {NavService} from '@shared/services/nav.service';
import {GlobalsService} from '@shared/services/globals.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent extends AbstractPageComponent implements OnInit {

  constructor(
    protected storeMain$: Store<mainRoot.FeatureState>,
    public navService: NavService,
    public globals: GlobalsService,
  ) {
    super(storeMain$);
    navService.setPageInfo({
      isHome: false,
      title: 'page.profile',
      chosen: Pages.profile
    });
  }

  ngOnInit() {
  }

}
