import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user',
  template: `
    <div class="root root-content">
      <div class="root-content-main">
        <app-top-nav></app-top-nav>
      </div>
    </div>
  `
})
export class UserComponent implements OnInit {

  constructor() {}

  ngOnInit() {}

}
// @HostListener('document:contextmenu', ['$event'])
