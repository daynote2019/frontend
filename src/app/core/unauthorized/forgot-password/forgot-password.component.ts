import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractPageComponent} from '@app/core/unauthorized/pages/abstract-page.component';
import {select, Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';
import {NavService} from '@shared/services/nav.service';
import {GlobalsService} from '@shared/services/globals.service';
import * as authRoot from '@app/root-store/auth/reduser';
import * as errorsRoot from '@app/root-store/errors/reduser';
import {AlertTypes, Pages} from '@shared/enums/common';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '@shared/services/auth.service';
import {ForgotPasswordAction} from '@app/root-store/auth/actions';
import {SpinnerTopSelectAction} from '@app/root-store/common/actions';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent extends AbstractPageComponent implements OnInit, OnDestroy {

  private validateErrorsSubscription;

  public pages = Pages;
  public forgotPasswordForm: FormGroup;

  constructor(
    public navService: NavService,
    public globals: GlobalsService,
    protected storeMain$: Store<mainRoot.FeatureState>,
    private storeAuth$: Store<authRoot.AuthState>,
    private storeErrors$: Store<errorsRoot.ErrorsState>,
  ) {
    super(storeMain$);
    navService.setPageInfo({
      isHome: false,
      title: 'page.login',
      chosen: Pages.forgotPassword
    });
    this.forgotPasswordForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email,
        Validators.maxLength(AuthService.emailMaxLength)
      ]),
    });
  }

  ngOnInit() {
    this.validateErrorsSubscription =  this.storeErrors$.pipe(select(errorsRoot.selectResponseValidateErrors)).subscribe(err => {
      if (this.globals.isNotEmptyObject(err) &&
        err.alertType && err.alertType === AlertTypes.fields) {
        const email = this.forgotPasswordForm.get('email');
        if (err.email) {
          email.setErrors({[err.email]: true});
        }
      }
    });
  }

  ngOnDestroy() {
    this.validateErrorsSubscription.unsubscribe();
  }

  public submit() {
    if (this.forgotPasswordForm.invalid) {
      return;
    }
    this.storeAuth$.dispatch(new ForgotPasswordAction(this.forgotPasswordForm.get('email').value));
    this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: true }));
  }
}
