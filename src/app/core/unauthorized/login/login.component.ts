import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavService} from '@shared/services/nav.service';
import {AlertTypes, Pages} from '@shared/enums/common';
import {AbstractPageComponent} from '@app/core/unauthorized/pages/abstract-page.component';
import {select, Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginAction} from '@app/root-store/auth/actions';
import {SpinnerTopSelectAction} from '@app/root-store/common/actions';
import * as authRoot from '@app/root-store/auth/reduser';
import {GlobalsService} from '@shared/services/globals.service';
import {AuthService} from '@shared/services/auth.service';
import * as errorsRoot from '@app/root-store/errors/reduser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends AbstractPageComponent implements OnInit, OnDestroy {

  private validateErrorsSubscription;

  public pages = Pages;
  public loginForm: FormGroup;

  constructor(
    public navService: NavService,
    public globals: GlobalsService,
    protected storeMain$: Store<mainRoot.FeatureState>,
    private storeAuth$: Store<authRoot.AuthState>,
    private storeErrors$: Store<errorsRoot.ErrorsState>,
  ) {
    super(storeMain$);
    navService.setPageInfo({
      isHome: false,
      title: 'page.login',
      chosen: Pages.login
    });
    this.loginForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email,
        Validators.maxLength(AuthService.emailMaxLength)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(AuthService.passwordMinLength),
        Validators.maxLength(AuthService.passwordMaxLength),
        // Validators.pattern(AuthService.passwordPattern)
      ]),
      alienComp: new FormControl(false),
    });
  }

  ngOnInit() {
    this.validateErrorsSubscription = this.storeErrors$
      .pipe(select(errorsRoot.selectResponseValidateErrors))
      .subscribe(err => {
        if (this.globals.isNotEmptyObject(err) &&
          err.alertType && err.alertType === AlertTypes.fields) {
          const email = this.loginForm.get('email');
          const password = this.loginForm.get('password');
          if (err.email) {
            email.setErrors({[err.email]: true});
          }
          if (err.password) {
            password.setErrors({[err.password]: true});
          }
        }
    });
  }

  ngOnDestroy() {
    this.validateErrorsSubscription.unsubscribe();
  }

  public loginSubmit() {
    if (this.loginForm.invalid) {
      return;
    }
    this.storeAuth$.dispatch(new LoginAction({
      email: this.loginForm.get('email').value,
      password: this.loginForm.get('password').value,
      isAlienComp: this.loginForm.get('alienComp').value,
    }));
    this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: true }));
  }
}
