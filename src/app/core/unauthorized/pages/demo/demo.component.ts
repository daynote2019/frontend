import { Component, OnInit } from '@angular/core';
import {NavService} from '@shared/services/nav.service';
import {Pages} from '@shared/enums/common';
import {Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';
import {AbstractPageComponent} from '@app/core/unauthorized/pages/abstract-page.component';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent extends AbstractPageComponent implements OnInit {

  constructor(
    navService: NavService,
    protected storeMain$: Store<mainRoot.FeatureState>,
  ) {
    super(storeMain$);
    navService.setPageInfo({
      isHome: false,
      title: 'page.demo',
      chosen: Pages.demo
    });
  }

  ngOnInit() {
  }

}
