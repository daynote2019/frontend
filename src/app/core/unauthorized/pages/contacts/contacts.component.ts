import { Component, OnInit } from '@angular/core';
import {NavService} from '@shared/services/nav.service';
import {Pages} from '@shared/enums/common';
import {AbstractPageComponent} from '@app/core/unauthorized/pages/abstract-page.component';
import {Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent extends AbstractPageComponent implements OnInit {

  constructor(
    navService: NavService,
    protected storeMain$: Store<mainRoot.FeatureState>,
  ) {
    super(storeMain$);
    navService.setPageInfo({
      isHome: false,
      title: 'page.contacts',
      chosen: Pages.contacts
    });
  }

  ngOnInit() {
  }

}
