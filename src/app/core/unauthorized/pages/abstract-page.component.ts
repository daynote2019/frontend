import {AfterViewInit, Component, OnInit} from '@angular/core';
import {SpinnerCenterSelectAction} from '@app/root-store/common/actions';
import {Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';

@Component({
  selector: 'app-abstract-page',
  template: ``
})
export class AbstractPageComponent implements AfterViewInit {

  constructor(
    protected storeMain$: Store<mainRoot.FeatureState>,
  ) {}

  ngAfterViewInit() {
    setTimeout(() => {
      this.storeMain$.dispatch(new SpinnerCenterSelectAction({ state: false }));
    }, 10);
  }
}
