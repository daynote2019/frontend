import { Component, OnInit } from '@angular/core';
import {NavService} from '@shared/services/nav.service';
import {Pages} from '@shared/enums/common';
import {AbstractPageComponent} from '@app/core/unauthorized/pages/abstract-page.component';
import {Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends AbstractPageComponent implements OnInit {

  constructor(
    navService: NavService,
    protected storeMain$: Store<mainRoot.FeatureState>,
  ) {
    super(storeMain$);
    navService.setPageInfo({
      isHome: true,
      title: 'page.home',
      chosen: Pages.home
    });
  }

  ngOnInit() {}

}
