import {AfterViewInit, Component, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {CustomTranslateService} from '@shared/services/custom-translate.service';
import {NavService} from '@shared/services/nav.service';
import {Langs, MobileLeftSidenavListItemArrowOpen, NavList, PageInfo} from '@shared/interfaces/common';
import {LangSelectAction} from '@app/root-store/common/actions';
import {select, Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';
import {NavListTypes, Pages} from '@shared/enums/common';
import {logo} from '../../../../environments/environment';
import {GlobalsService} from '@shared/services/globals.service';

@Component({
  selector: 'app-main-top',
  templateUrl: './main-top.component.html',
  styleUrls: ['./main-top.component.scss']
})
export class MainTopComponent implements OnInit, AfterViewInit, OnDestroy {

  private pageInfoSubscription;
  private selectLangSubscription;

  public title = logo;
  public pages = Pages;
  public listType = NavListTypes;
  public leftNavList: NavList[];
  public rightNavList: NavList[];
  public arrowOpen: MobileLeftSidenavListItemArrowOpen;
  public closeList: MobileLeftSidenavListItemArrowOpen;
  public langName: string;
  public langs: Array<Langs>;
  public pageInfo: PageInfo;
  /* Посылает зачение в <app-mobile-menu> */
  public mobileIsOpen = false;

  constructor(
    private router: Router,
    private globals: GlobalsService,
    private renderer: Renderer2,
    private translate: TranslateService,
    private customTranslate: CustomTranslateService,
    public navService: NavService,
    private store$: Store<mainRoot.FeatureState>,
    ) {
    this.langs = customTranslate.getLangs();
  }

  ngOnInit() {
    /* Подписаться на изменение информации о странице */
    this.pageInfoSubscription = this.navService
      .pageInfo$.subscribe(pageInfo => {
        this.pageInfo = pageInfo;
      });
    this.selectLangSubscription = this.store$.pipe(select(mainRoot.selectLang))
      .subscribe(lang => {
      this.langName = this.customTranslate.getLangName(lang);
    });
    this.arrowOpen = {
      about: false,
      lang: false,
    };
    this.closeList = {
      about: false,
      lang: false,
    };
    this.leftNavList = [
      // {
      //   type: NavListTypes.arrow,
      //   name: 'page.about',
      //   click: Pages.about,
      //   active: Pages.about,
      //   children: [
      //     {
      //       click: Pages.history,
      //       active: Pages.about,
      //       name: 'page.history.name',
      //       img: 'history.svg'
      //     },
      //     {
      //       click: Pages.how,
      //       active: Pages.about,
      //       name: 'page.howBegin.name',
      //       img: 'compass-calibration.svg'
      //     },
      //     {
      //       click: Pages.history,
      //       active: Pages.about,
      //       name: 'page.whereFind.name',
      //       img: 'how-to-reg.svg'
      //     },
      //   ]
      // },
      // {
      //   type: NavListTypes.link,
      //   name: 'page.contacts',
      //   click: Pages.contacts,
      //   active: Pages.contacts,
      // },
    ];
    this.rightNavList = [
      {
        type: NavListTypes.link,
        name: 'page.demo',
        click: Pages.demo,
        active: Pages.demo,
      },
      {
        type: NavListTypes.link,
        name: 'page.login',
        click: Pages.login,
        active: Pages.login,
      },
      {
        type: NavListTypes.link,
        name: 'page.registration',
        click: Pages.registration,
        active: Pages.registration,
      },
    ];
  }

  ngOnDestroy() {
    this.pageInfoSubscription.unsubscribe();
    this.selectLangSubscription.unsubscribe();
  }

  ngAfterViewInit() { }

  /* Выбрать язык */
  chooseLang(lang) {
    this.store$.dispatch(new LangSelectAction({ lang }));
    this.closeList.lang = false;
  }

  mouseEnter(item) {
    this.closeList[item] = true;
    if (!this.isPageActive(item)) {
      this.arrowOpen[item] = true;
    }
  }

  mouseLeave(item) {
    this.closeList[item] = false;
    if (!this.isPageActive(item)) {
      this.arrowOpen[item] = false;
    }
  }

  /* Открыть/закрыть мобильное меню
   * Вызывается так же из <app-mobile-menu> */
  controlMobileMenu(value: boolean) {
    this.mobileIsOpen = value;
    if (value) { // Если меню открыто, то скрываем скролл у body(фиксируем)
      this.globals.fixBody(this.renderer, true);
    } else {
      this.globals.fixBody(this.renderer, false);
    }
  }

  /* Проверка находимся ли мы на странице пункта меню */
  isPageActive(page: string) {
    return page === this.pageInfo.chosen;
  }

  /* Проверка находимся ли мы на странице ПОДпункта меню */
  isChildActive(page: string) {
    return page === this.pageInfo.childActive;
  }

  /* При переходе на страницу ПОДпункта меню. выключить */
  onPageRouteByArrow(url: string, from) {
    this.navService.onPageRoute(url);
    this.arrowOpen[from] = false;
    this.closeList[from] = false;
  }
}
