import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NavService} from '@shared/services/nav.service';
import {AlertTypes, Pages} from '@shared/enums/common';
import {GlobalsService} from '@shared/services/globals.service';
import {SpinnerTopSelectAction} from '@app/root-store/common/actions';
import {select, Store} from '@ngrx/store';
import * as errorsRoot from '@app/root-store/errors/reduser';
import * as authRoot from '@app/root-store/auth/reduser';
import * as mainRoot from '@app/root-store/common/reduser';
import {RegisterAction} from '@app/root-store/auth/actions';
import {AbstractPageComponent} from '@app/core/unauthorized/pages/abstract-page.component';
import {AuthService} from '@shared/services/auth.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent extends AbstractPageComponent implements OnInit, OnDestroy {

  private validateErrorsSubscription;

  public pages = Pages;
  public registrationForm: FormGroup;
  public ifAgree: boolean;
  private lang: string;

  constructor(
    public navService: NavService,
    public globals: GlobalsService,
    protected storeMain$: Store<mainRoot.FeatureState>,
    private storeAuth$: Store<authRoot.AuthState>,
    private storeErrors$: Store<errorsRoot.ErrorsState>,
  ) {
    super(storeMain$);
    this.registrationForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email,
        Validators.maxLength(AuthService.emailMaxLength)
      ]),
      agree: new FormControl(false, Validators.requiredTrue),
    });
    navService.setPageInfo({
      isHome: false,
      title: 'page.registration',
      chosen: Pages.registration
    });
  }

  ngOnInit() {
    this.ifAgree = false;
    this.validateErrorsSubscription = this.storeErrors$
      .pipe(select(errorsRoot.selectResponseValidateErrors))
      .subscribe(err => {
        if (this.globals.isNotEmptyObject(err) &&
        err.alertType && err.alertType === AlertTypes.fields) {
        const email = this.registrationForm.get('email');
        if (err.email) {
          email.setErrors({[err.email]: true});
        }
      }
    });
    this.storeMain$.pipe(select(mainRoot.selectLang)).subscribe(lang => this.lang = lang);
  }

  ngOnDestroy() {
    this.validateErrorsSubscription.unsubscribe();
  }

  public clickAgree() {
    this.ifAgree = true; // Чтобы активировать mat-error для agree(изначально он false, чтобы не показывать error сразу)
  }

  public checkAgree() {
    return (this.ifAgree && !this.registrationForm.controls.agree.value);
  }

  public registrationSubmit() {
    this.ifAgree = true; // Чтобы активировать mat-error для agree(изначально он false, чтобы не показывать error сразу)
    if (this.registrationForm.invalid) {
      return;
    }
    this.storeAuth$.dispatch(new RegisterAction({
      email: this.registrationForm.get('email').value,
      agree: this.registrationForm.get('agree').value,
      lang: this.lang
    }));
    this.storeMain$.dispatch(new SpinnerTopSelectAction({ state: true }));
  }
}
