import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GlobalsService} from '@shared/services/globals.service';
import {CentralModalAlertSelectAction, SpinnerCenterSelectAction} from '@app/root-store/common/actions';
import {select, Store} from '@ngrx/store';
import * as mainRoot from '../../../root-store/common/reduser';
import {AlertTypes, CentralModalAlertType, ErrorMessageFormControlField, Pages} from '@shared/enums/common';
import {RegisterCreateNewAction} from '@app/root-store/auth/actions';
import * as authRoot from '@app/root-store/auth/reduser';
import * as errorsRoot from '@app/root-store/errors/reduser';
import {NavService} from '@shared/services/nav.service';
import {IDeviceResponse} from '@shared/interfaces/request';
import {CustomHttpService} from '@shared/services/custom-http.service';
import {INetworkData} from '@shared/interfaces/common';

@Component({
  selector: 'app-registration-create-new',
  template: ``
})
export class RegistrationCreateNewComponent implements OnInit, OnDestroy {

  private routeSubscription;
  private validateErrorsSubscription;
  private networkDataSubscription;

  private address: INetworkData;

  constructor(
    private route: ActivatedRoute,
    private globals: GlobalsService,
    private nav: NavService,
    private customHttpService: CustomHttpService,
    protected storeMain$: Store<mainRoot.FeatureState>,
    private storeAuth$: Store<authRoot.AuthState>,
    private storeErrors$: Store<errorsRoot.ErrorsState>
  ) { }

  ngOnInit() {
    this.routeSubscription = this.route.queryParams
      .subscribe(params => {
        if (params && this.globals.isNotEmptyObject(params)) {
          const tempUserToken = params.token ? params.token : '';
          const lang = params.lang ? params.lang : '';
          const deviceInfo = this.customHttpService.getDeviceInfo();
          this.networkDataSubscription = this.storeMain$
            .pipe(select(mainRoot.selectNetworkData))
            .subscribe(networkData => {
              this.address = networkData;
            });
          /* На отправку ставим таймаут, чтобы подтянулиь данные по ip-адресу */
          setTimeout(() => {
            const device: IDeviceResponse = {
              name: deviceInfo.name,
              browser: deviceInfo.browser,
              ipAddress: (this.address && this.address.ip) && this.address.ip || '',
              macAddress: (this.address && this.address.mac) && this.address.mac || '',
            };
            this.storeAuth$.dispatch(new RegisterCreateNewAction({
              tempUserToken,
              lang,
              device
            }));
          }, 1000);
        } else {
          this.storeMain$.dispatch(new SpinnerCenterSelectAction({
            state: false,
            action: () => {
              return this.storeMain$.dispatch(new CentralModalAlertSelectAction({
                state: true,
                type: CentralModalAlertType.error,
                title: 'errors.registerCreateNew.title',
                message: 'errors.registerCreateNew.message',
                button: 'errors.unknown.button',
                url: Pages.registration,
                isRedirectIfClickBg: true
              }));
            }
          }));
        }
      });
    /* Обработка ошибок */
    this.validateErrorsSubscription = this.storeErrors$.pipe(select(errorsRoot.selectResponseValidateErrors)).subscribe(err => {
      if (this.globals.isNotEmptyObject(err) &&
        err.alertType && err.alertType === AlertTypes.centralModal) {
        if (err.user) {
          this.setCentralModal(err.user);
        } else if (err.email) {
          this.setCentralModal(ErrorMessageFormControlField[err.email]);
        } else if (err.tempUserToken) {
          return this.storeMain$.dispatch(new CentralModalAlertSelectAction({
            state: true,
            type: CentralModalAlertType.error,
            title: 'errors.registerCreateNew.title',
            message: 'errors.registerCreateNew.tempUserToken.' + err.tempUserToken,
            button: 'errors.unknown.button',
            url: Pages.registration,
            isRedirectIfClickBg: true
          }));
        } else {
          this.setCentralModal('errors.unknown.message');
        }
      }
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
    this.validateErrorsSubscription.unsubscribe();
    this.networkDataSubscription.unsubscribe();
  }

  private setCentralModal(message: string) {
    return this.storeMain$.dispatch(new CentralModalAlertSelectAction({
      state: true,
      type: CentralModalAlertType.error,
      title: 'errors.unknown.title',
      message,
      button: 'errors.unknown.button',
      url: Pages.registration,
      isRedirectIfClickBg: true
    }));
  }
}
