import {AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {FooterTopInfo, Langs} from '@shared/interfaces/common';
import {NavService} from '@shared/services/nav.service';
import {CustomTranslateService} from '@shared/services/custom-translate.service';
import {TranslateService} from '@ngx-translate/core';
import {Store, select} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';
import {LangSelectAction} from '@app/root-store/common/actions';
import {Observable} from 'rxjs';
import { logo } from '../../../../environments/environment';
import {Pages} from '@shared/enums/common';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, AfterViewInit {

  public lang$: Observable<string>;
  public langs: Array<Langs>;
  public logo: string = logo;
  private pages = Pages;
  public topInfo: Array<FooterTopInfo>;
  public year = (new Date()).getFullYear();
  @ViewChild('footer') footer: ElementRef;

  constructor(
    public navService: NavService,
    private customTranslate: CustomTranslateService,
    private translate: TranslateService,
    private store$: Store<mainRoot.FeatureState>
  ) {
    this.langs = customTranslate.getLangs();
  }

  ngOnInit() {
    this.lang$ = this.store$.pipe(select(mainRoot.selectLang));
    this.topInfo = [
      // {
      //   header: 'footerItems.tariffs.header',
      //   items: [
      //     {
      //       isLink: false,
      //       name: 'footerItems.tariffs.items.free'
      //     },
      //     {
      //       isLink: false,
      //       name: 'footerItems.tariffs.items.medium'
      //     },
      //     {
      //       isLink: false,
      //       name: 'footerItems.tariffs.items.high'
      //     },
      //     {
      //       isLink: false,
      //       name: 'footerItems.tariffs.items.other'
      //     },
      //   ]
      // },
      // {
      //   header: 'footerItems.about.header',
      //   items: [
      //     {
      //       isLink: true,
      //       name: 'footerItems.about.items.history',
      //       url: this.pages.login
      //     },
      //     {
      //       isLink: true,
      //       name: 'footerItems.about.items.how',
      //       url: this.pages.registration
      //     },
      //   ]
      // },
      // {
      //   header: 'footerItems.tariffs.header',
      //   items: [
      //     {
      //       isLink: false,
      //       name: 'footerItems.tariffs.items.free'
      //     },
      //     {
      //       isLink: false,
      //       name: 'footerItems.tariffs.items.medium'
      //     },
      //     {
      //       isLink: false,
      //       name: 'footerItems.tariffs.items.high'
      //     },
      //     {
      //       isLink: false,
      //       name: 'footerItems.tariffs.items.other'
      //     },
      //   ]
      // },
      {
        header: 'footerItems.login.header',
        items: [
          {
            isLink: true,
            name: 'footerItems.login.items.login',
            url: this.pages.login
          },
          {
            isLink: true,
            name: 'footerItems.register.items.register',
            url: this.pages.registration
          },
        ]
      },
    ];
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.setFooterHeight();
    }, 200);
  }

  /* Событие ресайз браузера */
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setFooterHeight();
  }

  setFooterHeight() {
    if (this.footer.nativeElement && this.footer.nativeElement.offsetHeight) {
      this.navService.setFooterInfo({
        height: this.footer.nativeElement.offsetHeight
      });
    }
  }

  /* Выбрать язык */
  chooseLang(lang) {
    this.store$.dispatch(new LangSelectAction({ lang }));
  }
}
