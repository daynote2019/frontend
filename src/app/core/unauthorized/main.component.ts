import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavService} from '@shared/services/nav.service';

@Component({
  selector: 'app-base-root',
  template: `
        <div class="root root-content">
          <div class="root-content-main"
               [style.padding-bottom]="footerHeight">
            <app-main-top></app-main-top>
            <app-main-content></app-main-content>
            <app-footer></app-footer>
          </div>
        </div>
    `
})
export class MainComponent implements OnInit, OnDestroy {

  private footerInfoSubscription;

  public footerHeight: string;

  constructor(
    private navService: NavService
  ) {
    this.footerHeight = '200px';
  }

  ngOnInit() {
    /* Подписаться на изменение информации о футере */
    this.footerInfoSubscription = this.navService.footerInfo$.subscribe(footerInfo => {
      this.footerHeight = `${footerInfo.height + 10}px`;
    });
  }

  ngOnDestroy() {
    this.footerInfoSubscription.unsubscribe();
  }
}
