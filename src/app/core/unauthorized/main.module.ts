import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MainComponent} from './main.component';
import {RouterModule, Routes} from '@angular/router';
import { RootStoreModule } from '@app/root-store/root-store.module';
import { MainTopComponent } from './main-top/main-top.component';
import { MainContentComponent } from './main-content/main-content.component';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule, MatMenuModule, MatToolbarModule,
  MatSelectModule, MatInputModule, MatCheckboxModule, MatButtonModule} from '@angular/material';
import {NgxMatSelectSearchModule} from 'ngx-mat-select-search';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import {TranslateModule} from '@ngx-translate/core';
import {MobileMenuComponent} from '@shared/components/mobile-menu/mobile-menu.component';
import { FooterComponent } from './footer/footer.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { CustomMatSelectComponent } from '@shared/components/custom-mat-select/custom-mat-select.component';
import { RegistrationComponent } from './registration/registration.component';
import { RegistrationCreateNewComponent } from './registration/registration-create-new.component';
import { ContactsComponent } from './pages/contacts/contacts.component';
import { DemoComponent } from './pages/demo/demo.component';
import { HistoryComponent } from './pages/history/history.component';
import { HowComponent } from './pages/how/how.component';
import { HomeComponent } from './pages/home/home.component';
import { AbstractPageComponent } from './pages/abstract-page.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { SharedModule } from '@shared/shared.module';

const routes: Routes = [
  { path: 'main', component: MainComponent,
    children: [
      {path: 'home', component: HomeComponent},
      {path: 'login', component: LoginComponent},
      {path: 'forgot-password', component: ForgotPasswordComponent},
      {path: 'registration', component: RegistrationComponent},
      {path: 'registration-create-new', component: RegistrationCreateNewComponent},
      {path: 'contacts', component: ContactsComponent},
      {path: 'demo', component: DemoComponent},
      {path: 'history', component: HistoryComponent},
      {path: 'how', component: HowComponent},
    ]
  },
];

@NgModule({
  declarations: [
    MainComponent,
    MainTopComponent,
    MainContentComponent,
    AbstractPageComponent,
    HomeComponent,
    MobileMenuComponent,
    LoginComponent,
    FooterComponent,
    RegistrationComponent,
    ContactsComponent,
    DemoComponent,
    HistoryComponent,
    HowComponent,
    RegistrationCreateNewComponent,
    ForgotPasswordComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    TranslateModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatButtonModule,
    NgxMatSelectSearchModule,
    RootStoreModule,
    MatCheckboxModule,
    SharedModule
  ],
  exports: [
    ReactiveFormsModule,
    MatSelectModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    NgxMatSelectSearchModule,
    MatCheckboxModule,
  ],
  providers: [

  ]
})
export class MainModule { }
