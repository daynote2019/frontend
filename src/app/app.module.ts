import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainModule as UnauthorizedMainModule } from './core/unauthorized/main.module';
import { AuthorizedModule } from './core/authorized/authorized.module';
import { ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule, MatInputModule,
  MatSelectModule, MatCheckboxModule, MatProgressBarModule} from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import {NgxMatSelectSearchModule} from 'ngx-mat-select-search';
import { RootStoreModule } from './root-store/root-store.module';
import {CookieService} from 'ngx-cookie-service';
import {DeviceDetectorModule} from 'ngx-device-detector';
import { CustomMatSelectComponent } from '@shared/components/custom-mat-select/custom-mat-select.component';
import { SpinnerTopComponent } from '@shared/components/spinner-top/spinner-top.component';
import { CentralModalAlertComponent } from '@shared/components/modals/central-modal-alert/central-modal-alert.component';
import { SpinnerCenterComponent } from '@shared/components/spinner-center/spinner-center.component';


export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/localize/', '.json');
}
import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    CentralModalAlertComponent,
    SpinnerTopComponent,
    SpinnerCenterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UnauthorizedMainModule,
    MatFormFieldModule,
    MatProgressBarModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    ReactiveFormsModule,
    MatSelectModule,
    NgxMatSelectSearchModule,
    RootStoreModule,
    DeviceDetectorModule.forRoot(),
    AuthorizedModule,
    SharedModule
  ],
  providers: [
    MatCheckboxModule,
    CookieService
  ],
  bootstrap: [AppComponent],
  exports: [
    MatButtonModule,
    MatFormFieldModule,
    MatProgressBarModule,
    MatInputModule,
    MatSelectModule,
    TranslateModule,
    NgxMatSelectSearchModule,
    RootStoreModule
  ]
})
export class AppModule { }
