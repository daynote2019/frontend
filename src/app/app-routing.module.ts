import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from './core/unauthorized/main.component';
import {HomeComponent} from '@app/core/unauthorized/pages/home/home.component';

const routes: Routes = [
  // { path: '', component: AppComponent, canActivate: [AuthGuard] }, // TODO Реализовать гуард. По идее, если гуард не пустил, то идём ниже и переходим на главную страницу
  // { path: '', redirectTo: '/home', pathMatch: 'full' }, // TODO это наверное не нужно
  // {path: '**', redirectTo: '/home'}, // TODO сделать страницу 404 и перенаправлять на неё
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
