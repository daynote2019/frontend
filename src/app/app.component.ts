import {Component, ElementRef, HostListener, Inject, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {select, Store} from '@ngrx/store';
import * as mainRoot from '@app/root-store/common/reduser';
import {LangSelectAction, NetworkDataSelectAction, SpinnerCenterSelectAction} from '@app/root-store/common/actions';
import {CookieService} from 'ngx-cookie-service';
import {logo} from '../environments/environment';
import { DOCUMENT } from '@angular/common';
import {GlobalsService} from '@shared/services/globals.service';
import {HttpClient} from '@angular/common/http';
import {catchError, tap, map} from 'rxjs/operators';
import { of } from 'rxjs';
import {SessionCommon} from '@shared/enums/sessions';
import * as workRoot from '@app/root-store/work/reduser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  private leftSidebarWidthSubscription;

  public leftSidebarWidth: number = GlobalsService.leftSidebarWidth;
  title = logo;
  @ViewChild('body') body: ElementRef;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private http: HttpClient,
    private store$: Store<mainRoot.FeatureState>,
    protected storeMain$: Store<mainRoot.FeatureState>,
    private storeWork$: Store<workRoot.WorkState>,
    private cookie: CookieService,
    private globals: GlobalsService,
    @Inject(DOCUMENT) private document: Document
  ) {
    // console.log('cookie.getAll', cookie.getAll());
    translate.setDefaultLang('ru');
    if (cookie.check(SessionCommon.lang)) { // Если есть язык в куках, устанавливаем его
      this.setLang(cookie.get(SessionCommon.lang));
    } else { // Если в куках нет языка, берём значение браузера
      if (translate.getBrowserLang()) {
        this.setLang(translate.getBrowserLang());
      }
    }
    // TODO Проверять установлена ли страна? Если нет, то определять страну и уточнять: Ваша страна Россия?
  }

  ngOnInit() {
    this.storeMain$.dispatch(new SpinnerCenterSelectAction({ state: true }));
    this.setScrollTopAndWindowWidth();
    this.setNetworkData();
    this.leftSidebarWidthSubscription = this.storeWork$
      .pipe(select(workRoot.selectLeftSidebarWidth))
      .subscribe(width => {
        this.leftSidebarWidth = width;
        this.setScrollTopAndWindowWidth();
      });
  }

  ngOnDestroy() {
    this.leftSidebarWidthSubscription.unsubscribe();
  }

  /* Событие ресайз браузера */
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.setScrollTopAndWindowWidth();
  }

  /* Событие скролл браузера */
  @HostListener('window:scroll', ['$event'])
  onWindowScroll(event) {
    this.setScrollTopAndWindowWidth();
  }

  /**
   * Записать язык в store
   * При этом через effects и сервис язык записывается в куки и выбирается в translate.use
   */
  private setLang(lang) {
    if (lang === 'ru') {
      this.store$.dispatch(new LangSelectAction({ lang: 'ru' }));
    } else {
      this.store$.dispatch(new LangSelectAction({ lang: 'en' })); // Если не русский, то ставим en
    }
  }

  private setScrollTopAndWindowWidth() {
    const scrollTop = this.document.documentElement.scrollTop ||
      this.document.body.scrollTop || window.pageYOffset || 0;
    const windowWidth = (window && window.innerWidth) || 960;
    const windowHeight = (window && window.innerHeight) || 850;
    const windowOuterHeight = (window && window.outerHeight) || 1000;
    const editingAreaWidth = windowWidth - (25/* leftToolbar */ + this.leftSidebarWidth + 3);

    this.globals.setDocumentInfo({
      windowWidth,
      scrollTop,
      windowHeight,
      windowOuterHeight,
      editingAreaWidth
    });
  }

  /* Установить сетевые данные устройства пользователя */
  private setNetworkData() {
    this.http.get('https://jsonip.com').pipe(
      catchError(err => {
        console.warn('https://jsonip.com is not unavailable.');
        return of<Response>();
      }),
    ).subscribe((data: {ip: string}) => {
      this.storeMain$.dispatch(new NetworkDataSelectAction({
        ip: data.ip,
        mac: '', // TODO скорее всего без мака, да и пока не знаю, зачем он нужен
      }));
    });
  }
}
